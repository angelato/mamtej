@extends('layouts.raise')
@section('content')


<head>


    <link rel="stylesheet" href="/css/form-basic.css">
<style>
    body {
        background-image: url('/images/bgg.png');
        top right no-repeat; 
        background-size: cover;
        margin-top: 0px;
    }
    #x{
        font-size: 40px;
        color: black;
    }
    #popup {
    position: absolute;
    padding: 40px;
    left: 50%;
    top: 50%;
    margin-top: -100px;
    margin-left: -200px;
    background-color: #f7e5c5;
    border-radius: 20px;
    border: 3px solid black;
    }
    #div{
      background-color:#f7e5c5 ;
      padding: 10px;
      width: 375px; 
      height: 235px;
      margin-top: -60px;
      margin-left: 5px;
      border: 3px solid black;
      border-radius: 20px;
    }
    #divf{
      float: right;
      background-color:#f7e5c5 ;
      padding: 5px;
      width: 1300px; 
      margin-right: 5px;
      display: inline-block;
      border: 3px solid black;
      border-radius: 20px;
    }
    #pntdetails {
      font-family: arial, sans-serif;
      border-collapse: collapse;
      width: 100%;
      color: black;
  }
  .pntdetails {
      font-family: arial, sans-serif;
      border-collapse: collapse;
      width: 100%;
      color: black;
  }
  #td, #th {
      border: 1px solid #dddddd;
      text-align: left;
      padding: 8px;
      color: black;
  }
  tr:nth-child(even) {
      background-color: #dddddd;
  }

</style>
</head>
<br>
<div id="divf">
<script type="text/javascript">
     $(document).ready(function (){
      var to = "";
      var total = "";
      var div=$(this).parent();
        $.ajax({
            type: 'GET',
            url: '/helpVoucher',
            data:{
                '_token': $('p[name=_token]').val()
            },
            success:function(data){

                for(var i=0; i<data.length; i++){

                    var image =  data[i].count+'&nbsp;&nbsp;'+'<img src="'+data[i].value+'" width = 300/>'+'&nbsp;&nbsp;';
                    $('#x').append(image);
                }if(data.length == 0){
                  console.log("no vouchers");
                  $('#x').html("No Voucher/s Available");
                } 
                
                console.log(data);
            },
            error:function(){
            }
        });
    });
</script>

<br><br>
<center><p style="font-weight: bold; " id="x"></p></center>
  @if($sponsored_pnt->count() == 0)
    <h1 id="noresult">NO RESULT</h1>
  @endif
<table id="pntdetails">
  <tr id="tr">
    <th id="th">DATE</th>
    <th>STORY BY</th>
    <th>PATIENT'S NAME</th>
    <th>VOUCHER AMOUNT</th>
    <th>SPONSOR'S NAME</th>
  </tr>
	@foreach($sponsored_pnt as $pnt)
  <tr >
		<td id="td">{{$pnt->created_at->format('F d, Y')}}</td>
		<td>{{$pnt->patient->userName->fname}} {{$pnt->patient->userName->lname}}</td>
		<td>{{$pnt->patient->patientname}}</td>
		<td>{{$pnt->sponsor->voucherValue}}</td>
		<td>{{$pnt->sponsor->user->fname}} {{$pnt->sponsor->user->lname}}</td>
	</tr>
	@endforeach
</table><br>

<button id="reco" name="reco" class="btn btn-primary" onclick="recommend()">recommend</button>
<a href="{{ url('/getrecoresults') }}" class="btn btn-link" style="float: right;">View possible results</a><br><br>

<p id="errorc1" style="color: black"><table id="patients" border="1px"></table></p><br>
<table id="longestDuration" name="pntdetails" ></table><br>
<table id="highestNeed"  name="pntdetails" ></table><br>
<table id="condition"  name="pntdetails" ></table><br>
<table id="final"  name="pntdetails" ></table><br>
</div>
<br><br><br>


<div id="div">
<center><h3 style="font-weight: bold">Criteria</h3></center>
<table>@foreach($criteria as $cri)
  <tr>
    <td>{{$cri->name}}</td>
    <td>&nbsp;&nbsp;&nbsp;</td>
    <td><input type="number" step="5" min="0" style="width: 2.5em" value="{{$cri->percentage}}" id="change{{$cri->criteria_id}}">%</td>
  </tr>@endforeach
</table><br>
<center><button onclick="percentchange()" style="color: white" class="btn btn-primary">Change Percenatage/s</button></center><br><br>
<script>
  function percentchange(){
    var c1 = document.getElementById('change1').value;
    var c2 = document.getElementById('change2').value;
    var c3 = document.getElementById('change3').value;
    var c4 = document.getElementById('change4').value;
    var t1 = parseInt(c1);
    var t2 = parseInt(c2);
    var t3 = parseInt(c3);
    var t4 = parseInt(c4);
    var total = t1 + t2 + t3 + t4;
    if(total == 100){
        $.ajax({
        type: "GET",
        url: '/percentChange/'+c1+'/'+c2+'/'+c3+'/'+c4,
          success: function(data){
            console.log(data);
            alert('Percenatage Changed!');
          }
        });
    }else if(total > 100){
      alert('Your percentage is greater than 100%');
    }else if(total < 100){
      alert('Your percentage is lower than 100%');
    }
  }
</script>
</div>



<script>
function recommend(){
      var to = "";
      var text = "";
      var text1 = "";
      var text2 = "";
      var text3 = "";
      var sum_up = "";
        $.ajax({
            type: 'GET',
            url: '/recommending',
            success:function(data){
            if(data[5] != 0){
            if(data[0] != 0){
            text +=   `<tr>
                          <th colspan="4">Most Patient Donated as Sponsor</th>
                        </tr>
                        <tr>
                          <th id="th">Rank</th>
                          <th>Patient's Name</th>
                          <th>Total of number donated</th>
                          <th>Points Given</th>
                        </tr>`;   
            for(var i=0; i<data[0].length; i++){
                text +=`<tr>
                      <td id="td">`+data[0][i].rank+`</td>
                        <td>`+data[0][i].patientname+`</td>
                        <td>`+data[0][i].count+`</td>
                        <td>`+data[0][i].points+`%</td>
                        </tr>`;
            }$('#patients').html(text);
            }else{
              $('#errorc1').html("No Patient/s under this criteria");
            }

            if(data[1] != 0){
            text1 +=   `<tr>
                          <th colspan="4"><center>Patient with Longest Duration</center></th>
                        </tr>
                        <tr>
                        <th id="th">Rank</th>
                          <th>Date</th>
                          <th>Patient's Name</th>
                          <th>Points Given</th>
                        </tr>`;   
            for(var i=0; i<data[1].length; i++){
                var date = data[1][i].created_at.date;
                var dt_ld= new Date(date);
                var options = { year: 'numeric', month: 'long', day: 'numeric' };
                text1 +=`<tr>
                    <td id="td">`+data[1][i].rank+`</td>
                       <td>`+dt_ld.toLocaleDateString("en-US", options)+`</td>
                        <td>`+data[1][i].patientname+`</td>
                        <td>`+data[1][i].points+`%</td>
                        </tr>`;
            }$('#longestDuration').html(text1);
            }else{
              $('#longestDuration').html("No Patient/s under this criteria");
            }

            if(data[2] != 0){
            text2 +=   `<tr>
                          <th colspan="4"><center>Patients with Highest Need</center></th>
                        </tr>
                        <tr>
                        <th id="th">Rank</th>
                          <th>Patient's Name</th>
                          <th>Patient's Remaining Need</th>
                          <th>Points Given</th>
                        </tr>`;   
            for(var i=0; i<data[2].length; i++){
                text2 +=`<tr>
                  <td id="td">`+data[2][i].rank+`</td>
                        <td>`+data[2][i].patientname+`</td>
                        <td>`+data[2][i].lacking+`</td>
                        <td>`+data[2][i].points+`%</td>
                        </tr>`;
            }$('#highestNeed').html(text2);
            }else{
              $('#highestNeed').html("No Patient/s under this criteria");
            }

            if(data[3] != 0){
            text3 +=   `<tr>
                          <th colspan="4"><center>Patient's Condition</center></th>
                        </tr>
                        <tr>
                        <th id="th">Rank</th>
                          <th>Patient's Name</th>
                          <th>Critical Level</th>
                          <th>Points Given</th>
                        </tr>`;   
            for(var i=0; i<data[3].length; i++){
                text3 +=`<tr>
                      <td id="td">`+data[3][i].rank+`</td>
                        <td>`+data[3][i].patientname+`</td>
                        <td>`+data[3][i].condition+`</td>
                        <td>`+data[3][i].points+`%</td>
                        </tr>`;
            }$('#condition').html(text3);
            }else{
              $('#condition').html("No Patient/s under this criteria");
            }

            sum_up +=   `<tr>
                          <th colspan="8"><center>Summary of Computations</center></th>
                        </tr>
                        <tr>
                        <th id="th">Rank</th>
                          <th>Date</th>
                          <th>Patient's Name</th>
                          <th>Longest Duration</th>
                          <th>Number Donated</th>
                          <th>Total Need</th>
                          <th>Critical Level</th>
                          <th>Total Points</th>
                        </tr>`;   
            for(var i=0; i<data[4].length; i++){
                var date = data[4][i].created_at.date;
                var dt_f= new Date(date);
                var options = { year: 'numeric', month: 'long', day: 'numeric' };
                sum_up +=`<tr>
                        <td id="td">`+data[4][i].rank+`</td>
                        <td>`+dt_f.toLocaleDateString("en-US", options)+`</td>
                        <td>`+data[4][i].patientname+`</td>
                        <td>`+data[4][i].c_points+`</td>
                        @if(`data.hasOwnProperty("count")`)
                        <td>`+data[4][i].count+`</td>
                        @else
                        <td>--</td>
                        @endif
                        <td>`+data[4][i].lacking+`</td>
                        <td>`+data[4][i].condition+`</td>
                        <td>`+data[4][i].sum+`%</td>
                        </tr>`;
            }$('#final').html(sum_up);




                  $.ajax({
                        type: "GET",
                        url: "/rcmPatient",
                        success:function(data){
                            if(data.length == 0){
                              $('#noresult').html("NO RESULT");
                            }else{
                              $('#noresult').html(" ");
                            }
                            to+=`<tr id="tr">
                                  <th id="th">DATE</th>
                                  <th>STORY BY</th>
                                  <th>PATIENT'S NAME</th>
                                  <th>VOUCHER AMOUNT</th>
                                  <th>SPONSOR'S NAME</th>
                                </tr>`;
                            for(var i=0; i<data.length; i++){
                                var date = data[i].created_at;
                                var dt = new Date(date);
                                var options = { year: 'numeric', month: 'long', day: 'numeric' };
                                to+=`
                                <tr id="tr">
                                      <td id="td">`+dt.toLocaleDateString("en-US", options)+`</td>
                                      <td>`+data[i].userf+` `+data[i].userl+`</td>
                                      <td>`+data[i].patientname+`</td>
                                      <td>`+data[i].voucherValue+`</td>
                                      <td>`+data[i].fname+` `+data[i].lname+`</td>
                                </tr>
                                `;
                               console.log("success");
                            }$('#pntdetails').html(to);
                                  $.ajax({
                                      type: 'GET',
                                      url: '/helpVoucher',
                                      success:function(data){
                                        console.log("done");
                                          if(data.length == 0){
                                              $('#x').html("All vouchers donated!");
                                          }
                                          for(var i=0; i<data.length; i++){
                                              var image =  data[i].count+'&nbsp;&nbsp;'+'<img src="'+data[i].value+'" width = 300/>'+'&nbsp;&nbsp;';
                                              $('#x').html(image);
                                          }
                                      },
                                      error:function(){
                                        console.log('error');
                                      }
                                  });
                        },
                        error:function(){
                          console.log('error');
                        }
                  });
                }else{
                    $('#x').html("No Voucher/s Donated");
                    alert('No Voucher/s Donated');
                }
            },
            error:function(){
              console.log('error');
            }
        });

};
</script>


@endsection
