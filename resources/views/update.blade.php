@extends('layouts.voucher')
@section('content')

<head>

    
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="/css/apply-form.css">
    <script src="/js/apply-js.js"></script>
    <link rel="stylesheet" href="/css/fileupload.css">
    <script src="/js/fileupload.js"></script>
    <link rel="stylesheet" href="/css/fileupload2.css">
    <script src="/js/fileupload2.js"></script>
    <script class="jsbin" src="https://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>  
    <link rel="stylesheet" href="/css/update.css">
  

</head>

<br><br>


    <div class="main-content" >

    <!-- angel edited -->

              <section class="form-box">
            <div class="container" style="width: 105%;padding-left:0px">
                
                <div class="row">
                    <div class="col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2 col-lg-6 col-lg-offset-3 form-wizard">
          
            <!-- Form Wizard -->
        <form role="form" action="{{url('/saveUpdate')}}" method="post" enctype="multipart/form-data">
        {{csrf_field()}}  

      <input type="hidden" name="patientid" value="{{$patient}}">
<h3>Share with us your updates</h3>
    
      <fieldset>

                <div class="form-group">
                              <label>Story Update Title: <span>*</span></label>
                                    <input type="text" name="updatetitle" placeholder="Story update title" class="form-control required" style="width:100%">
                </div>

                <div class="form-group">
                              <label>Your Story: <span>*</span></label>
                               <textarea name="story" id="countWord" cols="60" rows="7" style="padding-left:50px;border-color:#1FB264" class="form-control required"></textarea>
                                    
                </div>
                                
                <div class="form-group">

        <label>Photo Update: <span>*</span></label>
    <div class="file-upload">
    <button class="file-upload-btn" type="button" onclick="$('.file-upload-input').trigger( 'click' )">Upload profile photo of your story</button>

    <div class="image-upload-wrap">
    <input class="file-upload-input" name="file[]" multiple type='file' onchange="readURL(this);" accept="image/*" />
    <div class="drag-text">
      <h3>Drag or upload photo</h3>
    </div>
  </div>


  <div class="file-upload-content">
    <img class="file-upload-image" src="#" alt="your image" />
    <div class="image-title-wrap">
      <button type="button" onclick="removeUpload()" class="remove-image">Remove <span class="image-title">Uploaded Image</span></button>
    </div>
  </div>
</div>
</div>








                <div align="center">
                <label>
                    <span style="font-color: white"><input type="submit" class="btn btn-primary" value="Submit">
                    <input style="color: white" type="reset" class="btn btn-danger" value="Reset"></span>
                </label>
            </div>
          </fieldset>

    
      
  </div>
</div>
 <br><br><br><br>
            

        </form>

    </div>
    <br><br>

</body>

</html>


@endsection