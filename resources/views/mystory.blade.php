@extends('layouts.raise')
@section('content')
<link rel="stylesheet" href="/css/form-basic.css">
<style>
     body {
        background-image: url('/images/bgg.jpg');
        background-size:  1700px 800px;
    }
    #x{
        font-size: 40px;
        color: black;
    }
</style>

<br><br>


<br><br><br><br><br><br>
<div class="row">
          <div class="col-md-12 text-center heading-section">
          @if($patient->count() <= 1)
            <span style="font-size: 20pt">Your On-Going Story</span>
          @else
          	<span style="font-size: 20pt">Your On-Going Stories</span>
          @endif
          <p>Monitor your goal and redeem anytime!</p>
          </div>

@foreach ($patient as $patients)
<div style="box-sizing: border-box; float: left; width: 24%; padding: 8px; margin-left: 12px;  content: ""; clear: both;
    display: table;">
 <div class="row row-bottom-padded-md" >
  <div class="col-md-12">
    <ul id="fh5co-portfolio-list">

      <li style="background-image: url( {{ url('storage/picture/'.$patients->filename) }}); width: 100%">
                <a href="http://localhost:8000/list/{{$patients['patientid']}}/view" class="color-3">
                  <div class="case-studies-summary">
                    <span>Give Love</span>
                    <h2>{{$patients->stories[0]->storytitle}}</h2><br>
                    
<br>
                    <div class="progress" style="width: 100%;margin-bottom: 0px">
    <div class="progress-bar" role="progressbar" aria-valuenow="{{$patients->TotalRedeem/$patients->goal*100}}" aria-valuemin="0" aria-valuemax="100" style="width:{{$patients->TotalRedeem/$patients->goal*100}}%">
    </div>
  </div>
  <br>
                <p style="text-align:center;font-size: 20px">P{{number_format($patients->TotalRedeem)}} raised of P{{number_format($patients->goal)}}</p>

                  </div>
                </a> 
            </li>

      </ul>
    </div>
  </div>
</div>

@endforeach  
</div>


@if($patient->count() == 0)
<center><div class="alert alert-info" style="width: 500px;">
   It seems that you dont have on-going story yet.
</div></center><br><br>
@endif  

<br><br><br><br><br><br>
<div class="row">
          <div class="col-md-12 text-center heading-section">
          @if($success->count() <= 1)
            <span style="font-size: 20pt">Your Success Story</span>
          @else
          	<span style="font-size: 20pt">Your Success Stories</span>
          @endif
          <p>Thank you for trusting us!</p>
          </div>

<!-- start div para sa stories -->

 @foreach ($success as $patients)


<div style="box-sizing: border-box; float: left; width: 24%; padding: 8px; margin-left: 12px;  content: ""; clear: both;
    display: table;">
 <div class="row row-bottom-padded-md" >
  <div class="col-md-12">
    <ul id="fh5co-portfolio-list">

      <li style="background-image: url( {{ url('storage/picture/'.$patients->filename) }}); width: 100%;">
                <a href="http://localhost:8000/list/{{$patients['patientid']}}/view" class="color-3">
                  <div class="case-studies-summary">
                    <span>Give Love</span>
                    <h2>{{$patients->stories[0]->storytitle}}</h2><br>
                    <div align="left"><input type="button" name="donate" value="+Read More" class="btn btn-primary-info"></div>
<br>
                    <div class="progress" style="width: 100%;margin-bottom: 0px">
    <div class="progress-bar" role="progressbar" aria-valuenow="{{$patients->TotalRedeem/$patients->goal*100}}" aria-valuemin="0" aria-valuemax="100" style="width:{{$patients->TotalRedeem/$patients->goal*100}}%">
    </div>
  </div>
  <br>
                <p style="text-align:center;font-size: 20px;">P{{number_format($patients->TotalRedeem)}} raised of P{{number_format($patients->goal)}}</p>

                  </div>
                </a> 
            </li>

      </ul>
    </div>
  </div>
</div>

@endforeach  
</div>

@if($success->count() == 0)
<center><div class="alert alert-info" style="width: 500px;">
   It seems that you dont have success story yet.
</div></center><br><br>
@endif


<br><br><br><br><br><br>
<div class="row">
          <div class="col-md-12 text-center heading-section">
            <span style="font-size: 20pt">Closed Stories</span>    
          </div>

@foreach ($close as $close)
<div style="box-sizing: border-box; float: left; width: 24%; padding: 8px; margin-left: 12px;  content: ""; clear: both;
    display: table;">
 <div class="row row-bottom-padded-md" >
  <div class="col-md-12">
    <ul id="fh5co-portfolio-list">

      <li style="background-image: url( {{ url('storage/picture/'.$close->filename) }}); width: 100%">
                <a href="http://localhost:8000/list/{{$close['patientid']}}/view" class="color-3">
                  <div class="case-studies-summary">
                    <span>Give Love</span>
                    <h2>{{$close->stories[0]->storytitle}}</h2><br>
                    
<br>
                    <div class="progress" style="width: 100%;margin-bottom: 0px">
    <div class="progress-bar" role="progressbar" aria-valuenow="{{$close->TotalRedeem/$close->goal*100}}" aria-valuemin="0" aria-valuemax="100" style="width:{{$close->TotalRedeem/$close->goal*100}}%">
    </div>
  </div>
  <br>
                <p style="text-align:center;font-size: 20px">P{{number_format($close->TotalRedeem)}} raised of P{{number_format($close->goal)}}</p>

                  </div>
                </a> 
            </li>

      </ul>
    </div>
  </div>
</div>

@endforeach  
</div>


@endsection