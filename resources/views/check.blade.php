<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>HealthAid - Dashboard</title>
	<link href="css/bootstrap.min.css" rel="stylesheet">
	<link href="css/font-awesome.min.css" rel="stylesheet">
	<link href="css/datepicker3.css" rel="stylesheet">
	<link href="css/styles.css" rel="stylesheet">

	<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	
	<!--Custom Font-->
	<link href="https://fonts.googleapis.com/css?family=Montserrat:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">
	<!-- modal -->
	<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
</head>

<body>
	<nav class="navbar navbar-custom navbar-fixed-top" role="navigation">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#sidebar-collapse"><span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span></button>
				<a class="navbar-brand" href="{{ url('/displayusers') }}"><span>Health</span>Aid</a>
				<ul class="nav navbar-top-links navbar-right">
          <li class="dropdown">
          <a href="{{ url('/approve') }}" style="width:140px;height:45px">
          <p>Story Request</p>
          <span class="label label-danger">
            <span id="admin_count" style="color:white"></span></span>
          </a>
          </li>

          <li class="dropdown">
          <a href="{{ url('/request') }}" style="width:165px;height:45px">
          <p>Redeem Request</p>
          <span class="label label-info">0</span>
          </a>
            
          </li>

          <li class="dropdown">
          <a href="{{ url('/check') }}" style="width:170px;height:45px"><p>Uncheck Vouchers</p>
          <span class="label label-success">
            <span id="voucher_count" style="color:white"></span>
          </span>
          </a>
          </li>
        </ul>
			</div>
		</div><!-- /.container-fluid -->
	</nav>
	<div id="sidebar-collapse" class="col-sm-3 col-lg-2 sidebar">
		<div class="profile-sidebar">
			<div class="profile-userpic">
				<img src="http://placehold.it/50/30a5ff/fff" class="img-responsive" alt="">
			</div>
			<div class="profile-usertitle">
				<div class="profile-usertitle-name">{{ Auth::user()->fname }}</div>
				<div class="profile-usertitle-status"><span class="indicator label-success"></span>Online</div>
			</div>
			<div class="clear"></div>
		</div>
		<div class="divider"></div>

		<ul class="nav menu">
			<li><a href="{{url('/displayusers')}}"><em class="fa fa-dashboard">&nbsp;</em> Dashboard</a></li>
			<li><a href="{{ url('/displaypatients') }}"><em class="fa fa-xl fa-users color-teal">&nbsp;</em> Patients</a></li>
			<li class="active"><a href="{{ url('/displaysponsors') }}"><em class="fa fa-xl fa-users color-orange">&nbsp;</em> Sponsors</a></li>

<!-- new -->
      <li><a href="{{ url('/criteria') }}"><em class="fa fa-xl fa-users color-orange">&nbsp;</em> Recommendation Criteria</a></li>
      <li><a href="{{ url('/reco') }}"><em class="fa fa-xl fa-users color-orange">&nbsp;</em> Recommended</a></li>

			<li><a class="fa fa-power-off" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">&nbsp;&nbsp;Logout
                                    </a>
                                    
                                  
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
            </li>
		</ul>
	</div><!--/.sidebar-->
		
	<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
		<div class="row">
			<ol class="breadcrumb">
				<li><a href="#">
					<em class="fa fa-home"></em>
				</a></li>
				<li class="active">Dashboard</li>
			</ol>
		</div><!--/.row-->
		
		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">Requests Voucher</h1>
			</div>
		</div><!--/.row-->


	<div class="panel panel-container">
	<div class="row">
	<table class="table">
	
	<th class="fixed-table-container">Account Name</th>
	<th class="fixed-table-container">Account Number</th>
	<th class="fixed-table-container">Deposited Amount</th>
	<th class="fixed-table-container">Deposit Slip</th>
	<th class="fixed-table-container">Amount</th>
	<th class="fixed-table-container"></th>
	<th class="fixed-table-container"></th> 
	<br>

<p id="sponsor">
	<form action="{{url('checked')}}" method="post">
{{csrf_field()}}
	@foreach ($bill as $sponsors)
		<tr class="fixed-table-container">

			<td class="fixed-table-container">
			{{$sponsors->accountname}}
			</td>
			<td class="fixed-table-container">
			{{$sponsors->accountnum}}
			</td>
			<td class="fixed-table-container">
			&#8369; {{number_format($sponsors->amountdeposited)}}
			</td>

			<td class="fixed-table-container">
			<img src="{{url('storage/picture/'.$sponsors->receipt)}}" style="width:100px;height:110px;cursor:zoom-in" onclick="document.getElementById('modal01').style.display='block'">
					<!-- modal image-->
   				<div id="modal01" class="w3-modal" onclick="this.style.display='none'">
   				 <span class="w3-button w3-hover-red w3-xlarge w3-display-topright">&times;</span>
    			 <div class="w3-modal-content w3-animate-zoom">
      			 <img src="{{url('storage/picture/'.$sponsors->receipt)}}" style="width:100%;height: 100%">
    			 </div>
  				</div>
			</td>

			<td class="fixed-table-container">
				<input type="number" name="bankmoney" placeholder="Amount" required=>
			</td>

			<td class="fixed-table-container" align="center">             
				<button id="check" value="check" name="submit" class="btn btn-primary">Confirm</button>
			</td>
			<td class="fixed-table-container" align="center">   
				<select name="message" style="border-color:#1FB264">
                    <option value="Receipt Used">Deposit Slip Used</option>
                    <option value="Payment not received">Payment not received</option> 
                    <option value="Invalid Receipt">Invalid Deposit Slip</option>       
                </select>          
				<button value="deny" name="submit" class="btn btn-danger">Deny</button>
			</td>

			<input type="hidden" name="userid" value="{{$sponsors->userid}}">
			<input type="hidden" name="billing_id" value="{{$sponsors->billing_id}}">

</tr>
			
</form>
	
@endforeach
</p>
</table><br><br>

				
			</div><!--/.row-->
		</div>
		
			</div><!--/.col-->
			
		</div><!--/.row-->
	</div>	<!--/.main-->
	
	<script src="js/jquery-1.11.1.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/chart.min.js"></script>
	<script src="js/chart-data.js"></script>
	<script src="js/easypiechart.js"></script>
	<script src="js/easypiechart-data.js"></script>
	<script src="js/bootstrap-datepicker.js"></script>
	<script src="js/custom.js"></script>
	<script>
		window.onload = function () {
	var chart1 = document.getElementById("line-chart").getContext("2d");
	window.myLine = new Chart(chart1).Line(lineChartData, {
	responsive: true,
	scaleLineColor: "rgba(0,0,0,.2)",
	scaleGridLineColor: "rgba(0,0,0,.05)",
	scaleFontColor: "#c5c7cc"
	});
};
	</script>

	<!-- js for notif -->
<script>
  $(document).ready(function (){
     setInterval(function(){ 
       $.ajax({
            type: 'GET',
            url: '/adminStoryCount',
            success:function(data){
                document.getElementById('admin_count').innerHTML  = data;
                console.log(data);
            },
            error:function(){
              console.log("");
            }
        });

       $.ajax({
            type: 'GET',
            url: '/adminVoucherCount',
            success:function(data){
                document.getElementById('voucher_count').innerHTML  = data;
                console.log(data);
            },
            error:function(){
              console.log("");
            }
        });

      }, 1000);
      });
</script>
