@extends('layouts.historyui')
@section('content')     



<style>
    body {
       background-image: url('/images/bgg.png');
        top right no-repeat; 
        background-attachment:fixed;
        background-size: cover;
        margin-top: 0px;
    }
</style>

<style>
table {
    border-collapse: collapse;
    width: 100%;
}

th, td {
    padding: 8px;
    text-align: left;
    border-bottom: 1px solid #ddd;
}

tr:hover {background-color:#f5f5f5;}
</style>


<div style="    margin-right: 110px; margin-left: 70px; margin-top: 10px; border-radius: 15px 15px;">
<div class="limiter">
        
                
    <div class="wrap-table100"><p style="font-size: 15pt; font-family: Lato-Bold; color: black">Redeem History</p>
        <div class="table100 ver5 m-b-110">
                    <div class="table100-head">
                        <table>
                            <thead>
                                <tr>
                                    <th>Date</th>
                                    <!-- <th>Name</th> -->
                                    <th>Amount Redeemed</th>
                                    <th>Status</th>
                                </tr>
                           
                    
    @foreach ($redeem as $redeem)
                    
                                <tr>
                                    <td>{{$redeem->created_at->format('F d, Y')}}</td>  
                                    <td>{{$redeem->amountRedeemed}}</td>
                                    <td>{{$redeem->redeemStatus}}</td>
                                </tr>
                                
    @endforeach
                            </tbody>
                        </table>
                </div>
            </div>  
        </div>
    </div>
</div>

<br><br>
<!--  -->
Total Redeem: {{ $rdm }}





<br>
@endsection
