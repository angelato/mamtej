<?php 
use App\Redeem;?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>HELPXP - Dashboard</title>
	<link href="css/bootstrap.min.css" rel="stylesheet">
	<link href="css/font-awesome.min.css" rel="stylesheet">
	<link href="css/datepicker3.css" rel="stylesheet">
	<link href="css/styles.css" rel="stylesheet">

	<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

	<script src="https://code.jquery.com/jquery-2.1.4.js"
  integrity="sha256-siFczlgw4jULnUICcdm9gjQPZkw/YPDqhQ9+nAOScE4="
  crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.5/jspdf.min.js"></script> 
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.0.272/jspdf.debug.js"></script>
	
	<!--Custom Font-->
	<link href="https://fonts.googleapis.com/css?family=Montserrat:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">
	<!--[if lt IE 9]>
	<script src="js/html5shiv.js"></script>
	<script src="js/respond.min.js"></script>
	<![endif]-->
<style>
	img {
    transition:transform 0.25s ease;
	}

	img:hover {
    -webkit-transform:scale(1.5); /* or some other value */
    transform:scale(1.5);
	}

	html,
body {
  height: 100%;
}

.container {
  display: table;
  width: 100%;
  height: 100%;

}

.interior {
  display: table-cell;
  vertical-align: middle;
  text-align: center;
}

.btn {
  background-color: #ff471a;
  padding: 1em 3em;
  border-radius: 3px;
  text-decoration: none;
}

.btn1 {
	background-color: #99c2ff;
  padding: 1em 3em;
  border-radius: 3px;
  text-decoration: none;
}

.btn2 {
 background-color: #ff8080;
  padding: 1em 3em;
  border-radius: 3px;
  text-decoration: none;
}

.modal-window {
  position: fixed;
  background-color: rgba(255, 255, 255, 0.15);
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;
  z-index: 999;
  opacity: 0;
  pointer-events: none;
  -webkit-transition: all 0.3s;
  -moz-transition: all 0.3s;
  transition: all 0.3s;
}

.modal-window:target {
  opacity: 1;
  pointer-events: auto;
}

.modal-window>div {
  width: 600px;
  position: relative;
  margin: 10% auto;
  padding: 2rem;
  background: rgba(139, 130, 125, 0.54);
  color: #444;
}

.modal-window header {
  font-weight: bold;
}


.modal-close {
  color: #aaa;
  line-height: 50px;
  font-size: 80%;
  position: absolute;
  right: 0;
  text-align: center;
  top: 0;
  width: 70px;
  text-decoration: none;
}

.modal-close:hover {
  color: #000;
}

</style>

</head>
<body>
	<nav class="navbar navbar-custom navbar-fixed-top" role="navigation">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#sidebar-collapse"><span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span></button>
				<a class="navbar-brand" href="{{ url('/displayusers') }}"><span>Health</span>Aid</a>
							<!-- notification -->
				<ul class="nav navbar-top-links navbar-right">
					<li class="dropdown">
					<a href="{{ url('/approve') }}" style="width:140px;height:45px">
					<p>Story Request</p>
					<span class="label label-danger">
						<span id="admin_count" style="color:white"></span></span>
					</a>
					</li>

					<li class="dropdown">
					<a href="{{ url('/request') }}" style="width:165px;height:45px">
					<p>Redeem Request</p>
					<span class="label label-info">
						<span id="redeem_count" style="color:white"></span>
					</span>
					</a>	
					</li>

					<li class="dropdown">
					<a href="{{ url('/check') }}" style="width:170px;height:45px"><p>Uncheck Vouchers</p>
					<span class="label label-success">
						<span id="voucher_count" style="color:white"></span>
					</span>
					</a>
					</li>

				</ul>
			</div>
		</div><!-- /.container-fluid -->
	</nav>
	<div id="sidebar-collapse" class="col-sm-3 col-lg-2 sidebar">
	<div class="profile-sidebar">
		@if(Auth::user()->username == "gela") 
			<div class="profile-userpic">
				<img src={{('/images/gelprofile.jpg')}} class="img-responsive" alt="">
			</div>
		@elseif(Auth::user()->username == "camjoy") 
			<div class="profile-userpic">
				<img src={{('/images/camprofile.jpg')}} class="img-responsive" alt="">
			</div>
		@endif
			<div class="profile-usertitle">
				<div class="profile-usertitle-name">{{ Auth::user()->fname }}</div>
				<div class="profile-usertitle-status"><span class="indicator label-success"></span>Administrator</div>
			</div>
			<div class="clear"></div>
		</div>
		<div class="divider"></div>

		<ul class="nav menu">
			<li><a href="{{url('/displayusers')}}"><em class="fa fa-dashboard">&nbsp;</em> Dashboard</a></li>
			<li class="active"><a href="{{ url('/displaypatients') }}"><em class="fa fa-users">&nbsp;</em> Patients</a></li>
			<li><a href="{{ url('/displaysponsors') }}"><em class="fa fa-users">&nbsp;</em> Sponsors</a></li>

			<!-- new -->
      <li>
      <a href="{{ url('/criteria') }}"><em class="fa fa-bar-chart">&nbsp;</em> Recommendation Criteria</a>
      </li>
      
      <li><a href="{{ url('/reco') }}"><em class="fa fa-users">&nbsp;</em> Recommended</a>
      </li>

			<li><a class="fa fa-power-off" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">&nbsp;&nbsp;Logout
                                    </a>
                                    
                                  
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
            </li>
		</ul>
	</div><!--/.sidebar-->
		
	<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
		<div class="row">
			<ol class="breadcrumb">
				<li><a href="#">
					<em class="fa fa-home"></em>
				</a></li>
				<li class="active">Dashboard</li>
			</ol>
		</div><!--/.row-->
		
		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">Patients</h1>
			</div>
		</div><!--/.row-->


	<div class="panel panel-container">
	<div class="row">


	<div class="form-group pull-center" style="max-width: 500px; margin-left: 40px;">
    <input type="search" name="search" id="search"  onkeyup="search()" class="search form-control" placeholder="Search">
	</div>

	<table class="table">

	<th class="fixed-table-container">Profile</th>
	<th class="fixed-table-container">Story Title</th>
	<th class="fixed-table-container">User</th>
	<th class="fixed-table-container">Patient Name</th>
	<th class="fixed-table-container">Amount Goal</th>
	<th class="fixed-table-container">Raised Funds</th>
	<th class="fixed-table-container">Ongoing Amount</th>
	<th class="fixed-table-container">RedeemStatus</th>
	<th class="fixed-table-container">Action</th>
	<!-- <th>Illness</th>
	<th>Story</th>
	<th>Amount</th> --><br>


<tbody id="patient">
	@foreach ($redeem as $rdm)
	@if($rdm->action != "Released" || $rdm->story->patient->action != "Released")
		<tr class="fixed-table-container">
			<td class="fixed-table-container"><img src="{{  url('storage/picture/'.$rdm->story->patient->filename) }}" width="100px" height="100px" /></td>
			<td class="fixed-table-container">{{$rdm->story->storytitle}}</td>
			<td class="fixed-table-container">{{$rdm->story->patient->userName->fname}} {{$rdm->story->patient->userName->lname}}</td>
			<td class="fixed-table-container">{{$rdm->story->patient->patientname}}</td>
			<td class="fixed-table-container">{{$rdm->story->patient->goal}}</td>
			<td class="fixed-table-container">{{$rdm->story->patient->TotalRedeem}}</td>
			<td class="fixed-table-container">{{$rdm->story->patient->newgoal}}</td>
			<td class="fixed-table-container">{{$rdm->story->patient->redeemStatus}}</td>
	<!-- <form action="{{url('/requests')}}" method="POST">
		{{csrf_field()}} -->
			<td class="fixed-table-container" align="center">
				<div class="container">
  				<div class="interior" style="color: #fff">
    			<a class="btn" href="#{{$rdm->patientid}}">Release</a>
  				</div>
				</div>

				<div id="{{$rdm->patientid}}" class="modal-window">
  				<div>
    		<a href="#modal-close" title="Close" class="modal-close">Close</a>
    		<div class="main-content" >

        <form class="form-basic" action="{{url('/requests')}}" method="post" enctype="multipart/form-data" style="background-color: #F9F9F9">
        {{csrf_field()}}

       
        <input type="hidden" name="patientid" value="{{$rdm->patientid}}">
 
        <input type="hidden" name="redeemid" value="{{$rdm->redeemid}}">

            <div class="form-title-row">
                <h2><strong>Voucher Details!</strong></h2>
            </div>
            <table border="0" cellpadding="20px">
            <tr>
            <div class="form-row">
                <label>
                    <td><strong>Date</strong></td>
                   <td> <input type="text" value="{{$rdm->updated_at->format('F d, Y')}}"></td>
                </label>
             </tr>
            </div>

 			<tr>
                  <td><strong>Beneficiary Name</strong></td> 
                   <td> <input type="text" value="{{$rdm->story->patient->patientname}}"></td>
                
			</tr>
            
			<tr>
                <label>
                    <td><strong>Amount Redeemed</strong></td>
                    <td><input type="text" name="amount" value="{{$rdm->amountRedeemed}}"></td>
                </label>
             </tr>
            </table><br><br>

            <div class="form-title-row">
                <h2><strong>Recipient Details!</strong></h2>
            </div>
            <table border="0" cellpadding="20px">
            <tr>
            <div class="form-row">
            
 			<tr>
                  <td><strong>Recipient Name</strong></td> 
                   <td> <input type="text" name="recipient" id="recipient" ></td>
                <input type="hidden" name="patientid" value="{{$rdm->patientid}}">
			</tr>

			<tr>
                <label>
                    <td><strong>Amount Redeemed</strong></td>
                    <td><input type="text" name="title" value="{{$rdm->amountRedeemed}}"></td>
                </label>
             </tr>
            </table><br><br>

            <div>
                <label>
                    <span><input type="submit" class="btn1 btn-primary" value="Submit"></a>
                    <input type="reset" class="btn2 btn-danger" value="Reset"></span>
                </label>
            </div>

        </form>

    </div>
				</div>
			</div>
			</td>
			<!--  -->
	<!-- </form> -->

		</tr>
@endif
@endforeach

</tbody>
</table>
				
	
				
			</div><!--/.row-->
		</div>
		
			</div><!--/.col-->
			
		</div><!--/.row-->
	</div>	<!--/.main-->


</body>
</html>

<script>


function  search(argument) {
	// body...
	var d = document.getElementById('search').value;
	if(d == ''){
		d = 'all';
	}
	$.ajax({
            type: 'GET',
            url: '/searchredeem/'+d,
            success:function(data){
                document.getElementById('patient').innerHTML  = data;
                console.log(data);
            },
            error:function(){
              console.log("");
            }
        });
}

function updateState(context){
     context.setAttribute('disabled',true)
    }
</script>

	
	
	<script src="js/jquery-1.11.1.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/chart.min.js"></script>
	<script src="js/chart-data.js"></script>
	<script src="js/easypiechart.js"></script>
	<script src="js/easypiechart-data.js"></script>
	<script src="js/bootstrap-datepicker.js"></script>
	<script src="js/custom.js"></script>
	<script>
		window.onload = function () {
	var chart1 = document.getElementById("line-chart").getContext("2d");
	window.myLine = new Chart(chart1).Line(lineChartData, {
	responsive: true,
	scaleLineColor: "rgba(0,0,0,.2)",
	scaleGridLineColor: "rgba(0,0,0,.05)",
	scaleFontColor: "#c5c7cc"
	});
};
	</script>

	<!-- js for notif -->
<script>
	$(document).ready(function (){
     setInterval(function(){ 
       $.ajax({
            type: 'GET',
            url: '/adminStoryCount',
            success:function(data){
                document.getElementById('admin_count').innerHTML  = data;
                console.log(data);
            },
            error:function(){
              console.log("");
            }
        });

       $.ajax({
            type: 'GET',
            url: '/adminVoucherCount',
            success:function(data){
                document.getElementById('voucher_count').innerHTML  = data;
                console.log(data);
            },
            error:function(){
              console.log("");
            }
        });

        $.ajax({
            type: 'GET',
            url: '/redeemCount',
            success:function(data){
                document.getElementById('redeem_count').innerHTML  = data;
                console.log(data);
            },
            error:function(){
              console.log("");
            }
        });


      }, 300);
      });

</script>

	

		




