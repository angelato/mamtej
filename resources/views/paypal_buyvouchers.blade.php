@extends('layouts.voucher')

 
@section('content')


<head>

    <!-- <link rel="stylesheet" href="/css/form-basic.css"> -->
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
    <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>

    <!-- progress form -->
    <link rel="stylesheet" href="/css/fileupload2.css">
    <script src="/js/fileupload2.js"></script>
    <!-- file upload -->
    <link rel="stylesheet" href="/css/fileupload.css">
    <script src="/js/fileupload.js"></script>
    <link rel="stylesheet" href="/css/alert.css">

<style>
h1 {
    display: inline-block;
    box-sizing: border-box;
    color:  #4c565e;
    font-size: 24px;
    padding: 0 10px 15px;
    border-bottom: 2px solid #6caee0;
    margin: 0;
}
* {
  box-sizing: border-box;
}

/* Create two equal columns that floats next to each other */
.column {
  float: left;
  width: 50%;
  padding: 10px;
  height: 300px; /* Should be removed. Only for demonstration */
}

/* Clear floats after the columns */
.row:after {
  content: "";
  display: table;
  clear: both;
}
    </style>

</head>

<br><br><br><br><br><br>

<!-- start -->
<section class="form-box">
            <div class="container" style="width:100%">
                
                <div class="row">
                    <div class="col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2 col-lg-6 col-lg-offset-3 form-wizard">
                    
                        <!-- Form Wizard -->
                        <form  role="form" class="form-basic" action="{{url('/voucherpaypal')}}" method="post" enctype="multipart/form-data">
            {{csrf_field()}}

                            <h1 style="color:red">BUY VOUCHERS HERE</h1>
                            
                            <!-- Form progress -->
                            <div class="form-wizard-steps form-wizard-tolal-steps-4">
                                <div class="form-wizard-progress">
                                    <div class="form-wizard-progress-line" data-now-value="12.25" data-number-of-steps="4" style="width: 12.25%;"></div>
                                </div>
                                <!-- Step 1 -->
                                <div class="form-wizard-step active" style="width: 100%">
                                    <div class="form-wizard-step-icon"><i class="fa fa-user" aria-hidden="true" style="padding-top:10px"></i></div>
                                    <p>Vouchers</p>
                                </div>
                                <!-- Step 1 -->
                            </div>
                            <!-- Form progress -->
                            
                            
                <!-- Form Step 1 -->
            <fieldset>

    <h4>Voucher's Denomination: <span>Step 1 - 1</span></h4>
    <br><br>

    <div class="notice info">
        <p style="font-size:14pt;text-align:center">This is an info notice, you can only buy voucher/s worth &#8369;{{Auth::user()->paypal_balance}}
    </div>

    <center><u><div id="totalVoucher" style="text-align:center;width:500px;height:100px;font-size: 20pt;"></div></u></center>
                            
   <div style="text-align:center">
        <label>
            <img src="/images/v_100.JPG" height="28%" width="80%"/><br>
            <p style="text-align:center"><input type=number min="0" name='qty100' id='qty100' style="width: 100px"  onclick="getTotal()" /></p>
        </label>
           
        <label>
            <img src="/images/v_500.JPG" height="28%" width="80%" /><br>
            <p style="text-align:center"><input type='number' min="0" name='qty500' id='qty500' style="width: 100px" onclick="getTotal()" /></p>
        </label>
            
        <label>
            <img src="/images/v_1000.JPG" height="28%" width="80%" /><br>
            <p style="text-align:center"><input type='number' min="0" name='qty1000' id='qty1000' style="width: 100px" onclick="getTotal()" /></p>
        </label>
           
        <label>
            <img src="/images/v_5000.JPG" height="28%" width="80%" /><br>
            <p style="text-align:center"><input type='number' min="0" name='qty5000' id='qty5000' style="width: 100px" onclick="getTotal()" /></p>
        </label>
    </div>        
                                <div class="form-wizard-buttons">
                                    <button type="submit" class="btn btn-submit">Submit</button>
                                </div>
                            </fieldset>
                            <!-- Form Step 1 -->
                        
                        </form>
                        <!-- Form Wizard -->
                    </div>
                </div>
                    
            </div>
        </section>
<br><br><br><br><br>
<!-- end -->


@if('paypal_balance' >= 'total')
<script>alert("Success! Thank you for trusting us.")</script>
@endif

<!-- @if('paypal_balance' < 'total')
<script>alert("You don't have enough PayPal balance.")</script>
@endif -->

@if(Session::has('error'))
    <script>
        alert('Cant buy with that quantity');
    </script>
@elseif(Session::has('success'))
    <script>
        alert('Thankyou for purchasing');
    </script>
@elseif(Session::has('notenough'))
    <script>
        alert('Not enough balance in your PayPal account');
    </script>
@elseif(Session::has('nonzero'))
    <script>
        alert('No voucher purchase');
    </script>
@endif


@if($message = Session::get('success_pay'))
<script>
        alert('Payment Success');
</script>
<?php Session::forget('success_pay'); ?> 
@endif

@if($message = Session::get('error_pay'))
<script>
        alert('Payment Failed');
</script>
<?php Session::forget('error_pay'); ?> 
@endif
<script>
     
function getTotal() {
    var v1 = document.getElementById("qty100").value;
    var v2 = document.getElementById('qty500').value;
    var v3 = document.getElementById('qty1000').value;
    var v4 = document.getElementById('qty5000').value;

    var total = (v1 * 100) + (v2 * 500) + (v3 * 1000) + (v4 * 5000);
    document.getElementById("totalVoucher").innerHTML =  "Total Vouchers &#8369 "+ total;
}
</script>


<br><br>
@endsection