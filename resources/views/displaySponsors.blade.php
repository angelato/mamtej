<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>HealthAid - Dashboard</title>
	<link href="css/bootstrap.min.css" rel="stylesheet">
	<link href="css/font-awesome.min.css" rel="stylesheet">
	<link href="css/datepicker3.css" rel="stylesheet">
	<link href="css/styles.css" rel="stylesheet">
	<link href="css/adminnotif.css" rel="stylesheet">

	<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<!--Custom Font-->
	<link href="https://fonts.googleapis.com/css?family=Montserrat:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

	<!--[if lt IE 9]>
	<script src="js/html5shiv.js"></script>
	<script src="js/respond.min.js"></script>
	<![endif]-->
</head>

<body>
	<nav class="navbar navbar-custom navbar-fixed-top" role="navigation">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#sidebar-collapse"><span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span></button>
				<a class="navbar-brand" href="{{ url('/displayusers') }}"><span>Health</span>Aid</a>
<!-- notification -->
				<ul class="nav navbar-top-links navbar-right">
					<li class="dropdown">
					<a href="{{ url('/approve') }}" style="width:140px;height:45px">
					<p>Story Request</p>
					<span class="label label-danger">
						<span id="admin_count" style="color:white"></span></span>
					</a>
					</li>

					<li class="dropdown">
					<a href="{{ url('/request') }}" style="width:165px;height:45px">
					<p>Redeem Request</p>
					<span class="label label-info">
						<span id="redeem_count" style="color:white"></span>
					</span>
					</a>	
					</li>

					<li class="dropdown">
					<a href="{{ url('/check') }}" style="width:170px;height:45px"><p>Uncheck Vouchers</p>
					<span class="label label-success">
						<span id="voucher_count" style="color:white"></span>
					</span>
					</a>
					</li>
				</ul>

			</div>
		</div><!-- /.container-fluid -->
	</nav>
	<div id="sidebar-collapse" class="col-sm-3 col-lg-2 sidebar">
		<div class="profile-sidebar">
		@if(Auth::user()->username == "gela") 
			<div class="profile-userpic">
				<img src={{('/images/gelprofile.jpg')}} class="img-responsive" alt="">
			</div>
		@elseif(Auth::user()->username == "camjoy") 
			<div class="profile-userpic">
				<img src={{('/images/camprofile.jpg')}} class="img-responsive" alt="">
			</div>
		@endif
			<div class="profile-usertitle">
				<div class="profile-usertitle-name">{{ Auth::user()->fname }}</div>
				<div class="profile-usertitle-status"><span class="indicator label-success"></span>Administrator</div>
			</div>
			<div class="clear"></div>
		</div>
		<div class="divider"></div>

		<ul class="nav menu">
			<li><a href="{{url('/displayusers')}}"><em class="fa fa-dashboard">&nbsp;</em> Dashboard</a></li>
			<li><a href="{{ url('/displaypatients') }}"><em class="fa fa-xl fa-users">&nbsp;</em> Patients</a></li>
			<li class="active"><a href="{{ url('/displaysponsors') }}"><em class="fa fa-xl fa-users">&nbsp;</em> Sponsors</a></li>

   <!-- new -->
      <li><a href="{{ url('/criteria') }}"><em class="fa fa-bar-chart">&nbsp;</em> Recommendation Criteria</a></li>
      <li><a href="{{ url('/reco') }}"><em class="fa fa-xl fa-users">&nbsp;</em> Recommended</a></li>
			
			<li><a class="fa fa-power-off" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">&nbsp;&nbsp;Logout
                                    </a>
                                    
                                  
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
            </li>
		</ul>
	</div><!--/.sidebar-->
		
	<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
		<div class="row">
			<ol class="breadcrumb">
				<li><a href="{{ url('displayusers') }}">
					<em class="fa fa-home"></em>
				</a></li>
				<li class="active">Dashboard</li>
			</ol>
		</div><!--/.row-->
		
		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">List of Sponsors <p>(BENEFACTORS)</p></h1>
			</div>
		</div><!--/.row-->
		
		
		<div class="panel panel-container">
			<div class="row">
	<p align="center">
	<form action="{{ url('/filter') }}" method="post">
	{{csrf_field()}}
	<center>From: <input type="date" name="from" id="from">
	To: <input type="date" name="to" id="to">
	<input type="submit" name="filter" value="Filter" class="btn btn-info"></center>
	<!-- <a href="{{ url('/filter') }}" class="btn btn-info" name="filter" id="filter">Filter</a></p> -->
	</form>
			<table class="table">
	
	<th class="fixed-table-container">Name</th>
	<th class="fixed-table-container">Username</th>
	<th class="fixed-table-container">Email</th>
	<th class="fixed-table-container">Address</th>
	<th class="fixed-table-container">Birthdate</th>
	<th class="fixed-table-container">Contact</th>
	<th class="fixed-table-container">Sponsored Patient</th>
	<th class="fixed-table-container">HealthAid Donations</th>
	
	<!-- <th>Illness</th>
	<th>Story</th>
	<th>Amount</th> --><br>


	
	<?php 
	foreach ($users as $sponsors) {
		?>
		<tr class="fixed-table-container">
			<td class="fixed-table-container"><?php ?><?php print_r($sponsors->user->fname) ?> <?php print_r($sponsors->user->lname) ?></td>
			<td class="fixed-table-container"><?php ?><?php print_r($sponsors->user->username) ?></td>
			<td class="fixed-table-container"><?php ?><?php print_r($sponsors->user->email) ?></td>
			<td class="fixed-table-container"><?php ?><?php print_r($sponsors->user->address) ?></td>
			<td class="fixed-table-container"><?php ?><?php print_r($sponsors->user->birthdate) ?></td>
			<td class="fixed-table-container"><?php ?><?php print_r($sponsors->user->contact) ?></td>
			<td class="fixed-table-container" align="center"><a href="{{ url('/sponsoredpatient') }}/{{$sponsors->userid}}" class="btn btn-success">Patient Donations</td>
			<td class="fixed-table-container" align="center"><a href="{{ url('/sponsoredhelpxp') }}/{{$sponsors->userid}}" class="btn btn-warning">HealthAid Donations</td>
			<!-- <td class="fixed-table-container" align="center"><a href="{{ url('/displayvouchers') }}/{{$sponsors->userid}}" class="btn btn-success">Vouchers</td> -->

			<!--  -->
		</tr>
		<?php
	} ?>
</table>
				
			</div><!--/.row-->
		</div>
		
			</div><!--/.col-->
			
		</div><!--/.row-->
	</div>	<!--/.main-->
	
	<script src="js/jquery-1.11.1.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/chart.min.js"></script>
	<script src="js/chart-data.js"></script>
	<script src="js/easypiechart.js"></script>
	<script src="js/easypiechart-data.js"></script>
	<script src="js/bootstrap-datepicker.js"></script>
	<script src="js/custom.js"></script>
	<script>
		window.onload = function () {
	var chart1 = document.getElementById("line-chart").getContext("2d");
	window.myLine = new Chart(chart1).Line(lineChartData, {
	responsive: true,
	scaleLineColor: "rgba(0,0,0,.2)",
	scaleGridLineColor: "rgba(0,0,0,.05)",
	scaleFontColor: "#c5c7cc"
	});
};
	</script>

	<!-- js for notif -->
<script>
	$(document).ready(function (){
     setInterval(function(){ 
       $.ajax({
            type: 'GET',
            url: '/adminStoryCount',
            success:function(data){
                document.getElementById('admin_count').innerHTML  = data;
                console.log(data);
            },
            error:function(){
              console.log("");
            }
        });

       $.ajax({
            type: 'GET',
            url: '/adminVoucherCount',
            success:function(data){
                document.getElementById('voucher_count').innerHTML  = data;
                console.log(data);
            },
            error:function(){
              console.log("");
            }
        });

       $.ajax({
            type: 'GET',
            url: '/redeemCount',
            success:function(data){
                document.getElementById('redeem_count').innerHTML  = data;
                console.log(data);
            },
            error:function(){
              console.log("");
            }
        });

      }, 1000);
      });
</script>
		

	
