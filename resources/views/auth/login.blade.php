@extends('layouts.header')

@section('content')

    <style>
    .containerr {
    background: linear-gradient(
    to right,
    #fff 0%,
    #fff 50%,
    #f9f9f9 50%,
    #f9f9f9 100%
    );

    }
    </style>

<br><br><br><br>
<div class="containerr">

<div style="float: left; overflow: auto;"><br> 
<p style="color:  #333333; font-size: 35pt; letter-spacing: 0.1em; margin-top: 5px; margin-left: 70px; text-decoration: underline; text-decoration-color: #ff5c33"><strong>BIG</strong> OR <strong>SMALL</strong><br> YOUR CAUSE MATTERS.</p><br>     
    <img style="margin-left: 50px; width: 700px; max-width: 700px; height: 420px;" src="images/collage.png"><br>    
    

</div>
<br>    
<div style="margin-left:57%; overflow: auto; margin-right: 0; padding-right: 0; margin-top: 130px; ">
    <div class="row justify-content-center">
      <h2 align="center">{{ __('Sign in') }}</h2></div><br><br>
<br>
                <div class="card-body">
                    <form method="POST" action="{{ route('login') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="email" class="col-sm-4 col-form-label text-md-right">{{ __('Username') }}</label>

                            <div class="col-md-6">
                               <input id="email" type="text" class="form-control" name="email" value="{{ old('email') }}" required autofocus>

                                @if ($errors->has('email'))
                                    <script>
                                        alert('Incorrect Username/Password');
                                    </script>
                                @endif
                            </div>
                        </div><br><br>

                        <div class="form-group row" >
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                                @if ($errors->has('password'))
                                    <script>
                                        alert('Incorrect Username/Password');
                                    </script>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-6 offset-md-4">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> {{ __('Remember Me') }}
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4" style="margin-left: 100px">
                                <button type="submit" class="btn btn-primary" >
                                    {{ __('Login') }}
                                </button>

                                <a class="btn btn-link" href="{{ route('password.request') }}">
                                    {{ __('Forgot Your Password?') }}
                                </a>
                                <br><br><br><br><br><br><br><br><br><br>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

                



@endsection


