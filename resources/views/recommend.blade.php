<?php 
use App\Patient;
use App\Recopercentage;?>

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>HealthAid - Dashboard</title>
  <link href="css/bootstrap.min.css" rel="stylesheet">
  <link href="css/font-awesome.min.css" rel="stylesheet">
  <link href="css/datepicker3.css" rel="stylesheet">
  <link href="css/styles.css" rel="stylesheet">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/criteria.css">
    <link rel="stylesheet" href="css/criteriamodal.css">
  <!--Custom Font-->
  <link href="https://fonts.googleapis.com/css?family=Montserrat:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">
  <!-- modal -->
  
  <!-- Modernizr JS -->
  <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="/css/alert.css">
  <link rel="stylesheet" href="/css/reco.css">
</head>
<style>
    #x{
        font-size: 25px;
        color: black;
    }
    input{
      color: black;
      width: 100px;
    }
</style>

<body>
  <nav class="navbar navbar-custom navbar-fixed-top" role="navigation">
    <div class="container-fluid">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#sidebar-collapse"><span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span></button>
        <a class="navbar-brand" href="{{ url('/displayusers') }}"><span>Health</span>Aid</a>
<!-- notification -->
        <ul class="nav navbar-top-links navbar-right">
          <li class="dropdown">
          <a href="{{ url('/approve') }}" style="width:140px;height:45px">
          <p>Story Request</p>
          <span class="label label-danger">
            <span id="admin_count" style="color:white"></span></span>
          </a>
          </li>

            <li class="dropdown">
          <a href="{{ url('/request') }}" style="width:165px;height:45px">
          <p>Redeem Request</p>
          <span class="label label-info">
            <span id="redeem_count" style="color:white"></span>
          </span>
          </a>  
          </li>

          <li class="dropdown">
          <a href="{{ url('/check') }}" style="width:170px;height:45px"><p>Uncheck Vouchers</p>
          <span class="label label-success">
            <span id="voucher_count" style="color:white"></span>
          </span>
          </a>
          </li>
        </ul>

      </div>
    </div><!-- /.container-fluid -->
  </nav>
  <div id="sidebar-collapse" class="col-sm-3 col-lg-2 sidebar">
    <div class="profile-sidebar">
    @if(Auth::user()->username == "gela") 
      <div class="profile-userpic">
        <img src={{('/images/gelprofile.jpg')}} class="img-responsive" alt="">
      </div>
    @elseif(Auth::user()->username == "camjoy") 
      <div class="profile-userpic">
        <img src={{('/images/camprofile.jpg')}} class="img-responsive" alt="">
      </div>
    @endif
      <div class="profile-usertitle">
        <div class="profile-usertitle-name">{{ Auth::user()->fname }}</div>
        <div class="profile-usertitle-status"><span class="indicator label-success"></span>Administrator</div>
      </div>
      <div class="clear"></div>
    </div>
    <div class="divider"></div>

    <ul class="nav menu">
      <li ><a href="{{ url('/displayusers') }}"><em class="fa fa-dashboard">&nbsp;</em> Dashboard</a></li>
      <li>
        <a href="{{ url('/displaypatients') }}"><em class="fa fa-xl fa-users">&nbsp;</em> Patients</a>
      </li>
      <li><a href="{{ url('/displaysponsors') }}"><em class="fa fa-xl fa-users">&nbsp;</em> Sponsors</a></li>

      <!-- new -->
      <li><a href="{{ url('/criteria') }}"><em class="fa fa-bar-chart">&nbsp;</em> Recommendation Criteria</a></li>
      <li class="active"><a href="{{ url('/reco') }}"><em class="fa fa-xl fa-users">&nbsp;</em> Recommended</a></li>



      <li><a class="fa fa-power-off" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">&nbsp;&nbsp;Logout
                                    </a>
                                    
                                  
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
            </li>
    </ul>
  </div><!--/.sidebar-->
    
  <div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
    <div class="row">
      <ol class="breadcrumb">
        <li><a href="{{ url('displayusers') }}">
          <em class="fa fa-home"></em>
        </a></li>
        <li class="active">Recommended</li>
      </ol>
    </div><!--/.row-->
    
    <div class="row">
      <div class="col-lg-12">
        <h1 class="page-header" style="float:left">Recommendation</h1>

<h1 class="page-header" style="float:right;padding-right:35px;font-size:20px">HealthAid's current money &#8369; {{number_format($money)}}</h1>

      </div>
    </div>


@if($money >= 15000)
<form method="post" action="{{ url('donatereco') }}">
{{csrf_field()}}
<div class="notice info" style="height:60px;text-align:center"><p style="color:#80858e;font-size:12pt">This is a an info notice, your system is now capable of donating to your recommended patients.<button id="shows">Recommend</button>
<input type="hidden" name="recomoney" value="{{$money}}">
</form>
</p></div>
@endif



<?php 
$a = Recopercentage::get()->last();
 ?>

<h1 class="page-header" style="font-size:30px">Money for recommendation &#8369; {{number_format($a->percentage)}}</h1>

@if($a->percentage != 0)
<div id="list">
<button id="show" style="width:20%">Show Recommended List</button>
<button id="hide">Hide</button>
</div>
@endif


<!-- new -->

<script>
$(document).ready(function(){
    $("#hide").click(function(){
        $("#recommend").hide();
    });
    $("#show").click(function(){
        $("#recommend").show();
    });
     $("#shows").click(function(){
        $("#list").show();
    });
});
</script>

<!-- new -->

<!-- last -->


<!-- Recommendation -->
<div hidden id="recommend">
<section class="recolist">
  <!--for demo wrap-->

  <h1>Recommended Patients</h1>
  <div class="tbl-header">
    <table cellpadding="0" cellspacing="0" border="0">
      <thead>
        <tr>
          <th>Patient Name</th>
          <th>Lacking</th>
          <th>Amount</th>
          <th></th>
        </tr>
      </thead>
    </table>  
  </div>
  <div class="tbl-content">
    <table cellpadding="0" cellspacing="0" border="0">
      <tbody>
      @foreach($recommend as $all)
        <tr>
          <td>{{$all['patientname']}}</td>
          <td>&#8369; {{number_format($all['donationPercentage'])}}</td>
      
        <form method="POST" action="{{url('/recoresult')}}">
        {{csrf_field()}}
          <td>&#8369; <input type="text" name="money" value="{{$all['money']}}"></td> 
          <td><button style="color:grey;">Donate</button></td>    
          <input type="hidden" name="patientid" value="{{$all['patientid']}}">
        </form>

        </tr>
        @endforeach
      </tbody>
    </table>
  </div>
</section>
</div>

<section class="section">
  <!--for demo wrap-->
  <h1>Based on Condition</h1>
 
  <div class="tbl-header">
    <table cellpadding="0" cellspacing="0" border="0">
      <thead>
        <tr>
          <th>Rank</th>
          <th>Patient ID</th>
          <th>Patient Name</th>
          <th>Percentage %</th>
        </tr>
      </thead>
    </table>
  </div>
  <div class="tbl-content">
    <table cellpadding="0" cellspacing="0" border="0">
      <tbody>
      @foreach($collectCondition as $condition)
        <tr>
          <td>{{$condition['rank']}}</td>
          <td>{{$condition['patientid']}}</td>
          <td>{{$condition['patientname']}}</td>
          <td>{{$condition['percentage']}}</td>
        </tr> 
        @endforeach
      </tbody>
    </table>
  </div>
 
</section>


<section class="section">
  <!--for demo wrap-->
  <h1>Based on Highest Need</h1>
 
  <div class="tbl-header">
    <table cellpadding="0" cellspacing="0" border="0">
      <thead>
        <tr>
          <th>Rank</th>
          <th>Patient ID</th>
          <th>Patient Name</th>
          <th>Percentage %</th>
        </tr>
      </thead>
    </table>
  </div>
  <div class="tbl-content">
    <table cellpadding="0" cellspacing="0" border="0">
      <tbody>
      @foreach($collecthn as $hneed)
        <tr>
          <td>{{$hneed['rank']}}</td>
          <td>{{$hneed['patientid']}}</td>
          <td>{{$hneed['patientname']}}</td>
          <td>{{$hneed['percentage']}}</td>
        </tr>
       @endforeach 
      </tbody>
    </table>
  </div>
</section>


<section class="section">
  <!--for demo wrap-->
  <h1>Based on Longest Duration</h1>
 
  <div class="tbl-header">
    <table cellpadding="0" cellspacing="0" border="0">
      <thead>
        <tr>
          <th>Rank</th>
          <th>Patient ID</th>
          <th>Patient Name</th>
          <th>Percentage %</th>
        </tr>
      </thead>
    </table>
  </div>
  <div class="tbl-content">
    <table cellpadding="0" cellspacing="0" border="0">
      <tbody>
      @foreach($collectlongestduration as $long)
        <tr>
          <td>{{$long['rank']}}</td>
          <td>{{$long['patientid']}}</td>
          <td>{{$long['patientname']}}</td>
          <td>{{$long['percentage']}}</td>
        </tr>
         @endforeach
      </tbody>
    </table>
  </div>
</section>

<section class="section">
  <!--for demo wrap-->
  <h1>Based on Highest Lacking Amount of Goal</h1>
 
  <div class="tbl-header">
    <table cellpadding="0" cellspacing="0" border="0">
      <thead>
        <tr>
          <th>Rank</th>
          <th>Patient ID</th>
          <th>Patient Name</th>
          <th>Percentage %</th>
        </tr>
      </thead>
    </table>
  </div>
  <div class="tbl-content">
    <table cellpadding="0" cellspacing="0" border="0">
      <tbody>
      @foreach($collecthighestlacking as $soon)
        <tr>
          <td>{{$soon['rank']}}</td>
          <td>{{$soon['patientid']}}</td>
          <td>{{$soon['patientname']}}</td>
          <td>{{$soon['percentage']}}</td>
        </tr>
         @endforeach
      </tbody>
    </table>
  </div>
</section>

<section class="section">
  <!--for demo wrap-->
  <h1>Based on Most no. of donations</h1>
 
  <div class="tbl-header">
    <table cellpadding="0" cellspacing="0" border="0">
      <thead>
        <tr>
          <th>Rank</th>
          <th>Patient ID</th>
          <th>Patient Name</th>
          <th>Percentage %</th>
        </tr>
      </thead>
    </table>
  </div>
  <div class="tbl-content">
    <table cellpadding="0" cellspacing="0" border="0">
      <tbody>
      @foreach($collectmostdonated as $most)
        <tr>
          <td>{{$most['rank']}}</td>
          <td>{{$most['patientid']}}</td>
          <td>{{$most['patientname']}}</td>
          <td>{{$most['percentage']}}</td>
        </tr>
         @endforeach
      </tbody>
    </table>
  </div>
</section>






@if(Session::has('success'))
    <script>
        alert('Donated Successfully!');
    </script>
@endif


<script>
  $(window).on("load resize ", function() {
  var scrollWidth = $('.tbl-content').width() - $('.tbl-content table').width();
  $('.tbl-header').css({'padding-right':scrollWidth});
}).resize();
</script>

<!-- js for notif -->
<script>
  $(document).ready(function (){

     setInterval(function(){ 
       $.ajax({
            type: 'GET',
            url: '/adminStoryCount',
            success:function(data){
                document.getElementById('admin_count').innerHTML  = data;
                console.log(data);
            },
            error:function(){
              console.log("");
            }
        });

       $.ajax({
            type: 'GET',
            url: '/adminVoucherCount',
            success:function(data){
                document.getElementById('voucher_count').innerHTML  = data;
                console.log(data);
            },
            error:function(){
              console.log("");
            }
        });

       $.ajax({
            type: 'GET',
            url: '/redeemCount',
            success:function(data){
                document.getElementById('redeem_count').innerHTML  = data;
                console.log(data);
            },
            error:function(){
              console.log("");
            }
        });

}, 1000);

        $.ajax({
            type: 'GET',
            url: '/donatereco',
            success:function(data){
                document.getElementById('recomoney').innerHTML  = data;
                console.log(data);
            },
            error:function(){
              console.log("");
            }
        });

      });
</script>


    
</body>
</html>