<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>HealthAid - Dashboard</title>
	<link href="css/bootstrap.min.css" rel="stylesheet">
	<link href="css/font-awesome.min.css" rel="stylesheet">
	<link href="css/datepicker3.css" rel="stylesheet">
	<link href="css/styles.css" rel="stylesheet">

	<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	
	<!--Custom Font-->
	<link href="https://fonts.googleapis.com/css?family=Montserrat:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">
	<!-- modal -->
	<link href="css/modalstory.css" rel="stylesheet">
</head>
<body>
	<nav class="navbar navbar-custom navbar-fixed-top" role="navigation">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#sidebar-collapse"><span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span></button>
				<a class="navbar-brand" href="{{ url('/displayusers') }}"><span>Health</span>Aids</a>
				<!-- notification -->
				<ul class="nav navbar-top-links navbar-right">
					<li class="dropdown">
					<a href="{{ url('/approve') }}" style="width:140px;height:45px">
					<p>Story Request</p>
					<span class="label label-danger">
						<span id="admin_count" style="color:white"></span></span>
					</a>
					</li>

					<li class="dropdown">
					<a href="{{ url('/request') }}" style="width:165px;height:45px">
					<p>Redeem Request</p>
					<span class="label label-info">0</span>
					</a>
						
					</li>

					<li class="dropdown">
					<a href="{{ url('/check') }}" style="width:170px;height:45px"><p>Uncheck Vouchers</p>
					<span class="label label-success">
						<span id="voucher_count" style="color:white"></span>
					</span>
					</a>
					</li>

				</ul>
			</div>
		</div><!-- /.container-fluid -->
	</nav>
	<div id="sidebar-collapse" class="col-sm-3 col-lg-2 sidebar">
		<div class="profile-sidebar">
		@if(Auth::user()->username == "gela") 
			<div class="profile-userpic">
				<img src={{('/images/gelprofile.jpg')}} class="img-responsive" alt="">
			</div>
		@elseif(Auth::user()->username == "camjoy") 
			<div class="profile-userpic">
				<img src={{('/images/camprofile.jpg')}} class="img-responsive" alt="">
			</div>
		@endif
			<div class="profile-usertitle">
				<div class="profile-usertitle-name">{{ Auth::user()->fname }}</div>
				<div class="profile-usertitle-status"><span class="indicator label-success"></span>Administrator</div>
			</div>
			<div class="clear"></div>
		</div>
		<div class="divider"></div>
		<!-- <form role="search">
			<div class="form-group">
				<input type="text" class="form-control" placeholder="Search">
			</div>
		</form> -->
		<ul class="nav menu">
			<li><a href="{{url('/displayusers')}}"><em class="fa fa-dashboard">&nbsp;</em> Dashboard</a></li>
			<li class="active"><a href="{{ url('/displaypatients') }}"><em class="fa fa-users">&nbsp;</em> Patients</a></li>
			<li><a href="{{ url('/displaysponsors') }}"><em class="fa fa-xl fa-users color-orange">&nbsp;</em> Sponsors</a></li>

			<li><a href="{{ url('/criteria') }}"><em class="fa fa-xl fa-users color-orange">&nbsp;</em> Recommendation Criteria</a></li>
      <li><a href="{{ url('/reco') }}"><em class="fa fa-xl fa-users color-orange">&nbsp;</em> Recommended</a></li>
			
			<li><a class="fa fa-power-off" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">&nbsp;&nbsp;Logout
                                    </a>
                                    
                                  
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
            </li>
		</ul>
	</div><!--/.sidebar-->
		
	<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
		<div class="row">
			<ol class="breadcrumb">
				<li><a href="#">
					<em class="fa fa-home"></em>
				</a></li>
				<li class="active">Dashboard</li>
			</ol>
		</div><!--/.row-->
		
		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">Stories for Approval</h1>
			</div>
		</div><!--/.row-->
	

	<div class="panel panel-container">
	<div class="row">
	<table class="table">
	
	<th class="fixed-table-container">Patient Name</th>
	<th class="fixed-table-container">Amount</th>
	<th class="fixed-table-container" colspan="5">Requirements</th>
	<th class="fixed-table-container"></th>
	<th class="fixed-table-container"></th>
<br>



<p id="patient">


	@foreach ($approve as $apr)
		<tr class="fixed-table-container">
			<td class="fixed-table-container">{{$apr->patient->patientname}}</td>
			<td class="fixed-table-container">&#8369; {{$apr->patient->goal}}</td>
		@foreach($apr->picture as $ap)
			<td class="fixed-table-container"><center><img id="img{{$ap->filename}}" src="{{  url('storage/picture/'.$ap->filename) }}" width="100px" height="100px" /></center></td>
			<div id="m{{$ap->filename}}" class="modal">
  				<span style="float: right; margin-top: -90px; font-size: 100px; cursor: pointer;" class="c{{$ap->filename}}">&times;</span>
  				<img class="modal-content" id="mimg{{$ap->filename}}">
			</div>
		@endforeach


<form action="{{url('approved')}}" method="post">
{{csrf_field()}}
			<td class="fixed-table-container" align="center">
				<button type="submit" value="Confirm" name="submit" class="btn btn-primary">Confirm</button>
			</td>
			<td class="fixed-table-container" align="left">
				<input type="checkbox" name="denymsg[]" value="Invalid Medical Abstract">Medical Abstract<br>	
				<input type="checkbox" name="denymsg[]" value="Invalid Medical Certificate">Medical Certificate<br>
				<input type="checkbox" name="denymsg[]" value="Invalid ID">Valid ID<br>
				<input type="checkbox" name="denymsg[]" value="Invalid Hospital Bill">Hospital Bill<br>
				<input type="checkbox" name="denymsg[]" value="Duplicate Stories">Duplicate Stories<br>
  				<button type="submit" value="Deny" name="submit" class="btn btn-danger">Deny</button>
			</td>
			<input type="hidden" name="patientid" value="{{$apr->patient->patientid}}">
			<input type="hidden" name="userid" value="{{$apr->patient->userid}}">
			<input type="hidden" name="story" value="{{$apr->patient->stories[0]->storytitle}}">
</form>


<script>
	document.getElementById('confirm').onclick = function(){
		document.getElementById('confirm').innerHTML = "Confirmed";
	} 
</script>

<script>
	$(document).ready(function) {
		$('#buttonCloseID').click(function() {
			var data = $("#myModal #denymsg").val().trim();
			$('#result').html(data);

		});
	});
</script>


<script>

@foreach ($approve as $apr)
@foreach($apr->picture as $ap)

// Get the modal
var modal = document.getElementById('m{{$ap->filename}}');

// Get the image and insert it inside the modal - use its "alt" text as a caption
var img = document.getElementById('img{{$ap->filename}}');
var modalImg = document.getElementById('mimg{{$ap->filename}}');
img.onclick = function(){
    modal.style.display = "block";
    modalImg.src = this.src;
}


// Get the <span> element that closes the modal
var span = document.getElementsByClassName("c{{$ap->filename}}")[0];

// When the user clicks on <span> (x), close the modal
span.onclick = function() { 
    modal.style.display = "none";
}

@endforeach
@endforeach
</script>

</tr>
		
@endforeach



</p>
</table><br><br>

				
			</div><!--/.row-->
		</div>
		
			</div><!--/.col-->
			
		</div><!--/.row-->
	</div>	<!--/.main-->
	
	<script src="js/jquery-1.11.1.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/chart.min.js"></script>
	<script src="js/chart-data.js"></script>
	<script src="js/easypiechart.js"></script>
	<script src="js/easypiechart-data.js"></script>
	<script src="js/bootstrap-datepicker.js"></script>
	<script src="js/custom.js"></script>
	<script>
		window.onload = function () {
	var chart1 = document.getElementById("line-chart").getContext("2d");
	window.myLine = new Chart(chart1).Line(lineChartData, {
	responsive: true,
	scaleLineColor: "rgba(0,0,0,.2)",
	scaleGridLineColor: "rgba(0,0,0,.05)",
	scaleFontColor: "#c5c7cc"
	});
};
	</script>

<!-- js for notif -->
<script>
	$(document).ready(function (){
     setInterval(function(){ 
       $.ajax({
            type: 'GET',
            url: '/adminStoryCount',
            success:function(data){
                document.getElementById('admin_count').innerHTML  = data;
                console.log(data);
            },
            error:function(){
              console.log("");
            }
        });

       $.ajax({
            type: 'GET',
            url: '/adminVoucherCount',
            success:function(data){
                document.getElementById('voucher_count').innerHTML  = data;
                console.log(data);
            },
            error:function(){
              console.log("");
            }
        });

      }, 300);
      });
</script>






