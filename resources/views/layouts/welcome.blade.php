<?php 
use App\Sponsor;
use App\Patient;
use App\Donation;
$sponsor = Sponsor::where('userid', Auth::id())->get();
//--helpxpMoney
$helpxpMoney = DB::table('sponsors')->where('status','=', 'd-hx')->sum('voucherValue');        
//--strories money
$donatedtopatients = DB::table('sponsors')->where('status','=','donated')->sum('voucherValue');
//--alltimedonation
$alltimedonation = $helpxpMoney + $donatedtopatients;
?>


<!DOCTYPE html>

  	<!-- Facebook and Twitter integration -->
	<meta property="og:title" content=""/>
	<meta property="og:image" content=""/>
	<meta property="og:url" content=""/>
	<meta property="og:site_name" content=""/>
	<meta property="og:description" content=""/>
	<meta name="twitter:title" content="" />
	<meta name="twitter:image" content="" />
	<meta name="twitter:url" content="" />
	<meta name="twitter:card" content="" />

	<!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
	<link rel="shortcut icon" href="favicon.ico">

	<!-- <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,700,300' rel='stylesheet' type='text/css'> -->
	
	<!-- Animate.css -->
	<link rel="stylesheet" href="css/animate.css">
	<!-- Icomoon Icon Fonts-->
	<link rel="stylesheet" href="css/icomoon.css">
	<!-- Bootstrap  -->
	<link rel="stylesheet" href="css/bootstrap.css">
	<!-- Superfish -->
	<link rel="stylesheet" href="css/superfish.css">

	<link rel="stylesheet" href="css/style.css">


	<!-- Modernizr JS -->
	<script src="js/modernizr-2.6.2.min.js"></script>
	<!-- FOR IE9 below -->
	<!--[if lt IE 9]>
	<script src="js/respond.min.js"></script>
	<![endif]-->

	<!-- slider -->
	<link rel="stylesheet" href="css/slider.css">
	<script src="js/slider.js"></script>
	<link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:300,300italic,regular,italic,700,700italic&subset=latin-ext,greek-ext,cyrillic-ext,greek,vietnamese,latin,cyrillic" rel="stylesheet" type="text/css" />

	</head>
	<body>

		<header id="fh5co-header-section" class="sticky-banner">
			<div class="container">
				<div class="nav-header">
					<a href="#" class="js-fh5co-nav-toggle fh5co-nav-toggle dark"><i></i></a>
					<a href="{{ url('/') }}"><img data-u="image" src="images/logoHA.png" width="120cm" height="90cm" /></a>
					<!-- START #fh5co-menu-wrap -->
					<nav id="fh5co-menu-wrap" role="navigation">
						<ul class="sf-menu" id="fh5co-primary-menu">
							<li class="active">
								<a href="{{ url('/') }}">Home</a>
							</li>
							<li>
								<a href="#" class="fh5co-sub-ddown">Get Involved</a>
								<ul class="fh5co-sub-menu">
									<li><a href="{{ route('login') }}">Buy voucher</a></li>
									<li><a href="#" class="fh5co-sub-subb-ddown">Fundraise</a></li>
									<li><ul class="fh5co-sub-subb-menu"></li>
										<li><li><a href="{{ route('login') }}">HealthAid</a></li></li>
										<li><li><a href="{{ route('login') }}">Patient</a></li></li>
									</ul>
									<li><a href="{{ route('login') }}">Share Story</a></li>
								</ul>
							</li>
							<li><a href="{{ url('/about') }}">About</a></li>
							<li><a href="{{ route('login') }}">Login</a></li>
							<li><a href="{{ route('register') }}">Register</a></li>
						</ul>
					</nav>
				</div>
			</div>
		</header>
		
		

		<!-- slider -->
<div class="fh5co-hero">
	

<div id="jssor_1" style="position:relative;margin:0 auto;top:0px;left:0px;width:1300px;height:600px;overflow:hidden;visibility:hidden;">
<!-- Loading Screen -->
<div data-u="loading" class="jssorl-009-spin" style="position:absolute;top:0px;left:0px;width:100%;height:100%;text-align:center;background-color:rgba(0,0,0,0.7);">
<img style="margin-top:-19px;position:relative;top:50%;width:38px;height:38px;" src="images/spin.svg" />
</div>
<div data-u="slides" style="cursor:default;position:relative;top:0px;left:0px;width:1300px;height:600px;overflow:hidden;">
<div data-p="225.00">
<img data-u="image" src="images/pov2.jpg" />
</div>
<div data-p="225.00">
<img data-u="image" src="images/pov1.jpg" />
</div>

<div data-p="225.00">
<img data-u="image" src="images/pov.jpg" />
<div style="position:absolute;top:300px;left:30px;width:480px;height:130px;font-family:'Roboto Condensed',sans-serif;font-size:30px;color:#000000;line-height:1.27;padding:5px 5px 5px 5px;box-sizing:border-box;margin-left: 50px">
	<h2 style="color: white;font-size:45px"><strong>Big</strong> or <strong>Small</strong> your cause matters.</h2>&nbsp;
	<span"><a class="btn btn-primary btn-lg" href="#">Donate Now</a></span></div>
</div>
</div>
<!-- Arrow Navigator -->
<div data-u="arrowleft" class="jssora051" style="width:65px;height:65px;top:0px;left:25px;" data-autocenter="2" data-scale="0.75" data-scale-left="0.75">
<svg viewbox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
<polyline class="a" points="11040,1920 4960,8000 11040,14080 "></polyline>
</svg>
</div>
<div data-u="arrowright" class="jssora051" style="width:65px;height:65px;top:0px;right:25px;" data-autocenter="2" data-scale="0.75" data-scale-right="0.75">
<svg viewbox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
<polyline class="a" points="4960,1920 11040,8000 4960,14080 "></polyline>
</svg>
</div>
</div>
<script type="text/javascript">jssor_1_slider_init();</script>
<!-- #endregion Jssor Slider End -->


<!-- 	slider end -->
<br>
<div class="container2">
 <div class="gallery">
  <div class="desc" style="color: green; font-size: 35pt; letter-spacing: 0.2em"> 
   <span>&#8369;</span> <strong>{{number_format(DB::table('sponsors')->where('status', 'donated')->orwhere('status', 'dd-hx')->orwhere('status', 'd-hx')->sum('voucherValue'))}}</strong> all time donations
  </div>
</div> 
</div><br><br>

<!-- HealthAid money and all stories -->
<br>
<div class="container2">
 <div class="gallery">
  <div class="desc" style="color: green; font-size: 35pt;float: left"> 
   <center><span>&#8369;</span> <strong>{{number_format($helpxpMoney)}}</strong><p>donated to HealthAid</p></center> 
  </div>
</div> 
</div>

<div class="container2">
 <div class="gallery">
  <div class="desc" style="color: green; font-size: 35pt"> 
   <center><span>&#8369;</span> <strong>{{number_format($donatedtopatients)}}</strong><p>donated to all stories</p></center> 
  </div>
</div> 
</div>
<br><br>
<style>
/* width */
::-webkit-scrollbar {
    width: 10px;
}

/* Track */
::-webkit-scrollbar-track {
    background: #f1f1f1; 
}
 
/* Handle */
::-webkit-scrollbar-thumb {
    background: #888; 
}

/* Handle on hover */
::-webkit-scrollbar-thumb:hover {
    background: #555; 
}
</style>

<!-- start realtime donation -->
<center>
<div align="center" style="background-color: white; width: 500px; max-width: 500px; border-radius: 25px; overflow:scroll; height:370px; ">
	<div style="background-color: white; padding: 10px; font-size: 18pt; width: 375px; text-align: left; color: black; font-weight: bolder;text-align: center;">Recent Donations</div>
	@foreach ($real as $rl)
	<table style="border-bottom: 1px solid #e6e6e6; border-top: 1px solid #e6e6e6; width: 375px; text-align: center; max-width: 375px; padding-left: 30px; size: 14pt;">
	<tr style="margin-bottom: 10px;">
		<td style="color: black; font-weight: bold; font-size: 17;"><span>&#8369;</span>&nbsp;{{ $rl->amountDonated }}</td>
	</tr>
	<tr>
		<td style="color: green; font-weight: normal; padding-bottom: 15px; font-size: 11pt;">
	{{ $rl->sponsorName }}</td>
	</tr>
	</table>
	@endforeach

</div>
</center>
<!-- start realtime donation -->

<br><br><br><br><br><br>
		<!-- end:header-top -->
		<div id="fh5co-features">
			<div class="container">
				<div class="row">
					<div class="col-md-4">

						<div class="feature-left">
							
							<div class="feature-copy">
								<h3>Become a member</h3>
								<p>Be a member and help people in need. For sharing is loving.</p>
								<p><a href="#">Learn More</a></p>
							</div>
						</div>

					</div>

					<div class="col-md-4">
						<div class="feature-left">
							
							<div class="feature-copy">
								<h3>Happy Giving</h3>
								<p>Giving back is as good for you as it is for those you are helping.</p>
								<p><a href="#">Learn More</a></p>
							</div>
						</div>

					</div>
					<div class="col-md-4">
						<div class="feature-left">
							
							<div class="feature-copy">
								<h3>Donation</h3>
								<p>Making money is a happiness; making other people happy is a superhappiness.</p>
								<p><a href="#">Learn More</a></p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div id="fh5co-feature-product" class="fh5co-section-gray">
			<div class="container">
				<div class="row">
					<div class="col-md-12 text-center heading-section">
						<h3>Shared Stories.</h3>
						<p>A simple way to give and share the Kiwi spirit.</p>
					</div>
				</div>

				<main class="py-4">
				            @yield('content')
				</main>
				</div>
			</div>

			<div id="fh5co-feature-product" class="fh5co-section-white">
			<div class="container">

				<div class="row">
					<div class="col-md-4">
						<div class="feature-text">
							<h3>Love</h3>
							<p>Keep love in your heart. A life without it is like a sunless garden when the flowers are dead.</p>
						</div>
					</div>
					<div class="col-md-4">
						<div class="feature-text">
							<h3>Compassion</h3>
							<p>Compassion is the basis of morality.</p>
						</div>
					</div>
					<div class="col-md-4">
						<div class="feature-text">
							<h3>Charity</h3>
							<p>Charity brings to life again those who are spiritually dead.</p>
						</div>
					</div>
				</div>
			</div>
		</div>

				
			

		
		<div id="fh5co-feature-product" class="fh5co-section-gray">
			<div class="container">

				<div class="row">
					<div class="col-md-6 col-md-offset-3 text-center heading-section animate-box">
						<h3>Our Gallery</h3>
						<p>Valuable memories we collected from our beneficiaries.</p>
					</div>
				</div>

				
				<div class="row row-bottom-padded-md">
					<div class="col-md-12">
						<ul id="fh5co-portfolio-list">

							<li class="two-third animate-box" data-animate-effect="fadeIn" style="background-image: url(images/pov.jpg); ">
								<a href="#" class="color-3">
									<div class="case-studies-summary">
										<span>Give Love</span>
										<h2>Donation is caring</h2>
									</div>
								</a>
							</li>
						
							<li class="one-third animate-box" data-animate-effect="fadeIn" style="background-image: url(images/pov1.jpg); ">
								<a href="#" class="color-4">
									<div class="case-studies-summary">
										<span>Give Love</span>
										<h2>Donation is caring</h2>
									</div>
								</a>
							</li>

							<li class="one-third animate-box" data-animate-effect="fadeIn" style="background-image: url(images/pov3.jpg); "> 
								<a href="#" class="color-5">
									<div class="case-studies-summary">
										<span>Give Love</span>
										<h2>Donation is caring</h2>
									</div>
								</a>
							</li>
							<li class="two-third animate-box" data-animate-effect="fadeIn" style="background-image: url(images/pov2.jpg); ">
								<a href="#" class="color-6">
									<div class="case-studies-summary">
										<span>Give Love</span>
										<h2>Donation is caring</h2>
									</div>
								</a>
							</li>
						</ul>		
					</div>
				</div>

				<div class="row">
					<div class="col-md-4 col-md-offset-4 text-center animate-box">
						<a href="#" class="btn btn-primary btn-lg">See Gallery</a>
					</div>
				</div>

				
			</div>
		</div>
		

		<!-- fh5co-content-section -->

		

		<!-- END What we do -->

<br><br><br><br>
				<div class="row">
					<div class="col-md-4 col-md-offset-4 text-center animate-box">
						<a href="#" class="btn btn-primary btn-lg">Raise Funds</a>
					</div>
				</div>

				<br><br><br>

		<!-- fh5co-blog-section -->
		<footer>
			<div id="footer">
				<div class="container">
					<div class="row">
						<div class="col-md-6 col-md-offset-3 text-center">
							
							<p><a href="#"><img data-u="image" src="images/logoHA.png" width="120cm" height="90cm" /></a>. All Rights Reserved. </a></p>
						</div>
					</div>
				</div>
			</div>
		</footer>
	

	</div>
	<!-- END fh5co-page -->

	</div>
	<!-- END fh5co-wrapper -->

	<!-- jQuery -->


	<script src="js/jquery.min.js"></script>
	<!-- jQuery Easing -->
	<script src="js/jquery.easing.1.3.js"></script>
	<!-- Bootstrap -->
	<script src="js/bootstrap.min.js"></script>
	<!-- Waypoints -->
	<script src="js/jquery.waypoints.min.js"></script>
	<script src="js/sticky.js"></script>

	<!-- Stellar -->
	<script src="js/jquery.stellar.min.js"></script>
	<!-- Superfish -->
	<script src="js/hoverIntent.js"></script>
	<script src="js/superfish.js"></script>
	
	<!-- Main JS -->
	<script src="js/main.js"></script>

	</body>
</html>

