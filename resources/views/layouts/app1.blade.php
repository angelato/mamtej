<!-- welcome -->
<?php 
use App\Redeem;
use App\Patient;
?>


<!DOCTYPE html>
<html>
<head>
  <script src="{{ asset('js/app1.js') }}" defer></script>
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,600" rel="stylesheet" type="text/css">
  <link href="{{ asset('css/app1.css') }}" rel="stylesheet">
  <link href="{{ asset('css/nav.css') }}" rel="stylesheet">
  <link href="{{ asset('css/jumbotron.css') }}" rel="stylesheet">
    <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script>
</head>
<body>
<div id="app">
<ul>
   @guest
   <li><a href="{{ url('/') }}"><img data-u="image" src="images/logoHA.png" width="120cm" height="90cm" /></a></li>
    <li style="float: right; text-decoration: none;"><a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a></li>
    <li style="float: right; text-decoration: none;"><a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a></li>
  @else
  <li><a href="{{ url('/') }}">HealthAid</a></li>

<?php
$user = Auth::id();
$patient = Patient::where('userid', $user)->where('status', '=', null)->get()->count(); 
?>
    @if($patient <= 0)
  <li><a href="{{ url('/patientsdetail') }}">Apply</a></li>
    @else
    @endif



  <li><a href="{{ url('/donateAny') }}/{{Auth::user()->id}}">Raise Funds</a></li>
  <li><a href="{{ url('/buyvoucher') }}/{{Auth::user()->id}}">Buy Voucher</a></li>
  <li><a href="{{ url('/about') }}">About Us</a></li>
  <li style="float:right"><a href="{{ route('logout')}}" onclick="event.preventDefault();
      document.getElementById('logout-form').submit();">
      {{ __('Logout') }}</a></li>                           
  <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
  @csrf
  </form>
  <li style="float:right"><a href="{{ url('/history') }}">History</a></li>
  <li style="float:right"><a href="{{ url('/history') }}">Hello {{ Auth::user()->username }}!</a></li>
  @endguest
</ul>


 <main class="py-4">
            @yield('content')
        </main>
        
</div>


</body>
</html>
