<?php 
use App\Sponsor;
use App\Patient;
use App\Donation;
$sponsor = Sponsor::where('userid', Auth::id())->get();
//--helpxpMoney
$helpxpMoney = DB::table('sponsors')->where('status','=', 'd-hx')->orwhere('status','=','donated-xp')->sum('voucherValue');        
//--strories money
$donatedtopatients = DB::table('sponsors')->where('status','=','donated')->sum('voucherValue');
//--alltimedonation
$alltimedonation = $helpxpMoney + $donatedtopatients;
?>

<title>HealthAid</title>
<head>
<meta property="og:title" content=""/>
  <meta property="og:image" content=""/>
  <meta property="og:url" content=""/>
  <meta property="og:site_name" content=""/>
  <meta property="og:description" content=""/>
  <meta name="twitter:title" content="" />
  <meta name="twitter:image" content="" />
  <meta name="twitter:url" content="" />
  <meta name="twitter:card" content="" />

  <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
  <link rel="shortcut icon" href="favicon.ico">
  <link rel="stylesheet" href="/css/alert.css">

  <!-- <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,700,300' rel='stylesheet' type='text/css'> -->
  
  <!-- Animate.css -->
  <link rel="stylesheet" href="css/animate.css">
  <!-- Icomoon Icon Fonts-->
  <link rel="stylesheet" href="css/icomoon.css">
  <!-- Bootstrap  -->
  <link rel="stylesheet" href="css/bootstrap.css">
  <!-- Superfish -->
  <link rel="stylesheet" href="css/superfish.css">

  <link rel="stylesheet" href="css/style.css">

  <link rel="stylesheet" href="css/search.css">
  <link rel="stylesheet" href="css/notif.css">

  <!-- Modernizr JS -->
  <script src="js/modernizr-2.6.2.min.js"></script>

  <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<!-- ======= -->
  <!-- progress -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>


     <!--  carousel -->
 <link rel="stylesheet" href="css/slider.css">
  <script src="js/slider.js"></script>
  <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:300,300italic,regular,italic,700,700italic&subset=latin-ext,greek-ext,cyrillic-ext,greek,vietnamese,latin,cyrillic" rel="stylesheet" type="text/css" />

<link rel="stylesheet" href="css/buttonReco.css">


  </head>


  <body>
  @guest
    <header id="fh5co-header-section" class="sticky-banner">
      <div class="container">
        <div class="nav-header">
          <a href="#" class="js-fh5co-nav-toggle fh5co-nav-toggle dark"><i></i></a>
          <a href="{{ url('/home') }}"><img data-u="image" src="images/logoHA.png" width="120cm" height="90cm" /></a>
          <!-- START #fh5co-menu-wrap -->
          <nav id="fh5co-menu-wrap" role="navigation">

            <ul class="sf-menu" id="fh5co-primary-menu">   
              <li>
                <a href="{{ url('/home') }}">Home</a>
              </li>
              <li>
                <a href="#" class="fh5co-sub-ddown">Get Involved</a>
                <ul class="fh5co-sub-menu">
                  <li><a href="{{ url('/login') }}">Buy Voucher</a></li>
                     
                    
                    
                  <li><a href="{{ url('/login') }}">Fundraise</a></li>
                  <li><a href="{{ url('/login') }}">Share Story</a></li>
                </ul>
              </li>
              <li><a href="{{ url('/about') }}">About</a></li>
              <li><a href="{{ route('login') }}">Login</a></li>
              <li><a href="{{ route('register') }}">Register</a></li>
            </ul>
          </nav>
        </div>
      </div>
    </header>
  @else
    
  <header id="fh5co-header-section" class="sticky-banner">
      <div class="container">
        <div class="nav-header">
          <a href="#" class="js-fh5co-nav-toggle fh5co-nav-toggle dark"><i></i></a>
          <a href="{{ url('/home') }}"><img data-u="image" src="images/logoHA.png" width="120cm" height="90cm" /></a>

<?php
$patient = Patient::where('userid',Auth::id())->get();
?>
   
   <nav id="fh5co-menu-wrap" role="navigation">
            <ul class="sf-menu" id="fh5co-primary-menu">
            <li>
                               <!-- search -->

                          <form action="{{url('/search')}}" method="POST">
                              {{csrf_field()}}
                         <input type="search" placeholder="Search for illness.." name="search">
                              </form>
 
                    <!-- end search -->
            </li>
              <li>
                <a href="{{ url('/home') }}">Home</a>
              </li>
              <li>
                <a href="#" class="fh5co-sub-ddown">Get Involved</a>
                <ul class="fh5co-sub-menu">

                <li><a href="#" class="fh5co-sub-ddown">Buy vouchers</a>
                  <ul class="fh5co-sub-menu"  style="width: 250px">
                    <li><a href="{{ url('/buyvoucher') }}/{{Auth::user()->id }}">Buy Voucher with Bank Deposit</a></li>
                    <li><a href="{{ url('/paypal') }}">Buy Voucher with PayPal</a></li>
                  </ul>
                </li>

                  <li><a href="{{ url('/donateAny') }}/{{Auth::user()->id }}">Donate to HealthAid</a></li>
         
                  <li><a href="{{ url('/patientsdetail') }}">Post Story</a></li>
         
                </ul>
              </li>

              <!-- voucher notif -->
            
      <li id="voucher_li" >      
      <a onclick="vmarkRead()" id="voucherLink" class="voucherLink"><img src="/images/icons8-voucher-64.png" style="height:25px"><span id="voucher_count"></span></a>
      
      <div id="voucherContainer">
      <div id="voucherTitle">Voucher Notifications</div>

      <div id="voucherBody" class="voucher">
      <div id="vunread" style="background-color:lightgrey;border:1px solid #ccc;background-size:350px"></div>
      <div id="vread"></div>     
      </div>

      <div id="voucherFooter"><a href="{{url('/seeall')}}}">See All</a></div>
      </div>
      </li>
    

      <script>
      $(document).ready(function()
      {
      $("#voucherLink").click(function()
      {
      $("#voucherContainer").fadeToggle(300);
      $("#voucher_count").fadeOut("slow");
      return false;
      });

      //Document Click hiding the popup 
      $(document).click(function()
      {
      $("#voucherContainer").hide();
      });

      //Popup on click
      $("#voucherContainer").click(function()
      {
      return false;
      });

      });
      </script>
              <!-- voucher notif end -->

<!-- story notif -->
      <li id="notification_li" >      
      <a onclick="markRead()" id="notificationLink" class="notificationLink"><img src="/images/icons8-activity-history-50.png" style="height:25px"><span id="notification_count"></span></a>
      
      <div id="notificationContainer">
      <div id="notificationTitle">Story Notifications</div>

      <div id="notificationsBody" class="notifications">


      <!-- new -->
      <div id="unread" style="background-color:lightgrey">

      </div>
   
      <!-- end -->

      <div id="read"></div>     
      </div>

      <div id="notificationFooter"><!-- <a href="{{url('/seeall')}}}">See All</a> --></div>
      </div>
      </li>
    

      <script>
      $(document).ready(function()
      {
      $("#notificationLink").click(function()
      {
      $("#notificationContainer").fadeToggle(300);
      $("#notification_count").fadeOut("slow");
      return false;
      });

      //Document Click hiding the popup 
      $(document).click(function()
      {
      $("#notificationContainer").hide();
      });

      //Popup on click
      $("#notificationContainer").click(function()
      {
      return false;
      });

      });
      </script>
        <!-- end -->      



              <li><a href="{{ url('/about') }}">About</a></li>
              
              <li><a class="fh5co-sub-ddown" href="">Hi <strong>{{ Auth::user()->fname }}</strong>!
              </a>
              <ul class="fh5co-sub-menu">
<li><a href="{{url('/mystory')}}"><img src="/images/script.png" style="height:20px; width: 20px;">&nbsp;&nbsp;My Story</a></li>
 <li><a href="{{url('/archivestory')}}"><img src="/images/archive.png" style="height:20px; width: 20px;">&nbsp;&nbsp;Archive Story</a></li>
              <li>
<a href="{{url('/viewvoucher')}}" ><img src="/images/icons8-voucher-64.png" style="height:20px; width: 18px;">&nbsp;&nbsp;My Vouchers</a></li>
<li><a href="{{ url('/history') }}"><img src="/images/historyy.png" style="height:20px; width: 20px;">&nbsp;History</a></li>
              <li><a href="{{ route('logout')}}" onclick="event.preventDefault();
      document.getElementById('logout-form').submit();"><span class="glyphicon">&#xe163;</span>&nbsp;&nbsp;{{ __('Logout') }}</a></li>
              </ul>
              </li>
            
      <form id="logout-form" action="{{ route('logout') }}" method="POST">
      @csrf
      </form>
  
    </ul>
  </nav>
</div>
</div>
</header>

<!-- start sa not yet fin -->
<div class="fh5co-hero">
  

<div id="jssor_1" style="position:relative;margin:0 auto;top:0px;left:0px;width:1300px;height:600px;overflow:hidden;visibility:hidden;">
<!-- Loading Screen -->
<div data-u="loading" class="jssorl-009-spin" style="position:absolute;top:0px;left:0px;width:100%;height:100%;text-align:center;background-color:rgba(0,0,0,0.7);">
<img style="margin-top:-19px;position:relative;top:50%;width:38px;height:38px;" src="images/spin.svg" />
</div>
<div data-u="slides" style="cursor:default;position:relative;top:0px;left:0px;width:1300px;height:600px;overflow:hidden;">
<div data-p="225.00">
<img data-u="image" src="images/pov2.jpg" />
</div>
<div data-p="225.00">
<img data-u="image" src="images/pov1.jpg" />
</div>

<div data-p="225.00">
<img data-u="image" src="images/pov.jpg" />
<div style="position:absolute;top:300px;left:30px;width:480px;height:130px;font-family:'Roboto Condensed',sans-serif;font-size:30px;color:#000000;line-height:1.27;padding:5px 5px 5px 5px;box-sizing:border-box;margin-left: 50px">
  <h2 style="color: white;font-size:45px"><strong>Big</strong> or <strong>Small</strong> your cause matters.</h2>&nbsp;
  <span"><a class="btn btn-primary btn-lg" href="#">Donate Now</a></span></div>
</div>
</div>
<!-- Arrow Navigator -->
<div data-u="arrowleft" class="jssora051" style="width:65px;height:65px;top:0px;left:25px;" data-autocenter="2" data-scale="0.75" data-scale-left="0.75">
<svg viewbox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
<polyline class="a" points="11040,1920 4960,8000 11040,14080 "></polyline>
</svg>
</div>
<div data-u="arrowright" class="jssora051" style="width:65px;height:65px;top:0px;right:25px;" data-autocenter="2" data-scale="0.75" data-scale-right="0.75">
<svg viewbox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
<polyline class="a" points="4960,1920 11040,8000 4960,14080 "></polyline>
</svg>
</div>
</div>
<script type="text/javascript">jssor_1_slider_init();</script>

@endguest

<br>


<div class="container2" style="">
 <div class="gallery">
  <div class="desc" style="color: green; font-size: 40pt; letter-spacing: 0.2em"> 
   <span>&#8369;</span>&nbsp;<strong>{{number_format($alltimedonation)}}</strong> all time donations
  </div>
</div> 
</div><br>

<!-- HealthAid money and all stories -->
<br>
<div class="container2">
 <div class="gallery">
  <div class="desc" style="color: green; font-size: 40pt;float: left"> 
   <center><span>&#8369;</span>&nbsp;<strong>{{number_format($helpxpMoney)}}</strong><p>donated to HealthAid</p></center> 
  </div>
</div> 
</div>

<div class="container2">
 <div class="gallery">
  <div class="desc" style="color: green; font-size: 40pt"> 
   <center><span>&#8369;</span>&nbsp;<strong>{{number_format($donatedtopatients)}}</strong><p>donated to all stories</p></center> 
  </div>
</div> 
</div>



<br><br><br>
<!-- end -->

<main class="py-4">
            @yield('content')
</main>

<script src="js/jquery.min.js"></script>
  <!-- jQuery Easing -->
  <script src="js/jquery.easing.1.3.js"></script>
  <!-- Bootstrap -->
  <script src="js/bootstrap.min.js"></script>
  <!-- Waypoints -->
  <script src="js/jquery.waypoints.min.js"></script>
  <script src="js/sticky.js"></script>

  <!-- Stellar -->
  <script src="js/jquery.stellar.min.js"></script>
  <!-- Superfish -->
  <script src="js/hoverIntent.js"></script>
  <script src="js/superfish.js"></script>
  
  <!-- Main JS -->
  <script src="js/main.js"></script>



</body>
</html>
   

<!-- story notification scripts -->
   <script type="text/javascript">
     $(document).ready(function (){
      setInterval(function(){ 
      var text= " ";
      var read= " ";
     

        $.ajax({
            type: 'GET',
            url: '/storycount',
            success:function(data){
                document.getElementById('notification_count').innerHTML  = data;
                console.log(data);
            },
            error:function(){
              console.log("");
            }
        });

        $.ajax({
            type: 'GET',
            url: '/storynotifunread',
            success:function(data){
              
              for(i=0;i<data.length;i++){
                text += `<div style="border:1px solid #ccc"></strong><br><p style="color: blue"> </p>`+data[i].created_at+`<strong style="color:#ff5c33">`+data[i].subject+` `+ data[i].text + `</div>` 
              }  
                document.getElementById('unread').innerHTML  = text;
                console.log(data);
            },
            error:function(){
              console.log("");
            }
        });

         $.ajax({
            type: 'GET',
            url: '/read',
            success:function(data){
              
              for(i=0; i<data.length; i++){
                read +=`<div style="border:1px solid #ccc"><strong> `+ data[i].subject + data[i].text + `</strong>&nbsp;<p style="color: blue">`+data[i].created_at+`</p></div>` 
              }  
                document.getElementById('read').innerHTML  = read;
                console.log(data);
            },
            error:function(){
              console.log("");
            }
        });        

}, 300);
});
     function markRead() {
         $.ajax({
            type: 'GET',
            url: '/markRead',
            success:function(data){
                console.log(data);
            },
            error:function(){
              console.log("");
            }
        });      
     }
</script>


<!-- voucher notification script -->

 <script type="text/javascript">
     $(document).ready(function (){
      setInterval(function(){ 
      var vtext= " ";
      var vread= " ";
     

        $.ajax({
            type: 'GET',
            url: '/vouchercount',
            success:function(data){
                document.getElementById('voucher_count').innerHTML  = data;
                console.log(data);
            },
            error:function(){
              console.log("");
            }
        });

        $.ajax({
            type: 'GET',
            url: '/voucherunread',
            success:function(data){
              
              for(i=0;i<data.length;i++){
                vtext += `<div style="border:1px solid #ccc"></strong><br><p style="color:blue"> </p>`+data[i].subject+`<strong style="color:#ff5c33">`+data[i].text+`<br>`+ data[i].created_at +`</div>`  
              }  
                document.getElementById('vunread').innerHTML  = vtext;
                console.log(data);
            },
            error:function(){
              console.log("");
            }
        });

         $.ajax({
            type: 'GET',
            url: '/voucherread',
            success:function(data){
              
              for(i=0; i<data.length; i++){
                vread +=`<div style="border:1px solid #ccc"><strong> `+ data[i].subject + data[i].text + `</strong>&nbsp;<p style="color:blue">`+data[i].created_at+`</p></div>` 
              }  
                document.getElementById('vread').innerHTML  = vread;
                console.log(data);
            },
            error:function(){
              console.log("");
            }
        });        

}, 300);
});
     function vmarkRead() {
         $.ajax({
            type: 'GET',
            url: '/vouchermarkread',
            success:function(data){
                console.log(data);
            },
            error:function(){
              console.log("");
            }
        });      
     }
</script>