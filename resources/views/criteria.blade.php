<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>HealthAid - Dashboard</title>
  <link href="css/bootstrap.min.css" rel="stylesheet">
  <link href="css/font-awesome.min.css" rel="stylesheet">
  <link href="css/datepicker3.css" rel="stylesheet">
  <link href="css/styles.css" rel="stylesheet">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/criteria.css">
  <!--Custom Font-->
  <link href="https://fonts.googleapis.com/css?family=Montserrat:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">
  <!-- modal -->
  <link rel="stylesheet" href="css/criteriamodal.css">
  <!-- Modernizr JS -->
  <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">


</head>
<body>
  <nav class="navbar navbar-custom navbar-fixed-top" role="navigation">
    <div class="container-fluid">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#sidebar-collapse"><span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span></button>
        <a class="navbar-brand" href="{{ url('/displayusers') }}"><span>Health</span>Aid</a>
<!-- notification -->
        <ul class="nav navbar-top-links navbar-right">
          <li class="dropdown">
          <a href="{{ url('/approve') }}" style="width:140px;height:45px">
          <p>Story Request</p>
          <span class="label label-danger">
            <span id="admin_count" style="color:white"></span></span>
          </a>
          </li>

           <li class="dropdown">
          <a href="{{ url('/request') }}" style="width:165px;height:45px">
          <p>Redeem Request</p>
          <span class="label label-info">
            <span id="redeem_count" style="color:white"></span>
          </span>
          </a>  
          </li>

          <li class="dropdown">
          <a href="{{ url('/check') }}" style="width:170px;height:45px"><p>Uncheck Vouchers</p>
          <span class="label label-success">
            <span id="voucher_count" style="color:white"></span>
          </span>
          </a>
          </li>
        </ul>

      </div>
    </div><!-- /.container-fluid -->
  </nav>
  <div id="sidebar-collapse" class="col-sm-3 col-lg-2 sidebar">
    <div class="profile-sidebar">
    @if(Auth::user()->username == "gela") 
      <div class="profile-userpic">
        <img src={{('/images/gelprofile.jpg')}} class="img-responsive" alt="">
      </div>
    @elseif(Auth::user()->username == "camjoy") 
      <div class="profile-userpic">
        <img src={{('/images/camprofile.jpg')}} class="img-responsive" alt="">
      </div>
    @endif
      <div class="profile-usertitle">
        <div class="profile-usertitle-name">{{ Auth::user()->fname }}</div>
        <div class="profile-usertitle-status"><span class="indicator label-success"></span>Administrator</div>
      </div>
      <div class="clear"></div>
    </div>
    <div class="divider"></div>
    <!-- form role="search">
      <div class="form-group">
        <input type="text" class="form-control" placeholder="Search">
      </div>
    </form> -->

    <ul class="nav menu">
      <li ><a href="{{ url('/displayusers') }}"><em class="fa fa-dashboard">&nbsp;</em> Dashboard</a></li>
      <li>
        <a href="{{ url('/displaypatients') }}"><em class="fa fa-xl fa-users">&nbsp;</em> Patients</a>
      </li>
      <li><a href="{{ url('/displaysponsors') }}"><em class="fa fa-xl fa-users">&nbsp;</em> Sponsors</a></li>

      <!-- new -->
      <li class="active"><a href="{{ url('/criteria') }}"><em class="fa fa-bar-chart">&nbsp;</em> Recommendation Criteria</a></li>
      <li><a href="{{ url('/reco') }}"><em class="fa fa-xl fa-users">&nbsp;</em> Recommended</a></li>



      <li><a class="fa fa-power-off" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">&nbsp;&nbsp;Logout
                                    </a>
                                    
                                  
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
            </li>
    </ul>
  </div><!--/.sidebar-->
    
  <div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
    <div class="row">
      <ol class="breadcrumb">
        <li><a href="{{ url('displayusers') }}">
          <em class="fa fa-home"></em>
        </a></li>
        <li class="active">Recommendation Criteria</li>
      </ol>
    </div><!--/.row-->
    
    <div class="row">
      <div class="col-lg-12">
        <h1 class="page-header">Recommendation</h1>
      </div>
    </div><!--/.row-->
    

<!-- Recommendation -->
    
      <div class="card card-1"><br>
        <h2>Recommendation Criteria</h2>
<center>
       <table class="table table-bordered table-striped">
    <thead>
        <tr>
            <th class="">№</th>
            <th class="">Base on</th>
            <th class="">Percentage</th>
            <th></th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td style="text-align:center;" class="">{{$criteria[0]->criteria_id}}</td>
            <td class="">{{$criteria[0]->name}}</td>
            <td style="text-align:center;" class="">{{$criteria[0]->percentage}}</td>
        </tr>
        <tr>
            <td style="text-align:center;" class="">{{$criteria[1]->criteria_id}}</td>
            <td class="">{{$criteria[1]->name}}</td>
            <td style="text-align:center;" class="">{{$criteria[1]->percentage}}</td>
        </tr>
         <tr>
            <td style="text-align:center;" class="">{{$criteria[2]->criteria_id}}</td>
            <td class="">{{$criteria[2]->name}}</td>
            <td style="text-align:center;" class="">{{$criteria[2]->percentage}}</td>
        </tr>
         <tr>
            <td style="text-align:center;" class="">{{$criteria[3]->criteria_id}}</td>
            <td class="">{{$criteria[3]->name}}</td>
            <td style="text-align:center;" class="">{{$criteria[3]->percentage}}</td>
        </tr>
         <tr>
            <td style="text-align:center;" class="">{{$criteria[4]->criteria_id}}</td>
            <td class="">{{$criteria[4]->name}}</td>
            <td style="text-align:center;" class="">{{$criteria[4]->percentage}}</td>
        </tr>
    </tbody>
</table>
<button class="btn btn-success" id="myBtn">Change</button>
</center>
      </div>

<!-- The Modal -->
<div id="myModal" class="modal">

  <!-- Modal content -->
  <div class="modal-content">
    <span class="close">&times;</span>
    <table class="table data">
  <thead>
    <tr>
      <th></th>
      <th>Base on</th>
      <th>Current Percentage</th>
      <th>New Percentage</th>
    </tr>
  </thead>
  <tbody>
    <form method="POST" action="{{ url('/criteria') }}">
    @csrf
    <tr>
      <td><input type="hidden" name="criteria_1" value="{{$criteria[0]->criteria_id}}"></td>
      <td class="data">{{$criteria[0]->name}}</td>
      <td class="data">{{$criteria[0]->percentage}}</td>
      <td class="data"><input type='number' min="10" step="5" name='condition' style="width:50px" id="1" onclick="getTotal()"/></td>
    </tr>
    <tr>
      <td><input type="hidden" name="criteria_2" value="{{$criteria[1]->criteria_id}}"></td>
      <td class="data">{{$criteria[1]->name}}</td>
      <td class="data">{{$criteria[1]->percentage}}</td>
      <td class="data"><input type='number' min="10" step="5" name='hneed' style="width:50px" id="2" onclick="getTotal()"/></td>
    </tr>
    <tr>
      <td><input type="hidden" name="criteria_3" value="{{$criteria[2]->criteria_id}}"></td>
      <td class="data">{{$criteria[2]->name}}</td>
      <td class="data">{{$criteria[2]->percentage}}</td>
      <td class="data"><input type='number' min="10" step="5" name='longest' style="width:50px" id="3" onclick="getTotal()"/></td>
    </tr>
    <tr>
      <td><input type="hidden" name="criteria_4" value="{{$criteria[3]->criteria_id}}"></td>
      <td class="data">{{$criteria[3]->name}}</td>
      <td class="data">{{$criteria[3]->percentage}}</td>
      <td class="data"><input type='number' min="10" step="5" name='closing' style="width:50px" id="4" onclick="getTotal()"/></td>
    </tr>
    <tr>
      <td><input type="hidden" name="criteria_5" value="{{$criteria[4]->criteria_id}}"></td>
      <td class="data">{{$criteria[4]->name}}</td>
      <td class="data">{{$criteria[4]->percentage}}</td>
      <td class="data"><input type='number' min="10" step="5" name='most' style="width:50px" id="5" onclick="getTotal()"/></td>
    </tr>
  </tbody>
</table>
<button id="button">Save</button>
 <div id="totalcriteria" style="font-size:12pt;color:black;float:right"></div>
</form>
  </div>

</div>
<!-- end -->

    
      </div><!--/.col-->
      
    </div><!--/.row-->
  </div>  <!--/.main-->
  
  <script src="js/jquery-1.11.1.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <script src="js/chart.min.js"></script>
  <script src="js/chart-data.js"></script>
  <script src="js/easypiechart.js"></script>
  <script src="js/easypiechart-data.js"></script>
  <script src="js/bootstrap-datepicker.js"></script>
  <script src="js/custom.js"></script>
  <script>
    window.onload = function () {
  var chart1 = document.getElementById("line-chart").getContext("2d");
  window.myLine = new Chart(chart1).Line(lineChartData, {
  responsive: true,
  scaleLineColor: "rgba(0,0,0,.2)",
  scaleGridLineColor: "rgba(0,0,0,.05)",
  scaleFontColor: "#c5c7cc"
  });
};
  </script>

<script>
// Get the modal
var modal = document.getElementById('myModal');

// Get the button that opens the modal
var btn = document.getElementById("myBtn");

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];

// When the user clicks the button, open the modal 
btn.onclick = function() {
    modal.style.display = "block";
}

// When the user clicks on <span> (x), close the modal
span.onclick = function() {
    modal.style.display = "none";
}

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
    if (event.target == modal) {
        modal.style.display = "none";
    }
}
</script>

<script>
     
function getTotal() {
    var c1 = document.getElementById('1').value;
    var c2 = document.getElementById('2').value;
    var c3 = document.getElementById('3').value;
    var c4 = document.getElementById('4').value;
    var c5 = document.getElementById('5').value;

    var total = (c1*1) + (c2*1) + (c3*1) + (c4*1) + (c5*1);
    document.getElementById("totalcriteria").innerHTML = "Total: " + total + "%";
}
</script>

<!-- js for notif -->
<script>
  $(document).ready(function (){
     setInterval(function(){ 
       $.ajax({
            type: 'GET',
            url: '/adminStoryCount',
            success:function(data){
                document.getElementById('admin_count').innerHTML  = data;
                console.log(data);
            },
            error:function(){
              console.log("");
            }
        });

       $.ajax({
            type: 'GET',
            url: '/adminVoucherCount',
            success:function(data){
                document.getElementById('voucher_count').innerHTML  = data;
                console.log(data);
            },
            error:function(){
              console.log("");
            }
        });

       $.ajax({
            type: 'GET',
            url: '/redeemCount',
            success:function(data){
                document.getElementById('redeem_count').innerHTML  = data;
                console.log(data);
            },
            error:function(){
              console.log("");
            }
        });

      }, 1000);
      });
</script>

    
</body>
</html>