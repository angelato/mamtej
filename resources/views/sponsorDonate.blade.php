<?php 
use App\Sponsor;
$get = Sponsor::where('userid', Auth::id())->where('status',null)->sum('voucherValue');
?>

@extends('layouts.raise')
@section('content')


<head>

<link rel="stylesheet" href="/css/alert.css">
    <link rel="stylesheet" href="/css/form-basic.css">

</head>

<style>
    body {
        background-image: url('/images/bgg.jpg');
        background-size:  1700px 800px;

    }
    #x{
        font-size: 40px;
        color: black;
    }

</style>

<br><br><br><br><br>

    <div class="main-content">

<center><p id="x"></p></center>
        <!-- You only need this form and the form-register.css -->


        <form style="background-color: #f9f9f9;" class="form-basic" action="{{url('/helpxp')}}" method="post">
   
             {{csrf_field()}}

                <input type="hidden" name="patientid" value="{{ $patient->patientid}}">
                <input type="hidden" name="user" value="{{ $patient->userid}}">
                <input type="hidden" name="count" value="{{ $patient->sponsorcount}}">

                    <div class="form-title-row">
@if(Sponsor::where('userid', Auth::id())->where('status', null)->get()->count() == 0)
    <div class="notice error">
    <p>You run out of voucher/s, please avail vouchers first</p>
    <a href="{{ url('/buyvoucher') }}/{{Auth::user()->id }}">Buy Voucher with Bank Deposit</a>&nbsp;&nbsp;
    <a href="{{ url('/paypal') }}">Buy Voucher with PayPal</a>
    </div>
   @endif
   <p style="margin-top: 0px">Current Bank Balance: &#8369; {{Auth::User()->bank_balance}}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Current PayPal Balance: &#8369; {{Auth::User()->paypal_balance}}</p>  
 <br>
                        <h1 style="font-size:20pt">Donate</h1>
                        <p style="font-style: normal; font-family:'Arial Narrow';font-size:13pt">Make an online donation to help our organization provide treatment, care and support to vulnerable children. Fill out the form below to send your donation. Thank you for your help!</p>
                    </div>

                    <p style="color: white; 
                                    font-size: 20pt; 
                                    border-radius: 5px; 
                                    border: 2px solid orange; 
                                    background-color: orange;
                                    margin-top: -30px;  
                                    " id="total">&#8369;</p>

                        <label>
                            <span style="color: #5f5f5f; margin-left: -55px">For:&nbsp;&nbsp;&nbsp;&nbsp;</span>
                            <span style="font-size: 15pt; color: #5f5f5f">{{ $patient->patientname}}</span>
                        </label>
                   <br>

                    <div class="form-row">
                        <center><label>
                            <span style="font-size: 14pt"><input type="radio" value="Anonymous" name="anonymous" >&nbsp;Anonymous</span>
                        </label></center>
                    </div>

                     <!-- angel -->  
                     <center><p style="font-size: 15pt;">&#8369;{{$patient['goal']-$patient['TotalRedeem']}} lacking amount</p></center>
                     <br>  

                    <div class="form-row">
                        <center><label>
                            Amount to be donated:
                            <input type="range" id="myRange" onchange="myFunction()" min="100" max="{{$get}}" step="100" name="amount">&nbsp;<p id="demo"></p>
                        </label></center>
                    </div>
                 
                    <div class="notice info"><p>This is an info notice, you can only donate based on  your availed vouchers denomination.</p></div>
        
<script>
function myFunction() {
    var x = document.getElementById("myRange").value;
    document.getElementById("demo").innerHTML = x;
}
</script>

       <!-- end -->

                   
                    <div class="form-row" style="margin-left: 35px">
                    <center>
                        <button type="submit" name="submit" onclick="check()" style="width:200px;font-size:13pt">Donate</button></center>
                    </div>

        </form>

    </div>

    <input type="hidden" id="lacking" value="{{$patient['goal'] - $patient['donations']}}">


    <script type="text/javascript">
     $(document).ready(function (){
      var to = "";
      var total = "";
      var div=$(this).parent();
        $.ajax({
            type: 'GET',
            url: '/getDen',
            data:{
                '_token': $('p[name=_token]').val()
            },
            success:function(data){

                for(var i=0; i<data.length; i++){

                    var image = data[i].count+'&nbsp;&nbsp;'+'<img src="'+data[i].value+'" width = 200/>'+'&nbsp;&nbsp;';
                    $('#x').append(image);
                } 
                total = data[data.length - 1].total+' TOTAL WORTH OF VOUCHERS';
                $('#total').append(total);
                console.log(data);
            },
            error:function(){
            }
        });
    });
</script>

@if(Session::has('info'))
<div class="alert alert-confirm">
    <script>
        confirm('You cannot donate your desired amount. However you can donate {{ Session::get('avblVoucher', '') }} worth of voucher/s.');
    </script>  
</div>
<!-- @elseif(Session::has('alert'))
    <script>
        confirm('You cannot donate your desired amount. Please buy vouchers first.');
    </script> -->
@elseif(Session::has('excess'))
    <script>
        confirm('Your exceed donation will surely help others. Thank you!');
    </script>

@elseif(Session::has('success'))
    <script>
        alert('Successful Donation');
    </script>

@elseif(Session::has('message'))
    <script>
        alert('Cant donate with this amount');
    </script>
@elseif(Session::has('notenough'))
    <script>
        alert('You dont have enough vouchers');
    </script>



@elseif(Session::has('donate'))
    <script>
        alert('You cant donate more than {{ Session::get('lacking', '') }}.');
    </script>

@endif

<div id="fh5co-feature-product" class="fh5co-section-gray">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 text-center heading-section">
                        <h3 style="font-size: 23pt">On-going Stories.</h3>
                        <p style="color: #3d3d29">Make an online donation to help our organization provide treatment, care and support to vulnerable people.</p>
                    </div>
                </div>

               
                @foreach ($data as $patients)

 <center>
<div style="box-sizing: border-box; float: left; width: 30%; padding: 8px; margin-left: 12px;  content: ""; clear: both; display: table;">
 <div class="row row-bottom-padded-md" >
  <div class="col-md-12">
    <ul id="fh5co-portfolio-list">

      <li style="background-image: url( {{ url('storage/picture/'.$patients->filename) }}); width: 100%">
                <a href="http://localhost:8000/list/{{$patients['patientid']}}/view" class="color-3">
                  <div class="case-studies-summary">
                    <span>Give Love</span>
                    <h2>{{$patients->userName->name}}</h2><br>
                    <div align="left"><input type="button" name="donate" value="+Read More" class="btn btn-primary-info"></div>
<br>
                    <div class="progress" style="width: 100%;margin-bottom: 0px">
    <div class="progress-bar" role="progressbar" aria-valuenow="{{$patients->TotalRedeem/$patients->goal*100}}" aria-valuemin="0" aria-valuemax="100" style="width:{{$patients->TotalRedeem/$patients->goal*100}}%">
    </div>
  </div>
  <br>
                <p style="text-align:center;font-size: 20px; color: #fff">P{{number_format($patients->TotalRedeem)}} raised of P{{number_format($patients->goal)}}</p>

                  </div>
                </a> 
            </li>

      </ul>
    </div>
  </div>
</div>
</center>
@endforeach  


                </div>
            </div>
@endsection

