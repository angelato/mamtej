<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>HealthAid - Dashboard</title>
	<link href="css/bootstrap.min.css" rel="stylesheet">
	<link href="css/font-awesome.min.css" rel="stylesheet">
	<link href="css/datepicker3.css" rel="stylesheet">
	<link href="css/styles.css" rel="stylesheet">

	<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	
	<!--Custom Font-->
	<link href="https://fonts.googleapis.com/css?family=Montserrat:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">
	<!--[if lt IE 9]>
	<script src="js/html5shiv.js"></script>
	<script src="js/respond.min.js"></script>
	<![endif]-->
</head>
<body>
	<nav class="navbar navbar-custom navbar-fixed-top" role="navigation">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#sidebar-collapse"><span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span></button>
				<a class="navbar-brand" href="{{ url('/displayusers') }}"><span>Health</span>Aid</a>
			</div>
		</div><!-- /.container-fluid -->
	</nav>
	<div id="sidebar-collapse" class="col-sm-3 col-lg-2 sidebar">
		<div class="profile-sidebar">
			<div class="profile-userpic">
				<img src="http://placehold.it/50/30a5ff/fff" class="img-responsive" alt="">
			</div>
			<div class="profile-usertitle">
				<div class="profile-usertitle-name">{{ Auth::user()->username }}</div>
				<div class="profile-usertitle-status"><span class="indicator label-success"></span>Online</div>
			</div>
			<div class="clear"></div>
		</div>
		<div class="divider"></div>
		<!-- <form role="search">
			<div class="form-group">
				<input type="text" class="form-control" placeholder="Search">
			</div>
		</form> -->
		<ul class="nav menu">
			<li><a href="{{ url('/displayusers') }}"><em class="fa fa-dashboard">&nbsp;</em> Dashboard</a></li>
			<li  class="active">
				<a href="{{ url('/displaypatients') }}"><em class="fa fa-xl fa-users color-teal">&nbsp;</em> Patients</a>
			</li>
			<li><a href="{{ url('/displaysponsors') }}"><em class="fa fa-xl fa-users color-orange">&nbsp;</em> Sponsors</a></li>

			<li><a href="{{ url('/criteria') }}"><em class="fa fa-xl fa-users color-orange">&nbsp;</em> Recommendation Criteria</a></li>
      <li><a href="{{ url('/reco') }}"><em class="fa fa-xl fa-users color-orange">&nbsp;</em> Recommended</a></li>
			
			<li><a class="fa fa-power-off" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">&nbsp;&nbsp;Logout
                                    </a>
                                    
                                  
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
            </li>
		</ul>
	</div><!--/.sidebar-->
		
	<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
		<div class="row">
			<ol class="breadcrumb">
				<li><a href="#">
					<em class="fa fa-home"></em>
				</a></li>
				<li class="active">Dashboard</li>
			</ol>
		</div><!--/.row-->
		
		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">Patients</h1>
			</div>
		</div><!--/.row-->
	

	<div class="panel panel-container">
	<div class="row">
	<table class="table">
	
	<th class="fixed-table-container">Name</th>
	<th class="fixed-table-container">Goal</th>
	<th class="fixed-table-container" colspan="5">Requirements</th>
	<th class="fixed-table-container"></th>
	
	<!-- <th>Illness</th>
	<th>Story</th>
	<th>Amount</th> --><br>



<p id="patient">
	@foreach ($approve as $apr)
		<tr class="fixed-table-container">
			<td class="fixed-table-container">{{$apr->patient->userName->fname}} {{$apr->patient->userName->lname}}</td>
			<td class="fixed-table-container">{{$apr->patient->goal}}</td>
		@foreach($apr->picture as $ap)
			<td class="fixed-table-container"><img src="{{  url('storage/picture/'.$ap->filename) }}" width="100px" height="100px" /></td>
			
		@endforeach

			<td class="fixed-table-container" align="center"><input type="button" value="Confirmed" id="confirm" class="btn btn-danger" /></td>

	 
</script>

		</tr>
		
@endforeach



</p>
</table><br><br>


				
			</div><!--/.row-->
		</div>
		
			</div><!--/.col-->
			
		</div><!--/.row-->
	</div>	<!--/.main-->
	
	<script src="js/jquery-1.11.1.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/chart.min.js"></script>
	<script src="js/chart-data.js"></script>
	<script src="js/easypiechart.js"></script>
	<script src="js/easypiechart-data.js"></script>
	<script src="js/bootstrap-datepicker.js"></script>
	<script src="js/custom.js"></script>
	<script>
		window.onload = function () {
	var chart1 = document.getElementById("line-chart").getContext("2d");
	window.myLine = new Chart(chart1).Line(lineChartData, {
	responsive: true,
	scaleLineColor: "rgba(0,0,0,.2)",
	scaleGridLineColor: "rgba(0,0,0,.05)",
	scaleFontColor: "#c5c7cc"
	});
};
	</script>
		




