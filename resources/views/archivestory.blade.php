@extends('layouts.raise')
@section('content')
<link rel="stylesheet" href="/css/form-basic.css">
<style>
     body {
        background-image: url('/images/bgg.jpg');
        background-size:  1700px 800px;
    }
    #x{
        font-size: 40px;
        color: black;
    }
</style>

<br><br><br><br>
<div class="row">
          <div class="col-md-12 text-center heading-section">
          @if($denied->count() <= 1)
            <span style="font-size: 20pt">Archive Story</span>
          @else
            <span style="font-size: 20pt">Archive Stories</span>
          @endif
          <p>Stories that have been denied.</p>
          </div>

@foreach ($denied as $den)
<div style="box-sizing: border-box; float: left; width: 24%; padding: 8px; margin-left: 12px;  content: ""; clear: both;
    display: table;">
 <div class="row row-bottom-padded-md" >
  <div class="col-md-12">
    <ul id="fh5co-portfolio-list">

      <li style="background-image: url( {{ url('storage/picture/'.$den->filename) }}); width: 100%">
                <a href="http://localhost:8000/list/{{$den['patientid']}}/view" class="color-3">
                  <div class="case-studies-summary">
                    <span>Give Love</span>
                    <h2>{{$den->stories[0]->storytitle}}</h2><br>
                    
<br>
                    <div class="progress" style="width: 100%;margin-bottom: 0px">
    <div class="progress-bar" role="progressbar" aria-valuenow="{{$den->TotalRedeem/$den->goal*100}}" aria-valuemin="0" aria-valuemax="100" style="width:{{$den->TotalRedeem/$den->goal*100}}%">
    </div>
  </div>
  <br>
                <p style="text-align:center;font-size: 20px">P{{number_format($den->TotalRedeem)}} raised of P{{number_format($den->goal)}}</p>

                  </div>
                </a> 
            </li>

      </ul>
    </div>
  </div>
</div>

@endforeach  
</div>

@endsection