@extends('layouts.updatepage')

@section('content')

<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">

<style>
  body {
        background-image: url('/images/bgg.png');
        top right no-repeat; 
        background-attachment:fixed;
        background-size: cover;
        margin-top: 0px;

    }
</style>
<br><br><br>
<center>{{ $ups->links() }}</center>
<div class="w3-content w3-display-container" style="width: 50%">
@foreach ($ups as $u)
@foreach ($u->picture as $update)

<div class="w3-display-container mySlides">
  <img src="{{url('storage/picture/'.$update->filename)}}" style="width:100%; max-width: 100%;">
  <div style="letter-spacing: 0.1em; background-color: #f3f3f3; border: 1px solid #e6e6e6; height: 200px" class="w3-display-middle-bottom w3-large w3-container w3-padding-16">
    <strong style="letter-spacing: 0.1em; font-size: 13pt; color: #1a1a1a">{{$u->storytitle}}</strong><br>
    <span style="letter-spacing: 0.1em; font-size: 11pt; color: #3385ff; font-style: italic;">Date Posted: {{$u->created_at->format('F d, Y')}}</span><br>
    <span style="letter-spacing: 0.1em; font-size: 10pt; color: #3385ff; font-style: italic;">Time: {{$u->created_at->format('H:i A')}}</span><br><br>
    {{$u->story}}<br>
    <a href="http://localhost:8000/list/{{$u['patientid']}}/view" style="float: right;text-decoration: underline;">go back</a>
  </div>
</div>
<br>
@endforeach
@endforeach

 <button class="w3-button w3-display-left w3-black" onclick="plusDivs(-1)">&#10094;</button>
<button class="w3-button w3-display-right w3-black" onclick="plusDivs(1)">&#10095;</button>
</div>



<script>
var slideIndex = 1;
showDivs(slideIndex);

function plusDivs(n) {
  showDivs(slideIndex += n);
}

function showDivs(n) {
  var i;
  var x = document.getElementsByClassName("mySlides");
  if (n > x.length) {slideIndex = 1}    
  if (n < 1) {slideIndex = x.length}
  for (i = 0; i < x.length; i++) {
     x[i].style.display = "none";  
  }
  x[slideIndex-1].style.display = "block";  
}
</script>

</div>

@endsection
