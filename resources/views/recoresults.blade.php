@extends('layouts.raise')
@section('content')
<head>
	 <link rel="stylesheet" href="/css/form-basic.css">
	 <style>
    body {
        background-image: url('/images/bgg.png');
        top right no-repeat; 
        background-attachment:fixed;
        background-size: cover;
        margin-top: 0px;
    }
    #div{
    	background-color:#f7e5c5 ;
    	opacity: 0.8;
    	padding: 5px;
    	width: 800px; 
    	height: 475px;
    	display: inline-block;
    	border: 3px solid black;
    	border-radius: 20px;
    }
    #div_f{
    	background-color:#f7e5c5 ;
    	opacity: 0.8;
    	padding: 5px;
    	width: 1630px; 
    	height: 475px;
    	display: inline-block;
    	border: 3px solid black;
    	border-radius: 20px;
    }
    h1{
    	font-family: Verdana; 
    	text-decoration: underline orange;
    }
    h3{
    	font-family: Verdana;
    }
    table {
    	font-family: arial, sans-serif;
    	border-collapse: collapse;
    	width: 100%;
    	color: black;
	}
	td, th {
    	border: 1px solid #dddddd;
    	text-align: left;
    	padding: 8px;
    	color: black;
	}
	tr:nth-child(even) {
    	background-color: #dddddd;
	}
</style>
</head>
<center>
<div class="form-title-row">
    <h1>Recommendation Results</h1>
</div>
<br>
<div id="div">
	<h3>Patients with Donation History</h3>
	<table>
		<tr>
		<th>Rank</th>
		<th>Patient's name</th>
		<th>Donation Count</th>
		<th>Percentage Gained</th>
		</tr>
		@foreach($patients as $pnt)
		<tr>
			<td>{{$pnt['rank']}}</td>
			<td>{{$pnt['patientname']}}</td>
			<td>{{$pnt['count']}}</td>
			<td>{{$pnt['points']}}%</td>
		</tr>
		@endforeach
	</table>
</div>&nbsp;
<div id="div">
	<h3>Patients with Longest Duration</h3>
	<table>
		<tr>
		<th>Rank</th>
		<th>Date</th>
		<th>Patient's Name</th>
		<th>Percentage Gained</th>
		</tr>
		@foreach($longestDuration as $pnt)
		<tr>
			<td>{{$pnt['rank']}}</td>
			<td>{{$pnt['created_at']->format('F d, Y')}}</td>
			<td>{{$pnt['patientname']}}</td>
			<td>{{$pnt['points']}}%</td>
		</tr>
		@endforeach
	</table>
</div>
<br><br>
<div id="div">
	<h3>Patients with Highest Need</h3>
	<table>
		<tr>
		<th>Rank</th>
		<th>Patient's Name</th>
		<th>Lacking Amount</th>
		<th>Percentage Gained</th>
		</tr>
		@foreach($highest_need as $pnt)
		<tr>
			<td>{{$pnt['rank']}}</td>
			<td>{{$pnt['patientname']}}</td>
			<td>{{$pnt['lacking']}}</td>
			<td>{{$pnt['points']}}%</td>
		</tr>
		@endforeach
	</table>
</div>&nbsp;
<div id="div">
	<h3>Patients based on Condition</h3>
	<table>
		<tr>
		<th>Rank</th>
		<th>Patient's Name</th>
		<th>Critical Level</th>
		<th>Percentage Gained</th>
		</tr>
		@foreach($condition as $pnt)
		<tr>
			<td>{{$pnt['rank']}}</td>
			<td>{{$pnt['patientname']}}</td>
			<td>{{$pnt['condition']}}</td>
			<td>{{$pnt['points']}}%</td>
		</tr>
		@endforeach
	</table>
</div><br><br>
<div id="div_f">
	<h3>Summary of Patient's Percentage</h3>
	<table>
		<tr>
		<th>Rank</th>
		<th>Patient's Name</th>
		<th>Lacking Percentage</th>
		<th>Donation Count</th>
		<th>Condition</th>
		<th>Posted Percentage</th>
		<th>Total Percentage Gained</th>
		</tr>
		@foreach($finalList as $pnt)
		<tr>
			<td>{{$pnt['rank']}}</td>
			<td>{{$pnt['patientname']}}</td>
			@if(array_key_exists('lacking', $pnt))
				<td>{{$pnt['lacking']}}%</td>
			@else
				<td>--</td>
			@endif
			@if(array_key_exists('count', $pnt))
				<td>--</td>
			@else
				<td>--</td>
			@endif
			<td>{{$pnt['condition']}}%</td>
			<td>{{$pnt['c_points']}}%</td>
			<td>{{$pnt['sum']}}%</td>
		</tr>
		@endforeach
	</table>
</div><br><br><br><br>
</center>
@endsection