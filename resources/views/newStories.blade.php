<?php 
use App\Patient;
$onGoing = Patient::where('storystatus','=','approved')->get();
// echo $onGoing;
 ?>


@extends('layouts.home')
@section('content')

<br><br><br><br>

@if(Session::has('message'))
    <center><div style="border: 1px solid white;width: 500px;text-align:center;">
      <h2 style="font-family:Verdana">{{ Session::get('message') }}</h2> 
    </div></center> 
@elseif(Session::has('noNew'))
    <center><div style="border: 1px solid white;width: 500px;text-align:center;">
      <h2 style="font-family:Verdana">{{ Session::get('noNew') }}</h2> 
    </div></center>
@elseif(Session::has('noLong'))
    <center><div style="border: 1px solid white;width: 500px;text-align:center;">
      <h2 style="font-family:Verdana">{{ Session::get('noLong') }}</h2> 
    </div></center>
@elseif(Session::has('noMost'))
    <center><div style="border: 1px solid white;width: 500px;text-align:center;">
      <h2 style="font-family:Verdana">{{ Session::get('noMost') }}</h2> 
    </div></center>
@endif

<br>

<div style="text-align: center;padding-left:20%">
<a href="{{url('/mostDonated')}}" class="action-button shadow animate blue">Most Donated</a>
  <a href="{{url('/longestStory')}}" class="action-button shadow animate red">Longest Duration</a>
  <a href="{{ url('/newStories') }}" class="action-button shadow animate green">New Stories</a>
  <!-- <a href="#" class="action-button shadow animate yellow">You?</a> -->
</div>
<br><br><br><br>
<div>


  <h3 style="padding-left: 20px">New Stories</h3> 
  @foreach ($new as $result)
  <div style="box-sizing: border-box; float: left; width: 24%; padding: 8px; margin-left: 12px;  content: ""; clear: both;
    display: table;">
 <div class="row row-bottom-padded-md" >
  <div class="col-md-12">
    <ul id="fh5co-portfolio-list">

      <li style="background-image: url( {{ url('storage/picture/'.$result->filename) }}); width: 100%">
                <a href="http://localhost:8000/list/{{$result['patientid']}}/view" class="color-3">
                  <div class="case-studies-summary">
                    <span>Give Love</span>
                    <h2>{{$result->stories[0]->storytitle}}</h2><br>
                    <div align="left"><input type="button" name="donate" value="+Read More" class="btn btn-primary-info"></div>
<br>
                    <div class="progress" style="width: 100%;margin-bottom: 0px">
    <div class="progress-bar" role="progressbar" aria-valuenow="{{$result->TotalRedeem/$result->goal*100}}" aria-valuemin="0" aria-valuemax="100" style="width:{{$result->TotalRedeem/$result->goal*100}}%">
    </div>
  </div>
  <br>
                <p style="text-align:center;font-size: 20px">P{{number_format($result->TotalRedeem)}} raised of P{{number_format($result->goal)}}</p>

                  </div>
                </a> 
            </li>

      </ul>
    </div>
  </div>
</div>

@endforeach  
</div>


<br><br><br><br>
<div class="row">
          <div class="col-md-12 text-center heading-section">
            <span style="font-size: 20pt">On-Going Stories</span>
            <p>Make an online donation to help our organization provide treatment, care and support to vulnerable people.</p>
          </div>

<!-- start div para sa stories -->

 @foreach ($onGoing as $data)


<div style="box-sizing: border-box; float: left; width: 24%; padding: 8px; margin-left: 12px;  content: ""; clear: both;
    display: table;">
 <div class="row row-bottom-padded-md" >
  <div class="col-md-12">
    <ul id="fh5co-portfolio-list">

      <li style="background-image: url( {{ url('storage/picture/'.$data->filename) }}); width: 100%">
                <a href="http://localhost:8000/list/{{$data['patientid']}}/view" class="color-3">
                  <div class="case-studies-summary">
                    <span>Give Love</span>
                    <h2>{{$data->stories[0]->storytitle}}</h2><br>
                    <div align="left"><input type="button" name="donate" value="+Read More" class="btn btn-primary-info"></div>
<br>
                    <div class="progress" style="width: 100%;margin-bottom: 0px">
    <div class="progress-bar" role="progressbar" aria-valuenow="{{$data->TotalRedeem/$data->goal*100}}" aria-valuemin="0" aria-valuemax="100" style="width:{{$data->TotalRedeem/$data->goal*100}}%">
    </div>
  </div>
  <br>
                <p style="text-align:center;font-size: 20px">P{{number_format($data->TotalRedeem)}} raised of P{{number_format($data->goal)}}</p>

                  </div>
                </a> 
            </li>

      </ul>
    </div>
  </div>
</div>

@endforeach 


<!-- end div -->

<br><br><br><br>
        <div class="row">
          <div class="col-md-4 col-md-offset-4 text-center animate-box">
            <a href="{{ url('/donateAny') }}/{{Auth::user()->id }}" class="btn btn-primary btn-lg">Raise Funds</a>
          </div>
        </div>

        <br><br><br>

    <!-- fh5co-blog-section -->
    <footer>
      <div id="footer">
        <div class="container">
          <div class="row">
            <div class="col-md-6 col-md-offset-3 text-center">
              
              <p><a href="{{ url('/home') }}"><img data-u="image" src="images/logoHA.png" width="120cm" height="90cm" /></a>. All Rights Reserved. </a></p>
            </div>
          </div>
        </div>
      </div>
    </footer>



@endsection



  










