@extends('layouts.historyui')
@section('content')
<?php
use App\Donation;

?>

<head>
    <title>History</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->  
    <link rel="icon" type="image/png" href="images/icons/favicon.ico"/>
<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="vendor/animate/animate.css">
<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css">
<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="vendor/perfect-scrollbar/perfect-scrollbar.css">
<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="css/util.css">
    <link rel="stylesheet" type="text/css" href="css/main.css">
<!--===============================================================================================-->
</head>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script>
setInterval(function(){
    $(document).ready(function (){
      var to = "";
      var div=$(this).parent();
        $.ajax({
            type: 'GET',
            url: '/total',
            data:{
                '_token': $('input[name=_token]').val()
            },
            success:function(data){
                if(data.length == 0){
                    to+=`<center><h3>No record as of now</h3></center>`;
                    $('#patientTotal').html(to);
                }
                for(var i=0; i<data.length; i++){
                    var date = data[i].created_at;
                    var dt = new Date(date);
                    var redeem = data[i].TotalRedeem - data[i].redeemed;
                    var options = { year: 'numeric', month: 'long', day: 'numeric' };
                    to+=`
                    <form action="{{url('/confirm')}}/`+data[i].patientid+`">
                        <table>
                            <tbody>
                                <tr class="row100 body">
                                    <td class="cell100 column1">`+dt.toLocaleDateString("en-US", options)+`</td>
                                    <td class="cell100 column2">`+data[i].goal+`</td>
                                    
                                        <td class="cell100 column3"><input type="submit" class="btn btn-primary" name="Redeem" value="Redeem" ></td>
                                    
                                        
                                     <td class="cell100 column4"><div id="">`+redeem+`</td>
                                </tr>         
                            </tbody>
                        </table>
                    </form>
                    `;
                
                }$('#patientTotal').html(to);
                 
                console.log(to);
            },
            error:function(){
            }
        });
    });
},2000);
</script>


<script>
// setInterval(function(){
    
        function donate(){
        var from = $('#from').val();
        var toDate = $('#to').val();

      var to = "";
      var div=$(this).parent();
        $.ajax({
            type: 'GET',
            url: '/filterDonations/'+from+'/'+toDate,
            success:function(data){
                for(var i=0; i<data.length; i++){
                    var date = data[i].updated_at;
                    var dt = new Date(date);
                    var options = { year: 'numeric', month: 'long', day: 'numeric' };
                   
                    to +=`
                    <table >
                            <tbody >
                       <tr class="row100 body">
                                    <td class="cell100 column1">`+dt.toLocaleDateString("en-US", options)+`</td>
                                    <td class="cell100 column2">`+data[i].voucherValue+`</td>
                                  
                                   <td class="cell100 column3">`+data[i].fname+` `+data[i].lname+`</td>
                                  
                                    
                                  
                                </tr>
                                </tbody>          
                        </table>
                    `;                    
                                
                }               
                 $('#donations').html(to);
                 
                console.log(to);
            },
            error:function(){
            }
        });
        }
// },3000);

</script>
 
<style>
body{
    background-image: url('/images/bgg.jpg');
    background-size:  1700px 800px;
}
</style>

<br><br><br><br>
<div style="margin-right:110px;margin-left:70px;margin-top:10px;border-radius:15px 15px;">
<div class="limiter" align="center">
        
            <div class="wrap-table100"><p style="font-size:15pt; font-family:Arial; color: black; text-align: left; margin-top:40px">Current Story</p>
                <div class="table100 ver5 m-b-110">
                    <div class="table100-head" style="background-color:#00b8e6">
                        <table>
                            <thead>
                                <tr class="row100 head">
                                    <th class="cell100 column1" style="color:black;font-family:Arial">Date</th>
                                    <th class="cell100 column2" style="color:black;font-family:Arial">Goal</th>
                                    <th class="cell100 column3" style="color:black;font-family:Arial"></th>
                                    <th class="cell100 column4" style="color:black;font-family:Arial">Total</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
        
                    <div class="table100-body js-pscroll" id="patientTotal"></div>
                  </div>
                


    <div class="wrap-table100"><p style="font-size:15pt;font-family:Arial;color:black; text-align: left;">Redeemed and Success Stories</p>
        <div class="table100 ver5 m-b-110">
                    <div class="table100-head" style="background-color:#00b8e6">
                        <table>
                            <thead>
                                <tr class="row100 head">
                                    <th class="cell100 column1" style="color:black;font-family:Arial">Date</th>
                                    <th class="cell100 column2" style="color:black;font-family:Arial">Goal</th>
                                    <th class="cell100 column3" style="color:black;font-family:Arial">Amount Redeemed</th>
                                </tr>
                            </thead>
                        </table>
                    </div>

                     @foreach ($redeemdetails as $redeem)

                    <div class="table100-body js-pscroll">
                        <table style="margin-left: 15px">
                            <tbody>
                                <tr class="row100 body">
                                    <td class="cell100 column1">{{$redeem->created_at->format('F d, Y')}}</td>
                                    <td class="cell100 column2">{{$redeem->goal}}</td>
                                    <td class="cell100 column3">{{$redeem->TotalRedeem}}</td>
                                </tr>
                                 @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

    <br>
    <div class="wrap-table100"><p style="font-size:15pt;font-family:Arial;color:black; text-align: left;">HealthAid Donations</p>
    <div class="table100 ver5 m-b-110">
                    <div class="table100-head" style="background-color:#00b8e6">
                                                <table>
                            <thead>
                                <tr class="row100 head">
                                    <th class="cell100 column1" style="color:black;font-family:Arial">Total HealthAid Donations</th>
                                </tr>
                            </thead>
                        </table>
                    </div>





                    <div class="table100-body js-pscroll">
                        <table id="donations" style="margin-left: 15px">
                            <tbody >
                                <tr class="row100 body">
                                    <td class="cell100 column1"><center>@foreach ($helpxp as $h)
                      &#8369;{{$h->sum}}
                    
                    @endforeach</center></td>
                                    
                                </tr>
                            </tbody>
                         
                        </table>
                    </div>
                </div>
            </div>  

            <div class="wrap-table100">
 
    <table align="left">
        <tr>
            <td>From:</td>
            <td><input type="date" name="from" id="from" width="80px"> </td>
        </tr> 
            <td>To:</td>
            <td><input type="date" name="to" id="to"></td>
        </tr> 
        </tr> 
            <td><input type="submit" id="filter" class="btn btn-success" value="Filter" onclick="donate()"></td>
        </tr> 
       
        
    </table>

    <p style="font-size:15pt;font-family:Arial;color:black; text-align: left;">History of Donations</p>
        <div class="table100 ver5 m-b-110">
                    <div class="table100-head" style="background-color:#00b8e6">
                        <table>
                            <thead>
                                <tr class="row100 head">
                                    <th class="cell100 column1" style="color:black;font-family:Arial">Date</th>
                                    <th class="cell100 column2" style="color:black;font-family:Arial">Amount Donated</th>
                                    <th class="cell100 column3" style="color:black;font-family:Arial">To whom</th>
                                </tr>
                            </thead>
                        </table>
                    </div>





                    <div class="table100-body js-pscroll">
                        <table id="donations"  style="margin-left: 15px">
                            <tbody >
                            @foreach ($sponsorCollect as $sponsors)
                                <tr class="row100 body">
                                    <td class="cell100 column1">{{$sponsors->updated_at->format('F d, Y')}}</td>
                                    <td class="cell100 column2">{{$sponsors->voucherValue}}</td>
                                    @foreach ($sponsors->donation as $sponsors)
                                    @if($sponsors->patient == null)
                                    <td class="cell100 column3">HealthAid</td>
                                    @else
                                    <td class="cell100 column4">{{$sponsors->patient->userName->fname}} {{$sponsors->patient->userName->lname}}</td>
                                    @endif
                                    
                                    @endforeach
                                </tr>
                                 @endforeach
                            </tbody>
                         
                        </table>
                    </div>
                </div>
            </div>  
        </div>
    </div>
</div>

<br>
@endsection
