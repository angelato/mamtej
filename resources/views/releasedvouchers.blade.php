

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>HELPXP - Dashboard</title>
	<link href="/css/bootstrap.min.css" rel="stylesheet">
	<link href="/css/font-awesome.min.css" rel="stylesheet">
	<link href="/css/datepicker3.css" rel="stylesheet">
	<link href="/css/styles.css" rel="stylesheet">

	<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	
	<!--Custom Font-->
	<link href="https://fonts.googleapis.com/css?family=Montserrat:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">
	<!--[if lt IE 9]>
	<script src="js/html5shiv.js"></script>
	<script src="js/respond.min.js"></script>
	<![endif]-->
</head>
<body>





	<nav class="navbar navbar-custom navbar-fixed-top" role="navigation">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#sidebar-collapse"><span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span></button>
				<a class="navbar-brand" href="{{ url('/displayusers') }}"><span>Health</span>AidP</a>
			</div>
		</div><!-- /.container-fluid -->
	</nav>
	<div id="sidebar-collapse" class="col-sm-3 col-lg-2 sidebar">
		<div class="profile-sidebar">
			<div class="profile-userpic">
				<img src="http://placehold.it/50/30a5ff/fff" class="img-responsive" alt="">
			</div>
			<div class="profile-usertitle">
				<div class="profile-usertitle-name">{{ Auth::user()->username }}</div>
				<div class="profile-usertitle-status"><span class="indicator label-success"></span>Online</div>
			</div>
			<div class="clear"></div>
		</div>
		<div class="divider"></div>
		<!-- <form role="search">
			<div class="form-group">
				<input type="text" class="form-control" placeholder="Search">
			</div>
		</form> -->
		<ul class="nav menu">
			<li><a href="{{url('/displayusers')}}"><em class="fa fa-dashboard">&nbsp;</em> Dashboard</a></li>
			<li class="active"><a href="{{ url('/displaypatients') }}"><em class="fa fa-users">&nbsp;</em> Patients</a></li>
			<li><a href="{{ url('/displaysponsors') }}"><em class="fa fa-xl fa-users color-orange">&nbsp;</em> Sponsors</a></li>
			<li><a class="fa fa-power-off" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">&nbsp;&nbsp;Logout
                                    </a>
                                    
                                  
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
            </li>
		</ul>
	</div><!--/.sidebar-->
		
	<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
		<div class="row">
			<ol class="breadcrumb">
				<li><a href="#">
					<em class="fa fa-home"></em>
				</a></li>
				<li class="active">Dashboard</li>
			</ol>
		</div><!--/.row-->
		
		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">Patients</h1>
			</div>
		</div><!--/.row-->


	<div class="panel panel-container"><p style="font-size: 15pt; margin-left: 10px;">Released Vouchers</p>
	<div class="row">

	<table class="table">
	
	<th class="fixed-table-container">Recipient Name</th>
	<th class="fixed-table-container">Date</th>
	<th class="fixed-table-container">Patient Name</th>
	
	<th class="fixed-table-container">Amount Redeemed</th>
	<th class="fixed-table-container">Status</th>
	<th class="fixed-table-container">Download Voucher</th>
	<th></th>
	<!-- <th>Illness</th>
	<th>Story</th>
	<th>Amount</th> --><br>


<p id="patient">
	
	@foreach ($redeem as $rls)
	@if($rls->story->patient->action == "Released")
		<tr id="release" class="fixed-table-container">
			<td class="fixed-table-container">{{$rls->recipient}}</td>
			<td class="fixed-table-container">{{$rls->story->patient->updated_at->format('F d, Y')}}</td>
			<td class="fixed-table-container">{{$rls->story->patient->patientname}}</td>
			<td class="fixed-table-container">{{$rls->amountRedeemed}}</td>
			<td class="fixed-table-container">{{$rls->story->patient->action}}</td>

			<form method="POST" action="{{url('/released')}}">
				{{csrf_field()}} 
				<input type="hidden" name="patientid" value="{{$rls->patientid}}">

			<td class="fixed-table-container"><a href="{{ url('/helpxp/pdf') }}" id="pdfclick" class="btn btn-danger" onclick="clickAndDisable(this);">PDF</a>
			</td>

			<td><button type="submit" class="btn btn-primary">Claim</button></td>
			

		</form>
			

			<!--  -->
		</tr>
		@endif
		@endforeach
</p>
</table>
				
			</div><!--/.row-->
		</div>
		
			</div><!--/.col-->
			
		</div><!--/.row-->
	</div>	<!--/.main-->

	<script>
	document.getElementById('pdfclick').onclick = function(){
		document.getElementById('pdfclick').innerHTML = "Downloaded";
	} 
	</script>
	
	<script src="js/jquery-1.11.1.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/chart.min.js"></script>
	<script src="js/chart-data.js"></script>
	<script src="js/easypiechart.js"></script>
	<script src="js/easypiechart-data.js"></script>
	<script src="js/bootstrap-datepicker.js"></script>
	<script src="js/custom.js"></script>
	<script>
		window.onload = function () {
	var chart1 = document.getElementById("line-chart").getContext("2d");
	window.myLine = new Chart(chart1).Line(lineChartData, {
	responsive: true,
	scaleLineColor: "rgba(0,0,0,.2)",
	scaleGridLineColor: "rgba(0,0,0,.05)",
	scaleFontColor: "#c5c7cc"
	});
};
	</script>
		


				<script> 
   function clickAndDisable(link) {
     // disable subsequent clicks
     link.onclick = function(event) {
        event.preventDefault();
     }
   }   
</script>


