 @extends('layouts.patient')

@section('content')



<head>

    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="/css/apply-form.css">
    <script src="/js/apply-js.js"></script>
    <link rel="stylesheet" href="/css/fileupload.css">
    <script src="/js/fileupload.js"></script>
    <link rel="stylesheet" href="/css/fileupload2.css">
    <script src="/js/fileupload2.js"></script>
    <script class="jsbin" src="https://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
    <link rel="stylesheet" href="/css/patientdetail.css">


</head>

<br><br>

    <div class="main-content" >

    <!-- angel edited -->

              <section class="form-box">
            <div class="container" style="width: 105%;padding-left:0px">
                
                <div class="row">
                    <div class="col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2 col-lg-6 col-lg-offset-3 form-wizard">
          
            <!-- Form Wizard -->
        <form role="form" action="{{url('/singlelist')}}" method="post" enctype="multipart/form-data">
{{csrf_field()}}  


                        <h3>Share your Story</h3>
                        <p>Fill all form field to go next step</p>
              
              <!-- Form progress -->
                        <div class="form-wizard-steps form-wizard-tolal-steps-3" style="width:100%">
                          <div class="form-wizard-progress">
                              <div class="form-wizard-progress-line" data-now-value="12.25" data-number-of-steps="4" style="width: 12.25%;"></div>
                          </div>
                <!-- Step 1 -->
                          <div class="form-wizard-step active">
                            <div class="form-wizard-step-icon"><i class="fa fa-file-text-o" aria-hidden="true" style="padding-top:10px"></i></div>
                            <p>Story Details</p>
                          </div>
                <!-- Step 1 -->
                
                <!-- Step 2 -->
                          <div class="form-wizard-step">
                            <div class="form-wizard-step-icon"><i class="fa fa-user" aria-hidden="true" style="padding-top:10px"></i></div>
                            <p>Patient Details</p>
                          </div>
                <!-- Step 2 -->
                
                <!-- Step 3 -->
                <div class="form-wizard-step">
                            <div class="form-wizard-step-icon"><i class="fa fa-files-o" aria-hidden="true" style="padding-top:10px"></i></div>
                            <p>Requirements</p>
                          </div>
                <!-- Step 3 -->

                        </div>
              <!-- Form progress -->
                        
              
              <!-- Form Step 1 -->
      <fieldset>

                            <h4>Story Details: <span>Step 1 - 3</span></h4>
                <div class="form-group">
                              <label>Story Title: <span>*</span></label>
                                    <input type="text" name="title" placeholder="Title of your story" class="form-control required" style="width:100%">
                </div>

                <div class="form-group">
                              <label>Your Story: <span>*</span></label>
                              <p style="font-size:15px;color:black;float:right"><span id="display_count">0</span>/250 words</p>
                               <textarea name="story" id="countWord" cols="100" rows="15" style="padding-left:50px;border-color:#1FB264" class="form-control required"></textarea>
                                    
                </div>

                <div class="form-group">
                              <label>Amount to Raise: <span>*</span></label>
                              <input type="number" name="goal" placeholder="Amount you want to raise" class="form-control required" style="width:100%">
                                    
                </div>

                <div class="form-wizard-buttons">
                                    <button type="button" class="btn btn-next">Next</button>
                </div>
      </fieldset>
              <!-- Form Step 1 -->

              <!-- Form Step 2 -->
      <fieldset>

                                <h4>Patient Details : <span>Step 2 - 3</span></h4>
                <div class="form-group">

    <div class="file-upload" class="form-control required">
    <button class="file-upload-btn" type="button" onclick="$('.file-upload-input').trigger( 'click' )">Upload profile photo of your story</button>

    <div class="image-upload-wrap">
    <input class="file-upload-input" name="profile" type='file' onchange="readURL(this);" accept="image/*" / required autofocus>
    <div class="drag-text">
      <h3>Drag or upload photo</h3>
    </div>
  </div>
  <div class="file-upload-content">
    <img class="file-upload-image" src="#" alt="your image" />
    <div class="image-title-wrap">
      <button type="button" onclick="removeUpload()" class="remove-image">Remove <span class="image-title">Uploaded Image</span></button>
    </div>
  </div>
</div>
              
                </div>

                <div class="form-group">
                      <label>Beneficiary Name: <span>*</span></label>
                      <input type="text" name="bname" placeholder="Complete name of the beneficiary" class="form-control required" style="width:100%">
                </div>

                <div class="form-group">
                      <label>Cause of Admission: <span>*</span></label>
                      <input type="text" name="illness" placeholder="Illness of the beneficiary" class="form-control required" style="width:100%">
                </div>

                <div class="form-group">
                      <label>Patient's Condition: </label>
                      <select class="form-control" name="condition">
                        <option value="1">Undetermined</option>
                        <option value="2">Good</option>
                        <option value="3">Stable</option>
                        <option value="4">Serious</option>
                        <option value="5">Critical</option>
                      </select>
                </div>
                
                <div class="form-wizard-buttons">
                      <button type="button" class="btn btn-previous">Previous</button>
                      <button type="button" class="btn btn-next">Next</button>
                </div>
      </fieldset>
              <!-- Form Step 2 -->

              <!-- Form Step 3 -->
      <fieldset>

                                <h4>Requirements: <span>Step 3 - 3</span></h4>

                <div class="form-group">
              <div class="container p-y-1">

     <center><br><br>          
  <div class="row m-b-1">
    <div class="col-sm-6 offset-sm-3">
        <button type="button" class="btn btn-primary btn-block" onclick="document.getElementById('inputFile').click()" style="width:140%;font-size:20px">Medical Abstract</button>
          <div class="form-group inputDnD" style="font-size:15px;width:15%">
             <label class="sr-only" for="inputFile">File Upload</label>
              <input type="file" name="medicalAbstract" class="form-control-file text-primary font-weight-bold" id="inputFile" accept="image/*" onchange="readUrl(this)" data-title="Drag and drop a file" required autofocus>
          </div>
      </div>
  </div>
      </center>
    <br>

<center>
   <div class="row m-b-1">
      <div class="col-sm-6 offset-sm-3">
          <button type="button" class="btn btn-success btn-block" onclick="document.getElementById('inputFile1').click()" style="width:140%;font-size:20px">Medical Certificate</button>
          <div class="form-group inputDnD" style="font-size:15px;width:15%">
              <label class="sr-only" for="inputFile1">File Upload</label>
              <input type="file" name="medicalCertificate" class="form-control-file text-success font-weight-bold" id="inputFile1" accept="image/*" onchange="readUrl(this)" data-title="Drag and drop a file" required autofocus>
          </div>
      </div>
   </div>
</center>
  <br>

<center>
  <div class="row m-b-1">
    <div class="col-sm-6 offset-sm-3">
        <button type="button" class="btn btn-warning btn-block" onclick="document.getElementById('inputFile2').click()" style="width:140%;font-size:20px">Valid ID (Government ID's)</button>
          <div class="form-group inputDnD" style="font-size:15px;width:15%">
            <label class="sr-only" for="inputFile2">File Upload</label>
            <input type="file" name="validID" class="form-control-file text-warning font-weight-bold" id="inputFile2" accept="image/*" onchange="readUrl(this)" data-title="Drag and drop a file" multiple required autofocus>
        </div>
    </div>
  </div>
</center>
  <br>

<center>
  <div class="row m-b-1">
    <div class="col-sm-6 offset-sm-3">
        <button type="button" class="btn btn-danger btn-block" onclick="document.getElementById('inputFile3').click()" style="width:140%;font-size:20px">Hospital Bill</button>
          <div class="form-group inputDnD" style="font-size:15px;width:15%">
            <label class="sr-only" for="inputFile3">File Upload</label>
            <input type="file" name="hospitalBill" class="form-control-file text-danger font-weight-bold" id="inputFile3" accept="image/*" onchange="readUrl(this)" data-title="Drag and drop a file" multiple required autofocus>
        </div>
    </div>
  </div>
</center>
  <br>

  <br>

          </div><!-- container pyi -->
                </div><!-- form group -->
<br/>
                  <div class="form-wizard-buttons">
                        <button type="button" class="btn btn-previous">Previous</button>
                        <button type="submit" class="btn btn-submit">Submit</button>
                  </div>
</fieldset>
              <!-- Form Step 3 -->    
                      </form>
            <!-- Form Wizard -->
                    </div>
                </div>
                    
            </div>
        </section>





    <!-- angel end -->
</div>
</body>

</html>

<script>
    $(document).ready(function() {
  $("#countWord").on('keyup', function() {
    var words = this.value.match(/\S+/g).length;

    if (words > 250) {
      // Split the string on first 200 words and rejoin on spaces
      var trimmed = $(this).val().split(/\s+/, 250).join(" ");
      // Add a space at the end to make sure more typing creates new words
      $(this).val(trimmed + " ");
    }
    else {
      $('#display_count').text(words);
      $('#word_left').text(250-words);
    }
  });
}); 
</script>

<script>
    function readUrl(input) {
  
  if (input.files && input.files[0]) {
    let reader = new FileReader();
    reader.onload = (e) => {
      let imgData = e.target.result;
      let imgName = input.files[0].name;
      input.setAttribute("data-title", imgName);
      console.log(e.target.result);
    }
    reader.readAsDataURL(input.files[0]);
  }

}
</script>

@endsection 




