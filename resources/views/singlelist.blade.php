@extends('layouts.single')
@section('content')

<head>
	<link rel="stylesheet" href="/css/alert.css">
	<link rel="stylesheet" href="/css/singlepicmodal.css">
	<!-- close modal -->
	 <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
</head>

<?php use App\stories; ?>

<br><br><br><br>
@if($patient['storystatus'] == "denied")
<center>
<div class="notice error" id="notice" style="height:80px; margin-top: -7px">
<p style="color:#80858e;font-size:13pt">This is a an info notice, your story has been denied, <strong>{{ $patient->denymsg }}</strong> <a href="http://localhost:8000/modify/{{$patient['patientid']}}"><button id="modify" class="btn-danger" style="width:15%; height: 50px; margin-top: 5px; margin-bottom: 5px;">Modify Story</button></a></p>
</div>
</center>
<br><br><br><br><br>
@endif

<div style="float: right; overflow: auto; margin-top: 25px; margin-left: -150px; margin-right: 150px; max-width: 450px; width: 450px; display: block; clear: both; overflow-y: overlay">
	<div style="background-color: #FFF; padding: 35px; height: 300px; min-height: 300px; max-width: 450px; width: 450px">
	<strong style="color: #1C1C1C; font-size: 23pt; letter-spacing: 0.07em">&#8369; {{number_format($patient->TotalRedeem)}}</strong><span style="font-family: Palanquin; font-size: 17pt; color: #1C1C1C; letter-spacing: 0.1em"> out of  P {{number_format($patient->goal)}} goal</span>
	<div style="color: blue; font-style: italic">
		Amount lacking: {{number_format($patient->goal - $patient->TotalRedeem)}}
	</div><br><br>

	<div class="progress" style="max-width: 400px;">
    <div class="progress-bar" role="progressbar" aria-valuenow="{{$patient->TotalRedeem/$patient->goal*100}}" aria-valuemin="0" aria-valuemax="100" style="width:{{$patient->TotalRedeem/$patient->goal*100}}%">
    </div>

  </div>
 <center><p style="color:black">{{number_format($patient->TotalRedeem/$patient->goal*100)}}%</p></center>

	<div align="center">
	@if($patient['goal'] == $patient['TotalRedeem'] || $patient['storystatus'] == "Close")
	<span><button class="btn btn-primary" style="width:170px;height:45px;font-size:17" disabled>Donate Now</button></span><br><br>
	@else
	<span><a href="http://localhost:8000/sponsorDonate/{{ $patient['patientid'] }}" class="btn btn-primary" style="width:350px;height:45px;font-size:17">Donate Now</a></span><br>
	<div class="notice info"><p>Don't have voucher/s yet? Buy voucher/s through 
	<a href="{{ url('/buyvoucher') }}/{{Auth::user()->id }}">Bank Deposit</a> or 
	<a href="{{ url('/paypal') }}">PayPal</a></p>
	</div>
	@endif
	@if(Auth::id() == $patient['userid'])
	<a href="http://localhost:8000/confirm/{{$patient['patientid']}}" class="btn btn-warning" style="width:170px;height:45px;font-size:17;">REDEEM</a>

	@if($patient['storystatus'] !== "Close")
	<!-- close story modal -->
  <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#myModal" style="width:170px;height:45px;font-size:17">CLOSE STORY</button>

  <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">     
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4>Story</h4>
        </div>
        <div class="modal-body">
        <form method="post" action="{{url('/stories')}}">
        {{csrf_field()}}  
          <input type="hidden" name="patientid" value="{{$patient['patientid']}}">
          <p>Do you want to close the story? <button>Yes</button></p>
		</form>
        </div>
          <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Go Back</button>
        </div>
      </div>
      
    </div>
  </div>
  
<!-- modal end -->
	@endif

	<br><br>
	<!-- <a href="http://localhost:8000/redeemhistory/{{ $patient['patientid'] }}" class="btn btn-danger">Redeem History</a> -->
	@endif
	<br>
	<p style="border-bottom: 1px solid #e6e6e6; color: #262626; size: 375px; padding-bottom: 10px; text-align: left;">Created {{ $patient->created_at->format('F d, Y') }}</p>
	</div>
</div><br><br><br><br><br><br>
	<h4 style="font-weight: bold; color: grey; margin-bottom: 2px; padding: 5px; letter-spacing: 0.1em; padding-left: 30px;">Proof:</h4>
	<br><br>
	@foreach($pic as $ap)
		<h4 style="padding-left: 30px;"><img id="img{{$ap->filename}}" src="{{  url('storage/picture/'.$ap->filename)}}" width="400px" height="450px" />
		<div id="m{{$ap->filename}}" class="modal">
  			<span style="float: right; margin-top: -90px; font-size: 100px; cursor: pointer;" class="c{{$ap->filename}}">&times;</span>
  			<img class="modal-content" id="mimg{{$ap->filename}}">
		</div>
	@endforeach
	<center>{{$pic->links()}}</center>

</h4>
	<br>
	<br>
	<div style="background-color:  #ccffdd; padding: 10px; font-size: 17; font-weight: bold; margin-left: 30px; width: 420px">Recent Donations</div>
	<div align="center" style="background-color: white; overflow:scroll; height:500px; margin-left: 30px;">
	@foreach ($real as $rl)
	<table style="border-bottom: 1px solid #e6e6e6; width: 375px; text-align: center; max-width: 420px; padding-left: 30px; size: 14pt; margin-left: 30px;">
	<tr style="margin-bottom: 20px;">
		<td style="color: black; font-weight: bold; padding-bottom: 10px"><span>&#8369;</span>&nbsp;{{ $rl->amountDonated }}</td>
	</tr>
	<br><br>
	<tr>
		<td style="color: green; font-weight: normal; padding-bottom: 15px">{{ $rl->sponsorName }}</td>
	</tr>
	</table>
	@endforeach
	<center>{{$real->links()}}</center>
</div><br><br><br>	
</div>
	
<!-- start sa story title -->
<br>
<div style="padding-left: 120px; background-color: #fff; max-width: 790px; margin-left: 120px; padding-left: 6px; padding-right: 0;padding-top: 30px; overflow: auto; float: left;">
 <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
@if($patient['storystatus'] == "pending")
		<center><div class="alert alert-info">
        <strong>This story will not be published not until the Administrator will confirm it.</strong>
    	</div></center>
@endif            
<div>
	
	<img style="max-width: 750px; min-width: 750px; max-height: 750px; width: 750px; min-height: 500px; height: 500px;" src="{{  url('storage/picture/'.$patient->filename)}}" width="750px" height="500px" />
</div>
<br>
@if(Auth::id() == $patient['userid'])
	<table>
	<tr>	
	<td><a href="/update/{{$patient['patientid']}}" class="btn btn-success" style="width: 350px; height: 40px; font-size: 17;">UPDATE STORY</a></td>
	@if(stories::where('patientid', $patient->patientid)->where('role', null))
	<td><a style="margin-left: 50px; margin-top: 0; width: 350px; height: 40px; font-size: 17;" class="btn btn-info" href="http://localhost:8000/update/{{$story['patientid']}}/view">VIEW UPDATES</a></td>
	@endif
	</tr>
</table>
@endif
@if(Auth::id() != $patient['userid'])
@if(stories::where('patientid', $patient->patientid)->where('role', null))
	<td><a style="margin-top: 0; width: 350px; height: 40px; font-size: 17;" class="btn btn-info" href="http://localhost:8000/update/{{$story['patientid']}}/view">STORY UPDATES</a></td>
	@endif
	@endif


<h2 style="font-weight: bold; letter-spacing: 0.001em; color: #232323; font-family: Arial">{{$story['storytitle']}} </h2>

<!-- end sa story title -->



<div>



<!-- start tabbing -->

	<div style="height: auto; min-height: 400px; max-height: auto; max-width: 750px; width: 750px; display: inline-block; border: 1px solid #f2f2f2; border-color: #cccccc">
		<h4 style="margin-top: 5px; color: black; letter-spacing: 0.001em; padding: 15px; font-size: 13pt">{{$story['story']}}</h4>
	</div>
 
<div>
	<h4 style="letter-spacing: 0.1em; font-weight: bold; color: grey; margin-bottom: 2px; padding: 5px">Information</h4>
	
	<h4 style="margin-top: 5px; font-size: 12pt; color: black; letter-spacing: 0.1em; padding: 15px">
	Beneficiary Name: <strong>{{$patient->patientname}}</strong><br><br>
	Contact Info: <strong><pre style="font-size: 14pt; width: 750px">+63{{$patient->userName->contact}}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{$patient->userName->email}}</pre></strong><br>
	Illness: <strong>{{$patient['illness']}}</strong></h4>
	</div>
</div> <!-- end tabbing -->
	

<!-- start sa update -->


<!-- update end -->

	</div>	
</div>
</div>


<script>

@foreach($pic as $ap)

// Get the modal
var modal = document.getElementById('m{{$ap->filename}}');

// Get the image and insert it inside the modal - use its "alt" text as a caption
var img = document.getElementById('img{{$ap->filename}}');
var modalImg = document.getElementById('mimg{{$ap->filename}}');
img.onclick = function(){
    modal.style.display = "block";
    modalImg.src = this.src;
}


// Get the <span> element that closes the modal
var span = document.getElementsByClassName("c{{$ap->filename}}")[0];

// When the user clicks on <span> (x), close the modal
span.onclick = function() { 
    modal.style.display = "none";
}

@endforeach

</script>


@endsection


