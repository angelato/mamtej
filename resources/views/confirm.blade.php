@extends('layouts.appvoucher')

<script src="https://code.jquery.com/jquery-2.1.4.js"
  integrity="sha256-siFczlgw4jULnUICcdm9gjQPZkw/YPDqhQ9+nAOScE4="
  crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.5/jspdf.min.js"></script> 
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.0.272/jspdf.debug.js"></script>

<style>
    body {
        background-image: url('/images/bgg.jpg');
        background-size:  1700px 800px;
    }
</style>

<div class="container">
<div class="row justify-content-center" style="margin-top: 170px">
<div class="col-md-8">
<div class="card">
<div class="card-header">Donate to HealthAid</div>
<div class="card-body">




<form action="{{ url('/redeem') }}" method="post">
{{csrf_field()}}


<input type="hidden" id="id" name="id" value="{{$details->patientid}}">

<p>Are you sure you want to redeem the voucher with the amount of {{$total}}?</p>

<center><input type="submit" id="button" value="Yes" class="btn btn-primary"></center>
<h6 style="float: right;"><a href="/history">Go back</a></h6>
</form>

</div>
</div>
</div>
</div>
</div>

