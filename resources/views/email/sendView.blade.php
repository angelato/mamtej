<!DOCTYPE html>
<html>
<head>
	<title></title>

	<style>
		#des {
			font-size: 30pt;
			color: #1a75ff;
			font-family: serif;
			font-weight: lighter;
		}

		#verifybutton {
		background-color: #008CBA; /* Green */
  		border: none;
  		color: white;
  		padding: 15px 32px;
  		text-align: center;
  		text-decoration: none;
  		display: inline-block;
  		margin: 4px 2px;
  		cursor: pointer;
	}
	</style>

</head>
<body>


<strong>HELPXP<sub>It's all about you!</sub></strong>
<br>
<div id="des">Verify your email address</div>
<br>
To finish setting up your HELPXP account, we just need to make sure this email address is yours.
<br><br>
Thanks,<br>
The HELPXP team
<br><br>
<a id="verifybutton" class="btn btn-info" role="button" href = "{{ route('sendEmailDone',["email" => $user->email, "token"=>$user->token]) }}" class="btn btn-info" role="button">VERIFY EMAIL</a>
		

	




