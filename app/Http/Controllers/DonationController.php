<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Storage;
use App\Sponsor;
use App\Patient;
use App\User;
use App\Notification;
use App\Donation;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use DB;

class DonationController extends Controller
{
  public function patientDonation(Request $request){ 
    $patient = Patient::findorfail($request->patientid);
    $lacking = $patient['goal'] - $patient['TotalRedeem'];//lacking bill

    $sponsor = Sponsor::select('voucherValue', 'sponsor_serial')->where('userid', Auth::id())->where('status', null)->orderBy('voucherValue', 'desc')->get();
    $name = Auth::user()->fname." ".Auth::user()->lname;//concatenate sponsor name
      // set it to anony if it's checked
      //  set it to sponm if it's not checked
    $radioname = Input::has('anonymous') ? $request->anonymous : $name;
      // $radio = Input::get('anonymous') == true ? $request->anonymous:$name;
    $amount = $request->amount;//patients bill
    $total = $sponsor->sum('voucherValue'); //get total voucher
      //if zero ang input
    if($amount == 0){
      return Redirect::back()->with('message', true);
    }//no amount donated

      
      // $collect = new Collection();
    $V = new Collection();
    $amt = $amount;

    if($amt <= $total && $amount != 0){
      foreach($sponsor as $c){ 
        if($amt - $c['voucherValue'] >= 0){
          $V->push($c);
          $amt -= $c['voucherValue']; 
        }
      }
     
    $avblVoucher =  $V->sum('voucherValue');//collection of vouchers
    if($lacking < $amount){
    $donateCollection = new Collection();
    $excess = new Collection();
    foreach($V as $c){ 
      if($lacking - $c['voucherValue'] >= 0){
        $donateCollection->push($c);
        $lacking -= $c['voucherValue']; 
      }else{
        $excess->push($c);
      }
    }
 
    $toPatient = $donateCollection->sum('voucherValue') + $lacking;
    $total = $patient->TotalRedeem + $toPatient;
    $patient->TotalRedeem = $total;
    $patient->save();

    foreach($donateCollection as $donate){
      $sponsor = Sponsor::findorfail($donate['sponsor_serial']);//vouchers serial
      $sponsor->status = "donated";
      $sponsor->save();
      $donation = new Donation();
      $donation->patientid = $patient->patientid;
      $donation->sponsor_serial = $donate['sponsor_serial'];
      $donation->sponsorName = $radioname;
      $donation->amountDonated = $request->amount;
      $donation->save();
    }

      $lack = $lacking;
      foreach($excess as $ex){
        $excessValue = $ex['voucherValue'] - $lack;
        $sponsor = Sponsor::findorfail($ex['sponsor_serial']);
        $sponsor->status = "donated";
        $sponsor->save();
        $donation = new Donation();
        $donation->patientid = $patient->patientid;
        $donation->sponsor_serial = $ex['sponsor_serial'];
        $donation->sponsorName = $radioname;
        $donation->amountDonated = $lack;
        $donation->excessDonation = $excessValue;
        $donation->save();
        $lack = $lack - $lack;
      }
      return Redirect::back()->with('excess', true); 
      }
        //if okay
      else if($amt == 0 && $lacking >= $amount){

        $total = $patient->TotalRedeem + $avblVoucher;
        $patient->TotalRedeem = $total;
        //added
        $patient->donationPercentage = $patient->goal - $patient->TotalRedeem;
        $patient->sponsorcount = $request->count +1;
        $patient->save();
          
        foreach ($V as $donate){
          $s = Sponsor::find($donate['sponsor_serial']);
          $s->status = "donated";
          $s->save();
          $donation = new Donation();
          $donation->patientid = $patient->patientid;
          $donation->sponsor_serial = $donate['sponsor_serial'];
          $donation->sponsorName = $radioname;
          $donation->amountDonated = $request->amount;
          // $donation->donationflag = $request->donationflag + 1;
          $donation->save();
            }
          $notif = new Notification();
          $notif->userid = $request->user;
          $notif->patienid = $patient->patientid;
          $notif->subject = $radioname;
          $notif->text = " has donated!";
          $notif->status = "unread";
          $notif->save();
        
          return Redirect::back()->with('success', true); 
          //if lacking iyang mga vouchers
        }
        else if ($amount != 0){      
          return Redirect::back()->with('info', true)->with('avblVoucher', $avblVoucher);
        }
        // else
        //   return Redirect::back()->with('alert', true);
      }

        return Redirect::back()->with('notenough', true);
  }


//donate to helpxp
  public function helpxpDonation(Request $request){
    $amount = $request->amount;
    $sponsor = Sponsor::select('voucherValue', 'sponsor_serial')->where('userid', Auth::id())->where('status', null)->orderBy('voucherValue', 'desc')->get();  
    $total = $sponsor->sum('voucherValue'); 
    $name = Auth::user()->fname." ".Auth::user()->lname;
    $radio = Input::get('anonymous') == true ? $request->anonymous:$name;
    if($amount == 0){
      return Redirect::back()->with('message', true);
    }

    $collect = new Collection();
    $V = new Collection();
      // $amount = $amount;
    if($amount <= $total){
      foreach($sponsor as $c){ 
        if($amount - $c['voucherValue'] >= 0){
          $V->push($c);
          $amount -= $c['voucherValue'];
        }
      }
        

      $avblVoucher =  $V->sum('voucherValue');
      if($amount == 0){
        foreach ($V as $donate){
          $s = Sponsor::find($donate['sponsor_serial']);
          $s->status = "d-hx";
          $s->save();
          $donation = new Donation();
          $donation->sponsor_serial = $donate['sponsor_serial'];
          $donation->sponsorName = $radio;
          $donation->amountDonated = $request->amount;
          $donation->save();
        }
          return Redirect::back()->with('success', true); 
        }
        else if ($avblVoucher != 0){    
          return Redirect::back()->with('info', true)->with('avblVoucher', $avblVoucher);
        }
        else
          return Redirect::back()->with('alert', true);
  }

  return Redirect::back()->with('notenough', true);
  }


} //end class
