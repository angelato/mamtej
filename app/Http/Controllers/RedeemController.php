<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Paginator;
use App\Patient;
use App\User;
use App\Picture;
use App\Donation;
use App\Sponsor;
use App\Redeem;
use App\Notification;
use App\Stories;
use Carbon\Carbon;
use App\Notifications\NotifyUsers;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;

class RedeemController extends Controller
{
    public function redeem(Request $request){
        $redeem = Patient::findorfail($request->id);
        if($redeem['goal'] != $redeem['TotalRedeem']) {
            $redeem->redeemStatus = "partial";
            $redeem->expirydateV = $request->expirydate;
            $redeem->newgoal = $redeem['goal'] - $redeem['TotalRedeem'];
            $redeem->redeemed = $redeem['goal'] - $redeem['newgoal'];
            $redeem->save();

            $rdm = new Redeem();
            $rdm->patientid = $request->id;
            $prev = Redeem::where('patientid', $request->id)->orderBy('created_at', 'desc')->first();
            $rdm->amountRedeemed =  $redeem['TotalRedeem'] - $prev['amountRedeemed'];
            $rdm->redeemStatus = "partial";
            $rdm->save();


        }
        else {
            $redeem->redeemStatus = "full";
            $redeem->expirydateV = $request->expirydate;
            $redeem->newgoal = $redeem['goal'] - $redeem['TotalRedeem'];
            $redeem->redeemed = $redeem['goal'] - $redeem['newgoal'];
            $redeem->save();

            $rdm = new Redeem();
            $rdm->patientid = $request->id;
            $rdm->amountRedeemed =  $redeem['TotalRedeem'];
            $rdm->redeemStatus = "full";
            $rdm->save();
        }

    	$sponsor = Sponsor::where('userid', Auth::id())->get();
    	$donation = Donation::where('patientid', '!=', null)->get();
    	$sponsorCollect = new Collection();
    	foreach($sponsor as $spr){
    	foreach($donation as $dnr){
    	    if($dnr['sponsor_serial'] == $spr['sponsor_serial']){
    	        $sponsorCollect->push($spr);
    	        }
    	    }
    	}

    	$helpxp = DB::table('donations')
    	->join('sponsors', 'sponsors.sponsor_serial', 'donations.sponsor_serial')
    	->where('sponsors.userid', Auth::id())
    	->where('donations.patientid', null)
    	->select(DB::raw("SUM(sponsors.voucherValue) as sum"))
    	->get();


	//redeem
       
    	$redeemdetails = Patient::where('userid', Auth::id())->where('redeemStatus', 'partial')->orWhere('redeemStatus', 'full')->get();

    	return view('history')->with(['sponsorCollect'=>$sponsorCollect, 'redeemdetails'=>$redeemdetails, 'helpxp'=>$helpxp]);
 
    }

    public function redeemConfirmation($patientid){
        $details = Patient::findorfail($patientid);
        $total = $details->TotalRedeem - $details->redeemed;
        // $details = Patient::where('userid', $user)->where('status', 'approved')->get();
        // $expirydate = date('Y-m-d', strtotime('+1 years'));
        return view('confirm')->with(['details'=>$details, 'total'=>$total]);
    } 

    public function redeemHistory($patientid) {
        $redeem = Redeem::where('patientid',$patientid)->get();
        $rdm = Redeem::where('patientid',$patientid)->sum('amountRedeemed');
        //return $redeem;
        return view('redeemhistory')->with(['redeem' => $redeem, 'rdm'=>$rdm]);
    }

} //end class
