<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Redirect;
use App\Notification;
use App\Patient;
use App\Sponsor;
use App\Stories;
use App\Donation;
use App\User;
use App\Billing;
use App\Recomoney;
use DB;
use Carbon\Carbon;
use App\Criteria;
use App\Recopercentage;

class RecommendationController extends Controller
{
    public function recommend(){
        $criteria = Criteria::all();
        // return $criteria[0];


        $listofpatients = new Collection();
        $patients = Patient::all();
        foreach ($patients as $allpatients) {
            $patient['patientid'] = $allpatients['patientid'];
            $patient['patientname'] = $allpatients['patientname'];
            $patient['condition'] = $allpatients['condition'];
            $patient['goal'] = $allpatients['goal'];
            $patient['totalredeem'] = $allpatients['TotalRedeem'];
            $patient['lacking'] = $allpatients['goal'] - $allpatients['TotalRedeem'];
            // return $patient['lacking'];
            $patient['donationPercentage'] = $allpatients['donationPercentage'];
            $patient['sponsorcount'] = $allpatients['sponsorcount'];
            $patient['created_at'] = $allpatients['created_at'];
            if($allpatients['TotalRedeem'] < $allpatients['goal']){
               $listofpatients->push($patient); 
            }
        }
        // return $listofpatients;

        //condition
        $conditionrank = 1;
        $conditionpatients = $listofpatients->sortByDesc('condition');
        $percentCondition = array();   
        $collectCondition = new Collection();
        foreach($conditionpatients as $patientcondition){
            $percentCondition['rank']= $conditionrank;
            $percentCondition['patientid'] = $patientcondition['patientid'];
            $percentCondition['patientname'] = $patientcondition['patientname'];
            $percentCondition['condition'] = $patientcondition['condition'];
            $percentCondition['lacking'] = $patientcondition['lacking'];
            // return $percentCondition['lacking'];
            $percentCondition['percentage'] = number_format($patientcondition['condition'] / 5 * $criteria[0]->percentage, 2, '.', ',');
            $collectCondition->push($percentCondition);
            $conditionrank++; 
            }
            // return $collectCondition;


        //highest need
        $hnrank = 1;
        $hnpatients = $listofpatients->sortByDesc('goal');
        $hneed = Patient::max('goal');//patient with highest need
        $percenthn = array();   
        $collecthn = new Collection();
        foreach($hnpatients as $hn){
            $percenthn['rank']= $hnrank;
            $percenthn['patientid'] = $hn['patientid'];
            $percenthn['patientname'] = $hn['patientname'];
            $percenthn['goal'] = $hn['goal'];
            $percenthn['lacking'] = $hn['lacking'];
            // return $percenthn['lacking'];
            $percenthn['percentage'] = number_format($hn['goal'] / $hneed * $criteria[1]->percentage, 2, '.', ',');
            $collecthn->push($percenthn);
            $hnrank++; 
            }
            // return $collecthn;


        //longest duration// pinaka karaan iphon no. of days then kwaon ang max 
        $ldrank = 1;    
        $percentlongestduration = array();
        $collectlongestduration = new Collection();
        // $long = $listofpatients->sortByDesc('created_at');
        $long = Patient::orderBy('created_at','asc')->get();
        // $created = $long->count();
        // $a = Patient::orderBy('created_at')->get();//min
        $a = $listofpatients->sortByDesc('created_at');//max
    //     // return $a;
         $now = Carbon::now();
        foreach ($a as $b) {
        $created = new Carbon($b['created_at']);
        // return $created;
        $dif = (int)(($created->diff($now)->days < 1)? 'today': $created->diffInDays($now));
        $dif += 1;
    }
        $max = $dif;
        
    
        $now = Carbon::now();
          foreach ($long as $longest) {
            $percentlongestduration['rank']= $ldrank;
            $percentlongestduration['patientid'] = $longest['patientid'];
            $percentlongestduration['patientname'] = $longest['patientname'];
            $percentlongestduration['datecreated'] = $longest['created_at'];
            $percentlongestduration['lacking'] = $longest['lacking'];

            $percentlongestduration['date'] = new Carbon($longest['created_at']);
            $difference = (int)(($percentlongestduration['date']->diff($now)->days < 1)? 'today': $percentlongestduration['date']->diffInDays($now));
            $difference += 1;
            $percentlongestduration['days'] = $difference;
             // return $percentlongestduration['days'];
            // return $percentlongestduration['days'];
            $percentlongestduration['percentage'] = number_format($difference / $max * $criteria[2]->percentage, 2, '.', ',');//divided by created at ranking
            $collectlongestduration->push($percentlongestduration);
            $ldrank++; 
          }
          // return $collectlongestduration;


        $mdrank = 1;
        $percentmostdonated = array();
        $collectmostdonated = new Collection();
        $most = $listofpatients->sortByDesc('sponsorcount');
        $hmost = $listofpatients->max('sponsorcount');
        
        foreach($most as $patientmostdonated){   
            $percentmostdonated['rank'] = $mdrank;      
            $percentmostdonated['patientid'] = $patientmostdonated['patientid'];
            $percentmostdonated['patientname'] = $patientmostdonated['patientname'];
            $percentmostdonated['lacking'] = $patientmostdonated['lacking'];
            // return $percentmostdonated['lacking'];
            $percentmostdonated['percentage'] = number_format($patientmostdonated['sponsorcount'] / $hmost * $criteria[4]->percentage, 2, '.', ',');
            $collectmostdonated->push($percentmostdonated);
            $mdrank++;   
        }
        // return $collectmostdonated;


        //change to pinakadakog kuwang//
        $percenthighestlacking = array();
        $collecthighestlacking = new Collection;
        $hlacking = $listofpatients->sortByDesc('donationPercentage');
        // return $hlacking;
        $highestlacking = $listofpatients->max('donationPercentage');
        // return $highestlacking;
          $hlrank = 1;
          foreach($hlacking as $soon){
            $percenthighestlacking['rank'] = $hlrank;  
            $percenthighestlacking['patientid'] = $soon['patientid'];
            $percenthighestlacking['patientname'] = $soon['patientname'];
            $percenthighestlacking['donationPercentage'] = $soon['donationPercentage'];
            $percenthighestlacking['lacking'] = $soon['lacking'];
            $percenthighestlacking['percentage'] = number_format($soon['donationPercentage'] / $highestlacking * $criteria[3]->percentage, 2, '.', ',');
            $collecthighestlacking->push($percenthighestlacking);
            $hlrank++;  
          }
          // return $collecthighestlacking;


        $collectall = new Collection();
        foreach($collecthighestlacking as $soon){
            $collectall->push($soon);
        }
        foreach($collectCondition as $condition){
            $collectall->push($condition);
        }
        foreach($collectlongestduration as $longest){
            $collectall->push($longest);
        }
        foreach($collecthn as $hn){
            $collectall->push($hn);
        }
        foreach($collectmostdonated as $most){
            $collectall->push($most);
        }
        // return $collectall;

  

        $list = array();
        $new = new Collection();
        foreach($collectall as $d){

            // $final = new Collection();
             // if($new->where('patientid', $d['patientid'])->count() == 0){//para di mag balik2 ang id(patientid is variable)
                $find = $collectall->where('patientid', $d['patientid']);

                $list['patientid'] = $d['patientid'];
                $list['patientname'] = $d['patientname'];
                $list['lacking'] = $d['lacking'];
                // return $list['lacking'];
                $sum = $find->sum('percentage');
                $list['percentsum'] = number_format($sum, 2, '.', ',');
                // return $list['percentsum'];

                 $new->push($list);
                
             
}
    $sortallp = $new->sortByDesc('percentsum');
    // return $sortallp;

        $arr = [];
       foreach ($sortallp as $call) {
            $arr[] = [
            'id' => $call['patientid'],
            'pname' => $call['patientname'],
            'lacking' => $call['lacking']
            ];

        }

            // return $arr;

        $allp = array_column($arr, 'id');
        $uniqueid = array_unique($allp);
        $arrP = Patient::find($uniqueid);
       
        $new = new Collection();
        foreach ($arrP as $arrP) {
           $find = Patient::find($arrP['patientid']);
           $new->push($find);
        }
        
        $arrPnt = $new->where('storystatus','=','approved')->take(5);
        // return $arrPnt;

        $array = array();
        // return $recommend;
        foreach ($arrPnt as $rec) {
            array_push($array , [
            'id' => $rec['patientid'],
            'lacking' => $rec['donationPercentage'],
            'money' => '0'
            ]);
        }
        

 $a = Recopercentage::get()->last();
 $current = $a->percentage;

 if($current > 0){
  
        $toArr = array();
        while($current >= 500){
            for($i= 0 ; count($array) > $i; $i++) { 
                if($array[$i]['lacking'] > 500 && $current >= 500){
                $array[$i]['money'] += 500;
                // return $rec['money'];
                $current -= 500;   
               
            }

        }
        // return $array;
        $collectrec = new Collection();
        foreach($array as $recarray){
            $recP = Patient::find($recarray['id']);
            $recP['money'] = $recarray['money'];
            // return $recP;
            $collectrec->push($recP);
            $recommend = $collectrec;
        }     

    }
 }
 elseif ($current == 0) {
    $recommend = $arrPnt;
 }


            //helpxp money
        $money = Sponsor::where('status', '=', 'd-hx')->sum('voucherValue');

        return view('recommend')
                ->with(['recommend'=>$recommend,
                 'collecthighestlacking'=>$collecthighestlacking, 
                 'collectCondition'=>$collectCondition,
                 'collecthn'=>$collecthn,
                 'collectlongestduration'=>$collectlongestduration,
                 'collectmostdonated' => $collectmostdonated,
                 'money'=>$money]);


    }//end function



    //donate to patients
    public function donaterecopatients(Request $req){
        $money = Recopercentage::get()->last();
        $diff = $money->percentage - $req->money;
        $val = $diff;

        $patient = Patient::findorfail($req->patientid);
        $patient->TotalRedeem += $req->money;
        $patient->donationPercentage -= $req->money;
        $patient->save(); 

        $new = new Recopercentage();
        $new->percentage = $val;
        $new->patientid = $req->patientid; 
        $new->save();

        return Redirect::back();
    }


    public function donatereco(Request $request){
        $money = Recopercentage::get()->last();
        $amount = $money + $request->recomoney;
        $val = $amount;

        $rm = new Recopercentage();
        $rm->percentage = $val;
        $rm->patientid = null;
        $rm->save();

        $helpxpmoney = Sponsor::where('status','=','d-hx')->get();
        foreach ($helpxpmoney as $donate) {
            $arr = Sponsor::find($donate['sponsor_serial']);
            $arr->status = "donated-xp";           
            $arr->save();

        }

        return Redirect::back();
        
    }



}
