<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Patient;

class StoryController extends Controller
{
    public function storyList(){

        $success = Patient::where('userid', Auth::id())->where('goal','=','TotalRedeem')->orWhere('redeemStatus', 'full')->get();
        $patient = Patient::where('userid', Auth::id())->where('goal','!=','TotalRedeem')->where('storystatus', 'approved')->get();
        $close = Patient::where('userid', Auth::id())->where('storystatus','=','Close')->get();

        return view('mystory')->with(['patient'=>$patient, 'success'=>$success, 'close'=>$close]);
    }

    public function archiveStory(){

        $denied = Patient::where('userid', Auth::id())->where('storystatus', 'denied')->get();
       

        return view('archivestory')->with(['denied'=>$denied]);
    }
}
