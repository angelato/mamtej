<?php

namespace App\Http\Controllers;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Sponsor;
use App\User;
use App\Donation;
use App\Patient;
use App\Stories;
use App\Picture;
use App\Redeem;
use DB;
use App\PageView;

class UserController extends Controller
{

    public function total(Request $request){
        $user = Patient::select('userid')->where('userid', Auth::id())->get();
        $pnt = Patient::where('redeemStatus', 'partial')->orWhere('storystatus', 'approved')->whereIn('userid', $user)->get();

        $patient = new Collection();
        foreach($pnt as $p){
            if($p->TotalRedeem - $p->redeemed > 0){
            $patient->push($p);
        }
        }
        return $patient;
    }

    public function history(){
    //sponsors                      
        $sponsor = Sponsor::where('userid', Auth::id())->get();
        $donation = Donation::where('patientid', '!=', null)->get();
        $sponsorCollect = new Collection();
        foreach($sponsor as $spr){
            foreach($donation as $dnr){
                if($dnr['sponsor_serial'] == $spr['sponsor_serial']){
                    $sponsorCollect->push($spr);
                    }
                }
        }



        $helpxp = DB::table('sponsors')
        ->where('sponsors.status', 'dd-hx')
        ->orWhere('sponsors.status', 'd-hx')
        ->select(DB::raw("SUM(sponsors.voucherValue) as sum"))
        ->get();


//redeem
       
        $redeemdetails = Patient::where('userid', Auth::id())->where('redeemStatus', 'full')->get();

        return view('history')->with(['sponsorCollect'=>$sponsorCollect, 'redeemdetails'=>$redeemdetails, 'helpxp'=>$helpxp]);
    }

    public function sharedStories(Request $request){

        $data = Patient::whereRaw('goal = TotalRedeem')->where('storystatus', 'approved')->orWhere('redeemStatus', 'full')->paginate(12);

        $real = DB::table('donations')
        ->select('amountDonated','sponsorName', DB::raw("DATE_FORMAT(created_at, '%d/%m/%Y')"))
        ->orderBy(DB::raw("DATE_FORMAT(created_at, '%d/%m/%Y')"))
        ->distinct('DATE(created_at)')
        ->get();

        $counter = PageView::where('pagename', 'welcomepage')->first();
        $counter->pgcounter = $counter->pgcounter + 1;
        $counter->save();
        
        return view('welcome')->with(['data'=>$data, 'real'=>$real]);
    }

    public function filterDonations($from, $to){
        
        $donation = DB::table('donations')
        ->join('patients', 'patients.patientid', 'donations.patientid')
        ->join('sponsors', 'sponsors.sponsor_serial', 'donations.sponsor_serial')
        ->join('users', 'users.id', 'patients.userid')
        ->where('sponsors.userid', Auth::id())
        ->whereBetween(DB::raw('DATE(sponsors.updated_at)'), array($from, $to))
        ->select('sponsors.voucherValue', 'sponsors.updated_at', 'users.fname', 'users.lname', 'patients.patientid')
        ->get();

        return $donation;
                
    }   

    public function recommend($cat, $ill){
        if($cat == 'most donated'){
            $pnt = DB::table('donations');
                return $pnt;
        }

    }

} //end class
 
