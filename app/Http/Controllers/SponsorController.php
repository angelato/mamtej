<?php

namespace App\Http\Controllers;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Sponsor;
use App\Patient;
use App\User;
use App\Notification;
use App\Billing;
use App\Donation;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use DB;

class SponsorController extends Controller
{
  public function buyvoucher(){
    
        return view('buyvoucher');
        
    }

  public function saveBankVoucher(Request $request){

    if(!is_null($request->qty1000)){
      $qty1000 = $request->qty1000 * 1000;
    }
    else{
      $qty1000 = 0;
    }
    if(!is_null($request->qty500)){
      $qty500 = $request->qty500 * 500;
    }
    else{
      $qty500 = 0;
    }
    if(!is_null($request->qty100)){
      $qty100 = $request->qty100 * 100;
    }
    else{
      $qty100 = 0;
    }
    if(!is_null($request->qty5000)){
      $qty5000 = $request->qty5000 * 5000;
    }
    else{
      $qty5000 = 0;
    }
    
    $total = $qty1000 + $qty500 + $qty5000 + $qty100;
  
    if($request->qty100 < 0 || $request->qty500 < 0 || $request->qty1000 < 0 || $request->qty5000 < 0){
      return Redirect::back()->with('error', true);
    }
    elseif($request->qty100 == 0 && $request->qty500 == 0 && $request->qty1000 == 0 && $request->qty5000 == 0){
      return Redirect::back()->with('nonzero', true);
    }
    elseif(Auth::user()->bank_balance < $total){
      return Redirect::back()->with('notenough', true);
    }
    else{
      for($i = 0; $i < $request->qty100; $i++){
        $user = new Sponsor();
        $user->userid = Auth::id();
        $user->voucherValue = 100;
        $user->save();
      }
      for($i = 0; $i < $request->qty500; $i++){
        $user = new Sponsor();
        $user->userid = Auth::id();
        $user->voucherValue = 500;
        $user->save();
      }  
        for($i = 0; $i < $request->qty1000; $i++){
        $user = new Sponsor();
        $user->userid = Auth::id();
        $user->voucherValue = 1000;
        $user->save();;
        }
        for($i = 0; $i < $request->qty5000; $i++){
        $user = new Sponsor();
        $user->userid = Auth::id();
        $user->voucherValue = 5000;
        $user->save();
        }

        $user = User::findorfail(Auth::id());
        $user->bank_balance = $user->bank_balance - $total;
        $user->save();
    }

    return Redirect::back()->with('success', true);
  }

  public function saveVoucherPaypal(Request $request){

    if(!is_null($request->qty1000)){
      $qty1000 = $request->qty1000 * 1000;
    }else{
      $qty1000 = 0;
    }
    if(!is_null($request->qty500)){
      $qty500 = $request->qty500 * 500;
    }else{
      $qty500 = 0;
    }if(!is_null($request->qty100)){
      $qty100 = $request->qty100 * 100;
    }else{
      $qty100 = 0;
    }
    if(!is_null($request->qty5000)){
      $qty5000 = $request->qty5000 * 5000;
    }else{
      $qty5000 = 0;
    }

    $total = $qty1000 + $qty500 + $qty5000 + $qty100;
  
    if($request->qty100 < 0 || $request->qty500 < 0 || $request->qty1000 < 0 || $request->qty5000 < 0){
      return Redirect::back()->with('error', true);
    }
    elseif($request->qty100 == 0 && $request->qty500 == 0 && $request->qty1000 == 0 && $request->qty5000 == 0){
      return Redirect::back()->with('nonzero', true);
    }
    elseif(Auth::user()->paypal_balance < $total){
      return Redirect::back()->with('notenough', true);
    }
    else{
      for($i = 0; $i < $request->qty100; $i++){
        $user = new Sponsor();
        $user->userid = Auth::id();
        $user->voucherValue = 100;
        $user->save();
      }
      for($i = 0; $i < $request->qty500; $i++){
        $user = new Sponsor();
        $user->userid = Auth::id();
        $user->voucherValue = 500;
        $user->save();
      }  
        for($i = 0; $i < $request->qty1000; $i++){
        $user = new Sponsor();
        $user->userid = Auth::id();
        $user->voucherValue = 1000;
        $user->save();;
        }
        for($i = 0; $i < $request->qty5000; $i++){
        $user = new Sponsor();
        $user->userid = Auth::id();
        $user->voucherValue = 5000;
        $user->save();
        }

        $user = User::findorfail(Auth::id());
        $user->paypal_balance = $user->paypal_balance - $total;
        $user->save();
    }

     return Redirect::back()->with('success', true);
  }
//angel
  public function newSponsor($patientid){

    $patient = Patient::findorfail($patientid);
    $data = Patient::where('storystatus', 'approved')->whereRaw('goal != TotalRedeem')->get(); 

    return view('sponsorDonate')->with(['patient'=>$patient, 'data'=>$data]);
    
  }
   
  public function newSponsorAny(){
    $data = Patient::where('storystatus', 'approved')->whereRaw('goal != TotalRedeem')->paginate(4);
    
         return view('donateAny')->with(['data'=>$data]);

  }

  public function getValue()
  {
    return view('viewvouchers');
  }

  public function getDen(){
    
    $overall = 0;
 
    $countvalue = Sponsor::select('voucherValue')->where('userid', Auth::id())->where('status', null)->distinct()->get();
    $value = array();
    $d = new Collection();
    foreach ($countvalue as $key) {
    $cnt = Sponsor::where('voucherValue',$key->voucherValue)->where('userid', Auth::id())->where('status', null)->get()->count();
    $total = $key->voucherValue * $cnt;
    $overall = $total+=$overall;

    if($key->voucherValue == 100){
      $value['value'] = '/images/v_100.jpg';
    }else if($key->voucherValue == 500){
      $value['value'] = '/images/v_500.jpg';
    }else if($key->voucherValue == 1000){
      $value['value'] = '/images/v_1000.jpg';
    }else if($key->voucherValue == 5000){
      $value['value'] = '/images/v_5000.jpg';
    }

    $value['count'] = $cnt;
    $value['total'] = (string)$overall;
    $d->push($value);    
  }
      return $d;
  } //end function


}
