<?php
namespace App\Http\Controllers;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Paginator;
use App\Patient;
use App\User;
use App\Picture;
use App\Donation;
use App\Sponsor;
use App\Redeem;
use App\Notification;
use App\Stories;
use Carbon\Carbon;
use App\Notifications\NotifyUsers;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
class PatientController extends Controller
{
    
    public function displayPatient(Request $request){
        if(Auth::user()->role == "admin") {
        $pnt = Patient::get();

         $data = Patient::whereRaw('goal != TotalRedeem')->where('storystatus', 'approved')->orWhere('redeemStatus', 'partial')->paginate(8);

        $success = Patient::whereRaw('goal = TotalRedeem')->where('storystatus', 'approved')->orWhere('redeemStatus', 'full')->paginate(8);

        return view('displayusers')->with(['data'=>$data, 'success'=>$success]);
    
        }

        $data = Patient::whereRaw('goal != TotalRedeem')->where('storystatus', 'approved')->orWhere('redeemStatus', 'partial')->paginate(8);

        $success = Patient::whereRaw('goal = TotalRedeem')->where('storystatus', 'approved')->orWhere('redeemStatus', 'full')->paginate(8);

        return view('homepage')->with(['data'=>$data, 'success'=>$success]);
    }

   
    public function patient($patientid){

        $patient = Patient::findorfail($patientid); 
        $patient->views = $patient->views + 1;
        $patient->save();
        $story = Stories::where('patientid', $patient['patientid'])->first();
   
        $real = DB::table('donations')
        ->select('amountDonated','sponsorName', DB::raw("DATE_FORMAT(created_at, '%d/%m/%Y')"))
        ->where('donations.patientid', $patientid)
       ->orderBy(DB::raw("DATE_FORMAT(created_at, '%d/%m/%Y')"), 'desc')
       ->distinct('DATE(created_at)')
       ->paginate(5);
    
        $pic = Picture::where('storyid', $story['storyid'])->paginate(1);

        $denied = Patient::where('storystatus', 'denied')->get();

        return view('singlelist')->with(['patient'=>$patient, 'story'=>$story,'pic'=>$pic, 'real'=>$real, 'denied'=>$denied]);

	}


    public function updatepage($patientid){
    //$patient = Patient::findorfail($patientid);
    $ups = stories::where('patientid', $patientid)->where('role', null)->paginate(1);

    return view('updatepage')->with(['ups'=>$ups]);

    }



    public function newPatient(){
        
    	return view('patientsdetail');

    }

    public function savePatient(Request $request){
        $condition = Input::get('condition') == true ? $request->condition:"error";
        $id = Auth::id();
        if($request->hasFile('profile')){
            $files = $request->file('profile');
            $origextension = $files->getClientOriginalExtension();
            $origname = $files->getClientOriginalName();
            $filename = pathinfo($origname, PATHINFO_FILENAME);
            $storefile = $filename.'-'.time().'.'.$origextension;
            $files->storeAs('public/picture', $storefile);
            
            
            $patient = new Patient();
            $patient->userid = $id;
            $patient->goal = $request->goal;
            $patient->filename = $storefile;
            $patient->storystatus = "pending";
            $patient->patientname = ucfirst($request->bname);
            $patient->illness = ucfirst($request->illness);
            $patient->condition = $condition;
            $patient->donationPercentage = $request->goal;
            $patient->save();

        }


            $story = new Stories();
            $story->patientid = $patient->patientid;
            $story->storytitle = $request->title;
            $story->story = $request->story;
            $story->role = "main";
            $story->save();
    	   $insertid = $patient->patientid;
        

        if($request->hasFile('medicalAbstract')){
            $files = $request->file('medicalAbstract');

            $origextension = $files->getClientOriginalExtension();
            $origname = "medicalAbstract";
            $filename = pathinfo($origname, PATHINFO_FILENAME);
            $storefile = $filename.'-'.time().'.'.$origextension;

            $files->storeAs('public/picture', $storefile);


            $picture = new Picture();
            $count = $picture::count();
            $picture->picid = "pic".$count;
            $picture->filename = $storefile;
            $picture->storyid = $story->storyid;
            $picture->save();
           
        }


        if($request->hasFile('medicalCertificate')){
            $files = $request->file('medicalCertificate');

            $origextension = $files->getClientOriginalExtension();
            $origname = "medicalCertificate";
            $filename = pathinfo($origname, PATHINFO_FILENAME);
            $storefile = $filename.'-'.time().'.'.$origextension;

            $files->storeAs('public/picture', $storefile);


            $picture = new Picture();
            $count = $picture::count();
            $picture->picid = "pic".$count;
            $picture->filename = $storefile;
            $picture->storyid = $story->storyid;
            $picture->save();
    
        } 

        if($request->hasFile('validID')){
            $files = $request->file('validID');

            $origextension = $files->getClientOriginalExtension();
            $origname = "validID";
            $filename = pathinfo($origname, PATHINFO_FILENAME);
            $storefile = $filename.'-'.time().'.'.$origextension;

            $files->storeAs('public/picture', $storefile);


            $picture = new Picture();
            $count = $picture::count();
            $picture->picid = "pic".$count;
            $picture->filename = $storefile;
            $picture->storyid = $story->storyid;
            $picture->save();
         
        }

        if($request->hasFile('hospitalBill')){
            $files = $request->file('hospitalBill');
     
            $origextension = $files->getClientOriginalExtension();
            $origname = "hospitalBill";
            $filename = pathinfo($origname, PATHINFO_FILENAME);
            $storefile = $filename.'-'.time().'.'.$origextension;

            $files->storeAs('public/picture', $storefile);


            $picture = new Picture();
            $count = $picture::count();
            $picture->picid = "pic".$count;
            $picture->filename = $storefile;
            $picture->storyid = $story->storyid;
            $picture->save();
     
        }
        
    	return redirect(url('/list/'.$insertid.'/view/'));
    }




    public function updateStory($patientid){

        return view('update')->with(['patient'=>$patientid]);

    }

    public function saveupdateStory(Request $request){
        $story = new Stories();
        $story->patientid = $request->patientid;
        $story->storytitle = $request->updatetitle;
        $story->story = $request->story;
        $story->role = null;
        $story->save();
        $insertid = $request->patientid;
        

        if($request->hasFile('file')){
            $files = $request->file('file');
            foreach($files as $file){

           $origextension = $file->getClientOriginalExtension();
           $origname = $file->getClientOriginalName();
           $filename = pathinfo($origname, PATHINFO_FILENAME);
           $storefile = $filename.'-'.time().'.'.$origextension;

            $file->storeAs('public/picture', $storefile);


            $picture = new Picture();
            $count = $picture::count();
            $picture->picid = "pic".$count;
            $picture->filename = $storefile;
            $picture->storyid = $story->storyid;

            $picture->save();
            }
        }

        return redirect(url('/update/'.$insertid.'/view/'));
    }

    public function savemodify(Request $request){
        
        $picture = Stories::findorfail($request->patientid);
            foreach ($picture->picture as $p) {
                $p->delete();
            }
            
        
        $condition = Input::get('condition') == true ? $request->condition:"error";
        $id = Auth::id();
        if($request->hasFile('profile')){
            $files = $request->file('profile');
            $origextension = $files->getClientOriginalExtension();
            $origname = $files->getClientOriginalName();
            $filename = pathinfo($origname, PATHINFO_FILENAME);
            $storefile = $filename.'-'.time().'.'.$origextension;
            $files->storeAs('public/picture', $storefile);
            
        
            $patient = Patient::findorfail($request->patientid);
            $patient->goal = $request->goal;
            $patient->filename = $storefile;
            $patient->storystatus = "pending";
            $patient->patientname = $request->bname;
            $patient->illness = $request->illness;
            $patient->condition = $condition;
            $patient->save();

        }

            $story = Stories::findorfail($request->patientid);
            $story->storytitle = $request->title;
            $story->story = $request->story;
            $story->role = "main";
            $story->save();
            $insertid = $story->patientid;

        if($request->hasFile('medicalAbstract')){
            $files = $request->file('medicalAbstract');
            // foreach($files as $file){

            $origextension = $files->getClientOriginalExtension();
            $origname = "medicalAbstract";
            $filename = pathinfo($origname, PATHINFO_FILENAME);
            $storefile = $filename.'-'.time().'.'.$origextension;

            $files->storeAs('public/picture', $storefile);


            $picture = new Picture();
            $count = $picture::count();
            $picture->picid = "pic".$count;
            $picture->filename = $storefile;
            $picture->storyid = $story->storyid;
            $picture->save();

        }


        if($request->hasFile('medicalCertificate')){
            $files = $request->file('medicalCertificate');
            // foreach($files as $file){

            $origextension = $files->getClientOriginalExtension();
            $origname = "medicalCertificate";
            $filename = pathinfo($origname, PATHINFO_FILENAME);
            $storefile = $filename.'-'.time().'.'.$origextension;

            $files->storeAs('public/picture', $storefile);


            $picture = new Picture();
            $count = $picture::count();
            $picture->picid = "pic".$count;
            $picture->filename = $storefile;
            $picture->storyid = $story->storyid;
            $picture->save();
            // }
        } 

        if($request->hasFile('validID')){
            $files = $request->file('validID');
            // foreach($files as $file){

            $origextension = $files->getClientOriginalExtension();
            $origname = "validID";
            $filename = pathinfo($origname, PATHINFO_FILENAME);
            $storefile = $filename.'-'.time().'.'.$origextension;

            $files->storeAs('public/picture', $storefile);


            $picture = new Picture();
            $count = $picture::count();
            $picture->picid = "pic".$count;
            $picture->filename = $storefile;
            $picture->storyid = $story->storyid;
            $picture->save();
            // }
        }


        if($request->hasFile('hospitalBill')){
            $files = $request->file('hospitalBill');
            // foreach($files as $file){

            $origextension = $files->getClientOriginalExtension();
            $origname = "hospitalBill";
            $filename = pathinfo($origname, PATHINFO_FILENAME);
            $storefile = $filename.'-'.time().'.'.$origextension;

            $files->storeAs('public/picture', $storefile);


            $picture = new Picture();
            $count = $picture::count();
            $picture->picid = "pic".$count;
            $picture->filename = $storefile;
            $picture->storyid = $story->storyid;
            $picture->save();
            // }
        }
        
        return redirect(url('/list/'.$insertid.'/view/'));
    }

    public function newmodify($patientid){

        $patient = Patient::join('stories', 'patients.patientid', '=', 'stories.patientid')
        ->select('patients.*', 'stories.*' )
        ->where('patients.storystatus', 'denied')->where('patients.patientid', $patientid)->get();
        //return $patient;
        return view('modifystory')->with(['patient'=>$patient]);
    }    

    public function closestory(Request $req){
        $denied = Patient::where('userid', Auth::id())->where('storystatus', 'denied')->get();
        $success = Patient::where('userid', Auth::id())->whereRaw('goal = TotalRedeem')->where('storystatus', 'approved')->orWhere('redeemStatus', 'full')->get();
        $patient = Patient::where('userid', Auth::id())->whereRaw('goal != TotalRedeem')->where('storystatus', 'approved')->orWhere('redeemStatus', 'partial')->get();
        
        $story = Patient::findorfail($req->patientid);
        $story->storystatus = "Close";
        $story->save();
        
        return view('mystory')->with(['patient'=>$patient, 'success'=>$success, 'denied'=>$denied]);
    }


}

