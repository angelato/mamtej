<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Criteria;

class CriteriaController extends Controller
{
    public function viewCriteria(){
        $criteria = Criteria::all();
        // return $criteria;
        return view('criteria')->with(['criteria'=>$criteria]);
    }


    public function changeCriteria(Request $req){
        $criteria = Criteria::all();
        
        Criteria::where('criteria_id', '=', $req->criteria_1)
        ->update(['percentage' => $req->condition]);

        Criteria::where('criteria_id', '=', $req->criteria_2)
        ->update(['percentage' => $req->hneed]);

        Criteria::where('criteria_id', '=', $req->criteria_3)
        ->update(['percentage' => $req->longest]);

        Criteria::where('criteria_id', '=', $req->criteria_4)
        ->update(['percentage' => $req->closing]);

        Criteria::where('criteria_id', '=', $req->criteria_5)
        ->update(['percentage' => $req->most]);

      
         return view('criteria')->with(['criteria'=>$criteria]);
    }
}
