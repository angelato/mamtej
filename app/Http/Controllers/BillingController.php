<?php

namespace App\Http\Controllers;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Sponsor;
use App\Patient;
use App\User;
use App\Notification;
use App\Billing;
use App\Donation;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use DB;

class BillingController extends Controller
{
    public function saveBankDepositSlip(Request $request){
      
    $billing = new Billing();
    $name = Auth::user()->fname." ".Auth::user()->lname;
    $billing->userid = Auth::id();

    if($request->hasFile('receipt')){
      $files = $request->file('receipt');
      $origextension = $files->getClientOriginalExtension();
      $origname = $files->getClientOriginalName();
      $filename = pathinfo($origname, PATHINFO_FILENAME);
      $storefile = $filename.'-'.time().'.'.$origextension;
      $files->storeAs('public/picture', $storefile);

      $billing->receipt = $storefile;
      $billing->accountHolder = $name;
      $billing->accountname = $request->accountname;
      $billing->accountnum = $request->accountnum;
      $billing->amountdeposited = $request->amountdeposited;
      $billing->ifReceived= "pending";
      $billing->save();

    }
    return Redirect::back()->with('ok', true);
  }

} //end class
