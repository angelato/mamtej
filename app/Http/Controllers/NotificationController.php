<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Redirect;
use App\Notification;
use App\Patient;
use App\Sponsor;
use App\Stories;
use App\Donation;
use App\User;
use App\Billing;
use App\Recomoney;
use DB;
use Carbon\Carbon;
use App\Criteria;
use App\Recopercentage;

class NotificationController extends Controller
{
    public function storynotifRead(){
        $notifyUser = Notification::where('userid', Auth::id())->where('patienid','!=',null)->where('status','=','read')->orderBy('created_at', 'desc')->get();
        return $notifyUser ;
        //'created_at' => $downline->created_at->diffForHumans(),
    }

    public function markRead()
    {
        $count = Notification::where('userid', Auth::id())->where('patienid','!=',null)->where('status','=','unread')->get(); 
        foreach ($count as $count) {
            $arr = Notification::find($count['notif_id']);
            $arr->status = 'read';
            $arr->save();
        }
       return $count;
    }

    public function storynotifunread(){
        $story = Notification::where('userid', Auth::id())->where('patienid','!=',null)->where('status','=','unread')->get();
        return $story;
    }

    public function storynotifcount(){
        $storycount = Notification::where('userid', Auth::id())->Where('patienid','!=', null)->where('status','=','unread')->get()->count();
       return $storycount;
    }

    //madam
     public function realtimedonation(){
          $real = DB::table('donations')
       ->select('amountDonated','sponsorName', DB::raw("DATE_FORMAT(created_at, '%d/%m/%Y')"))
       ->orderBy(DB::raw("DATE_FORMAT(created_at, '%d/%m/%Y')"))
       ->distinct('DATE(created_at)')
       ->get();
        

        return $real;
    }


    //voucher notif
    public function vouchernotifcount(){
        $vouchercount = Notification::where('userid', Auth::id())->Where('patienid','=', null)->where('status','=','unread')->get()->count();
       return $vouchercount;
    }

     public function voucherRead(){
        $notifyUser = Notification::where('userid', Auth::id())->where('patienid','=',null)->where('status','=','read')->orderBy('created_at', 'desc')->get();
        return $notifyUser;
    }

    public function vouchermarkRead()
    {
        $read = Notification::where('userid', Auth::id())->where('patienid','=',null)->where('status','=','unread')->get(); 
        foreach ($read as $read) {
            $arr = Notification::find($read['notif_id']);
            $arr->status = 'read';
            $arr->save();
        }
       return $read;
    }

    public function vouchernotifunread(){
        $voucher = Notification::where('userid',Auth::id())->where('patienid','=',null)->where('status','=','unread')->get();
        return $voucher;
    }

    public function adminNotifVoucherCount(){
        $admin = Billing::where('ifReceived','=','pending')->count();
        // $admin = Notification::where('userid','=','admin')->where('subject','=','Check')->where('status','=','unread')->count();
        return $admin;
    }

    public function adminStoryCount(){
        $story = Patient::where('storystatus','=','pending')->count();
        return $story;
    }

    public function redeemNotifcount(){
        $redeem = Patient::where('redeemStatus','=','partial')->count();
        return $redeem;
    }


}
