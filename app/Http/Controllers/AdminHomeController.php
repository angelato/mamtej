<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Collection;
use App\Http\Requests;
use App\Http\Controllers\AdminHomeController as admin;
use Auth;
use App\User;
use App\Stories;
use App\Picture;
use App\Patient;
use App\Sponsor;
use App\Donation;
use App\Billing;
use App\Notification;
use App\Redeem;
use App\Criteria;
use Carbon\Carbon;
use DB;
use App\Events\Reco;
use AppHelper;
use App\Jobs\Recommendation;
use App\Alert;
use App\Recipient;
use App\PageView;
use PDF;

class AdminHomeController extends Controller
{ 

    public function adminLogin(Request $req) {
        if (Auth::attempt(['username' => $req->username, 'password' => $req->password, 'role' => 'admin']))
        {

            return view('displayUsers');
        }
        else
            return view('auth.admin-login');
    }

    public function viewUsers()
    {
        if(Auth::user()->role == "admin"){

        $data = Patient::where('goal', '!=', 'TotalRedeem')->where('storystatus', 'approved')->orWhere('redeemStatus', 'partial')->get();

        $success = Patient::where('goal', 'TotalRedeem')->where('storystatus', 'approved')->orWhere('redeemStatus', 'full')->get();

        return view('displayUsers')->with(['data'=>$data, 'success'=>$success]);

        }

        else
            return "ERROR!";

    }

    public function viewPatients()
    {

        if(Auth::user()->role == "admin"){
           
        $stories = DB::table('patients')
            ->join('stories', 'patients.patientid', '=', 'stories.patientid')
            ->select('patients.*', 'stories.storytitle', 'stories.story')->get();

        return view('displayPatients')->with(['stories'=>$stories]);
        
        }
        else
            return "ERROR!";
    }

    public function viewSponsors()
    {

        if(Auth::user()->role == "admin"){
            $users = Sponsor::select('userid')->distinct('userid')->get();
            return view('displaysponsors', compact('users'));
            }
        else
            return "ERROR!";
    }

    public function pageView(){

        $pageview = PageView::where('pagename', 'welcomepage')->first();

        return $pageview;
    }

    public function sponsorSponsored($id) {
        // AppHelper::setreco();
        if(Auth::user()->role == "admin"){
            $sponsorCollect = DB::table('donations')
            ->join('sponsors', 'donations.sponsor_serial', '=', 'sponsors.sponsor_serial')
            ->join('patients', 'donations.patientid', '=', 'patients.patientid')
            ->select('donations.amountDonated', 'patients.patientname', 'donations.patientid', 'sponsors.userid', DB::raw("DATE_FORMAT(donations.created_at, '%M %d, %Y') as date"))
            ->where('sponsors.userid', $id)
            ->orderBy(DB::raw("DATE_FORMAT(donations.created_at, '%M %d, %Y')"))
            ->distinct('DATE(donations.created_at)')
            ->paginate(10);

            return view('displaySponsorSponsored')->with(['sponsorCollect'=>$sponsorCollect,'id'=>$id]);
    }

    else return "ERROR!";

    }

    public function sponsoredHelpxp($id) {
        if(Auth::user()->role == "admin"){

            $sponsorCollect = DB::table('donations')
            ->join('sponsors', 'donations.sponsor_serial', '=', 'sponsors.sponsor_serial')
            ->select('donations.amountDonated', 'donations.patientid', 'sponsors.userid', DB::raw("DATE_FORMAT(donations.created_at, '%M %d, %Y') as date"))
            ->where('sponsors.userid', $id)
            ->where('donations.patientid', null)
            ->orderBy(DB::raw("DATE_FORMAT(donations.created_at, '%M %d, %Y')"))
            ->distinct('DATE(donations.created_at)')
            ->paginate(10);

            return view('filtersponsorshelpxp')->with(['sponsorCollect'=>$sponsorCollect,'id'=>$id]);
    }

    else return "ERROR!";
    }

    public function filterSponsor(Request $request,$id){ 
        // AppHelper::setreco();
        if(Auth::user()->role == "admin"){
        $from = $request->from;
        $to = $request->to;

        $sponsorCollect = DB::table('donations')
        ->join('sponsors', 'donations.sponsor_serial', '=', 'sponsors.sponsor_serial')
        ->join('patients', 'donations.patientid', '=', 'patients.patientid')
       ->select('donations.amountDonated', 'donations.patientid', 'patients.patientname', 'sponsors.userid', DB::raw("DATE_FORMAT(donations.created_at, '%M %d, %Y') as date"))
       ->where('sponsors.userid', $id)
       ->orderBy(DB::raw("DATE_FORMAT(donations.created_at, '%M %d, %Y')"))
       ->distinct('DATE(donations.created_at)')
       ->whereBetween(DB::raw('DATE(donations.created_at)'), array($from, $to))
       ->paginate(10);
        
         return view('displaySponsorSponsored')->with(['sponsorCollect'=>$sponsorCollect,'id'=>$id]);
         }
        else 
            return "ERROR";
    }

    public function filterSponsorHelpxp(Request $request,$id){ 
        // AppHelper::setreco();
        if(Auth::user()->role == "admin"){
        $from = $request->from;
        $to = $request->to;

        $sponsorCollect = DB::table('donations')
        ->join('sponsors', 'donations.sponsor_serial', '=', 'sponsors.sponsor_serial')
        ->select('donations.amountDonated', 'donations.patientid', 'sponsors.userid', DB::raw("DATE_FORMAT(donations.created_at, '%M %d, %Y') as date"))
        ->where('sponsors.userid', $id)
        ->where('donations.patientid', null)
        ->orderBy(DB::raw("DATE_FORMAT(donations.created_at, '%M %d, %Y')"))
        ->distinct('DATE(donations.created_at)')
        ->whereBetween(DB::raw('DATE(donations.created_at)'), array($from, $to))
        ->paginate(10);
        
        //return $sponsorCollect;
         return view('filtersponsorshelpxp')->with(['sponsorCollect'=>$sponsorCollect,'id'=>$id]);
         }
        else 
            return "ERROR";
    }

    public function approveStories() {
        // AppHelper::setreco();
        if(Auth::user()->role == "admin"){
            $patient = Patient::where('storystatus', 'pending')->get();
            $approve = new Collection();
            foreach($patient as $p){
                $story = $p->stories[0];
                $approve->push($story);
            }        
            return view('approveStories')->with(['approve'=>$approve]);
        }
        else 
            return "ERROR";

    }

    public function approved(Request $request){
        // AppHelper::setreco();
        if(Auth::user()->role == "admin"){
            if($request->submit == "Deny"){
            $pnt = Patient::findOrFail($request->patientid);
            $pnt->storystatus = "denied";
            $deny = implode(', ',$request->denymsg);
            $pnt->denymsg=$deny;
            $pnt->save();

            $patient = Patient::where('storystatus', 'pending')->get();
            $approve = new Collection();
            foreach($patient as $p){
                $story = $p->stories[0];
                $approve->push($story);
            }

        $notifs = new Notification();
        $notifs->userid = $request->userid;
        $notifs->patienid =$request->patientid;
        $notifs->subject = $request->story;
        $notifs->text = "Story has been denied"; 
        $notifs->status = "unread";
        $notifs->save();
            }
        else if($request->submit == "Confirm"){
            $pnt = Patient::findOrFail($request->patientid);
            $pnt->storystatus = "approved";
            $pnt->save();

            $patient = Patient::where('storystatus', 'pending')->get();
            $approve = new Collection();
            foreach($patient as $p){
                $story = $p->stories[0];
                $approve->push($story);
            }

        $notifs = new Notification();
        $notifs->userid = $request->userid;
        $notifs->patienid =$request->patientid;
        $notifs->subject = $request->story;
        $notifs->text = " story has been approved";
        $notifs->status = "unread";
        $notifs->save();
    
            
        }
        return view('approveStories')->with(['approve'=>$approve]);
     }

        else 
            return "ERROR";

    }

        public function filterOngoing(){
        // AppHelper::setreco();
        if(Auth::user()->role == "admin"){
            // $users = Patient::select('userid')->distinct('userid')->get();
            // return view('displaypatients', compact('users'));
        $stories = DB::table('patients')
            ->join('stories', 'patients.patientid', '=', 'stories.patientid')
            ->select('patients.*', 'stories.storytitle', 'stories.story')->get();

        return view('filterongoing')->with(['stories'=>$stories]);
        
        }
        else
            return "ERROR!";
    }

    public function filterSuccess(){
        // AppHelper::setreco();
        if(Auth::user()->role == "admin"){
            // $users = Patient::select('userid')->distinct('userid')->get();
            // return view('displaypatients', compact('users'));
        $stories = DB::table('patients')
            ->join('stories', 'patients.patientid', '=', 'stories.patientid')
            ->select('patients.*', 'stories.storytitle', 'stories.story')->get();

        return view('filtersuccess')->with(['stories'=>$stories]);
        
        }
        else
            return "ERROR!";
    }

        public function filterDenied(){
        // AppHelper::setreco();
        if(Auth::user()->role == "admin"){
            // $users = Patient::select('userid')->distinct('userid')->get();
            // return view('displaypatients', compact('users'));
        $stories = DB::table('patients')
            ->join('stories', 'patients.patientid', '=', 'stories.patientid')
            ->select('patients.*', 'stories.storytitle', 'stories.story')->get();

        return view('filterdenied')->with(['stories'=>$stories]);
        
        }
        else
            return "ERROR!";
    }

    public function filterSponsorDate(Request $request){ 
        // AppHelper::setreco();
        if(Auth::user()->role == "admin"){
        $from = $request->from;
        $to = $request->to;
        $users = sponsor::select('userid')->distinct('userid')->whereBetween(DB::raw('DATE(created_at)'), array($from, $to))->get();
         return view('displaySponsors')->with(['users'=>$users]);
         }
        else 
            return "ERROR";
    }

    public function filterPatientDate(Request $request){ 
        // AppHelper::setreco();
        if(Auth::user()->role == "admin"){
        $from = $request->from;
        $to = $request->to;
        // $sql = "SELECT * FROM patients 
        // INNER JOIN stories 
        // ON patients.patientid = stories.patientid";

        $stories = DB::table('patients')
            ->join('stories', 'patients.patientid', '=', 'stories.patientid')
            ->select('patients.*', 'stories.storytitle', 'stories.story')
            ->whereBetween(DB::raw('DATE(patients.created_at)'), array($from, $to))
            ->get();

         return view('displayPatients')->with(['stories'=>$stories]);
         }
        else 
            return "ERROR";
    }

    public function filterPatientDateOngoing(Request $request){ 
        // AppHelper::setreco();
        if(Auth::user()->role == "admin"){
        $from = $request->from;
        $to = $request->to;
        // $sql = "SELECT * FROM patients 
        // INNER JOIN stories 
        // ON patients.patientid = stories.patientid";

        $stories = DB::table('patients')
            ->join('stories', 'patients.patientid', '=', 'stories.patientid')
            ->select('patients.*', 'stories.storytitle', 'stories.story')
            ->whereBetween(DB::raw('DATE(patients.created_at)'), array($from, $to))
            ->get();

         return view('filterongoing')->with(['stories'=>$stories]);
         }
        else 
            return "ERROR";
    }

    public function filterPatientDateDenied(Request $request){ 
        // AppHelper::setreco();
        if(Auth::user()->role == "admin"){
        $from = $request->from;
        $to = $request->to;
        // $sql = "SELECT * FROM patients 
        // INNER JOIN stories 
        // ON patients.patientid = stories.patientid";

        $stories = DB::table('patients')
            ->join('stories', 'patients.patientid', '=', 'stories.patientid')
            ->select('patients.*', 'stories.storytitle', 'stories.story')
            ->whereBetween(DB::raw('DATE(patients.created_at)'), array($from, $to))
            ->get();

         return view('filterdenied')->with(['stories'=>$stories]);
         }
        else 
            return "ERROR";
    }

    public function filterPatientDateSuccess(Request $request){ 
        // AppHelper::setreco();
        if(Auth::user()->role == "admin"){
        $from = $request->from;
        $to = $request->to;
        // $sql = "SELECT * FROM patients 
        // INNER JOIN stories 
        // ON patients.patientid = stories.patientid";

        $stories = DB::table('patients')
            ->join('stories', 'patients.patientid', '=', 'stories.patientid')
            ->select('patients.*', 'stories.storytitle', 'stories.story')
            ->whereBetween(DB::raw('DATE(patients.created_at)'), array($from, $to))
            ->get();

         return view('filtersuccess')->with(['stories'=>$stories]);
         }
        else 
            return "ERROR";
    }



    public function requestRedeem()
    {
        if(Auth::user()->role == "admin") {
         $patient = Patient::where('redeemStatus', 'partial')->orWhere('redeemStatus', 'full')->where('action', '!=', 'Released')->get();

        $rdm = Redeem::get();
        $stories = Stories::get();

        $redeem = Redeem::where('action', null)->get();

        $user = Auth::id();   

        $released = Patient::where('userid', $user)->where('redeemStatus', 'partial')->orWhere('redeemStatus', 'full')->get();

        $expirydate = date('Y-m-d', strtotime('+1 years'));


    
        return view('reqRedeems')->with(['redeem'=>$redeem, 'released'=>$released, 'expirydate'=>$expirydate]);
        }
        else
            "ERROR";
    }

    public function requestApproveRedeem(Request $request)
    {
        // AppHelper::setreco();
        // dd($request);
        if(Auth::user()->role == "admin"){
        $patient = Patient::where('redeemStatus', 'partial')->orWhere('redeemStatus', 'full')->get();
        // dd($request);
            $pnt = Patient::findOrFail($request->patientid);
            $pnt->action = "Released";
            $pnt->save();

            // $rdm = Redeem::where('redeemid','=',$request->redeemid)->where('action', '=',NULL)->get();
            // dd($rdm);
            // $rdm = Redeem::find($request->redeemid);
            // dd($rdm);
            // $rdm->action = "Released";
            // $rdm->recipient = $request->recipient;
            // $rdm->save();

        Redeem::where('redeemid', '=', $request->redeemid)
        ->update(['action' => 'Released',
            'recipient' => $request->recipient]);
        


            //return $rdm;
            return redirect(url('/releasedvouchers'));
            }

        
       
        else
            return "ERROR!";

    }

    public function reqConfirm(Request $request){
        // AppHelper::setreco();
        $details = Patient::findorfail($request->patientid);
        $total = $details->redeemed;
        // $details = Patient::where('userid', $user)->where('status', 'approved')->get();
        $expirydate = date('d M Y', strtotime('+1 years'));
        return view('confirmreq')->with(['details'=>$details, 'total'=>$total, 'expirydate'=>$expirydate]);
    }

    public function releasedVouchers() {
        $redeem = Redeem::where('action', 'Released')->get();

        return view('releasedvouchers')->with(['redeem'=>$redeem]);
    }


    public function released(Request $request) {
        $find = Redeem::findorfail($request->patientid)->where('action','=','Released')->get();
        foreach ($find as $find) {
        $find->action = "claimed";
        $find->save();
        }
        

        return redirect::back();
    }

    public function searchRedeem($request) {
        if(Auth::user()->role == "admin") {


        $search = $request;

        $srch = Patient::where('patientname','LIKE', '%' .$search. '%')->get();
        if($request == 'all'){
            $srch = Patient::where('newgoal', "!=", null)->where('action', null)->get();
        }
        if(count($srch)>0){  
            $text = '';
            foreach ($srch as $rdm) {
                # code...
            
        $text.='<tr class="fixed-table-container">
            <td class="fixed-table-container"><img src="'.
              url('storage/picture/'.$rdm->filename).
            '" width="100px" height="100px" /></td>
            <td class="fixed-table-container">'.$rdm->storytitle.'</td>
            <td class="fixed-table-container">'.$rdm->userName->name.'</td>
            <td class="fixed-table-container">'.$rdm->patientname.'</td>
            <td class="fixed-table-container">'.$rdm->goal.'</td>
            <td class="fixed-table-container">'.$rdm->TotalRedeem.'</td>
            <td class="fixed-table-container">'.$rdm->newgoal.'</td>
            <td class="fixed-table-container">'.$rdm->redeemStatus.'</td>
  
            <td class="fixed-table-container" align="center">
                <div class="container">
                <div class="interior">
                <a class="btn" href="#'.$rdm->patientid.'">Release</a>
                </div>
                </div>

                <div id="'.$rdm->patientid.'" class="modal-window">
                <div>
            <a href="#modal-close" title="Close" class="modal-close">Close</a>
            <div class="main-content" >

        <form class="form-basic" action="'.url('/requests').'" method="post" enctype="multipart/form-data" style="background-color: #F9F9F9">
        '.csrf_field().'

        <input type="hidden" name="patientid" value="'.$rdm->patientid.'">

            <div class="form-title-row">
                <h2><strong>Voucher Details!</strong></h2>
            </div>
            <table border="0" cellpadding="20px">
            <tr>
            <div class="form-row">
                <label>
                    <td><strong>Date</strong></td>
                   <td> <input type="text" name="title" value="'.$rdm->updated_at->format('F d, Y').'"></td>
                </label>
             </tr>
            </div>

            <tr>
                  <td><strong>Beneficiary Name</strong></td> 
                   <td> <input type="text" name="title" value="'.$rdm->patientname.'"></td>
                
            </tr>
            
            <tr>
                <label>
                    <td><strong>Amount Redeemed</strong></td>
                    <td><input type="text" name="title" value="'.$rdm->redeemed.'"></td>
                </label>
             </tr>
            </table><br><br>
            <div>
                <label>
                    <span><input type="submit" class="btn btn-primary" value="Submit">
                    <input type="reset" class="btn btn-danger" value="Reset"></span>
                </label>
            </div>

        </form>

    </div>
                </div>
            </div>
            </td>
            <!--  -->
    <!-- </form> -->

        </tr>';
    }


        return $text;
        }
    }

    }

        // angel
    public function checkPayment() {
        // AppHelper::setreco();
        if(Auth::user()->role == "admin"){
            $bill = Billing::where('ifReceived','=','pending')->get();
            $check = new Collection();
            foreach($bill as $bills){
                $candonate = $bills->sponsors[0];
                $check->push($candonate);
            }
            return view('check')->with(['bill'=>$bill]);
        }
        else 
            return "ERROR";

    }
    
    public function checked(Request $request) {
        if(Auth::user()->role == "admin"){

            if($request->submit == "deny"){
            $bill = Billing::find($request->billing_id);
            $bill->ifReceived = "denied";
            $bill->save();            
                $bill = Billing::where('ifReceived','=','pending')->get();
            $notifs = new Notification();
            $notifs->userid = $request->userid;
            $notifs->patienid = null;
            $notifs->subject = " ";
            $notifs->text = "Payment denied, ". $request->message;
            $notifs->status = "unread";
            $notifs->save();

                return view('check')->with(['bill'=>$bill]);
            }

            else if($request->submit == "check"){
            $bill = Billing::find($request->billing_id);
            $bill->ifReceived = "received";
            $bill->save();
                $bill = Billing::where('ifReceived','=','pending')->get();
            $notifs = new Notification();
            $notifs->userid = $request->userid;
            $notifs->patienid = null;
            $notifs->subject = " ";
            $notifs->text = "Payment accepted, you may now buy voucher/s";
            $notifs->status = "unread";
            $notifs->save();
            $bank = User::findorfail($request->userid);
            $bank->bank_balance = $bank->bank_balance + $request->bankmoney;
            $bank->save();
            return view('check')->with(['bill'=>$bill]);
            }

        }
            
    }

    public function users() {
        $users = DB::table('users')->where('status', 1)->where('role', NULL)->count();
        return $users;
    }

    public function sponsors() {
       
        $sponsors = Sponsor::where('status', 'donated')->orWhere('status', 'd-hx')->distinct('userid')->count('userid');
        return $sponsors;

    }

    public function patients() {

        $patients = Patient::where('storystatus','=','approved')->count();
        return $patients;
    }

    public function displayStories() {
        if(Auth::user()->role == "admin"){

        $patient = Patient::get();
        $stories = new Collection();
            foreach($patient as $p){
                $pnt = $p->stories[0];
                $stories->push($pnt);
            }

        return view('displayStories')->with(['stories'=>$stories]);
        
        }

        else
            return "ERROR!";

    }

    public function searchPatient(Request $request){
        // AppHelper::setreco();
        $search = $request->search;
        $patient = Patient::where('illness','LIKE', '%' .$search. '%')->get();
        if(count($patient)>0){
          return view('search')->with(['patient' => $patient]);  
      }else
        return Redirect::back()->with("No Results Found");      
    }

    public function pdfvoucher() {

        $rdm = Redeem::where('action', 'Released')->get();

        $pdf = PDF::loadView('pdfvoucher', array('rdm' => $rdm) );
       
       
        return $pdf->download('helpxp.pdf');
    }

    

} //end class


