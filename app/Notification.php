<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
    //
     protected $primaryKey = 'notif_id';
	public function user(){
    	return $this->hasOne('App\User', 'id', 'userid');
    } 

     public function sponsor(){
        return $this->hasOne('App\Sponsor', 'userid', 'userid');
    }
    
     public function patient(){
        return $this->hasOne('App\Patient', 'patientid', 'patientid');
    }

    public function stories(){
    return $this->hasOne('App\Stories', 'patientid', 'patientid');
    }


}
