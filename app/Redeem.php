<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Redeem extends Model
{
	protected $table = 'Redeem';
    protected $primaryKey = 'patientid';

    public function patient(){
        return $this->hasOne('App\Patient', 'id', 'patientid');
    }
     public function story(){
        return $this->hasOne('App\Stories', 'patientid', 'patientid');
    }
}
