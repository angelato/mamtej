<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Patient extends Model
{
	protected $primaryKey = 'patientid';

    public function sponsor(){
        return $this->hasMany('App\Sponsor', 'userid', 'userid');
    }
    public function userName(){
    	return $this->hasOne('App\User','id','userid');
    }
    public function donation(){
    	return $this->hasMany('App\Donation', 'id');
    }
    public function picture(){
    	return $this->hasMany('App\Picture', 'patientid', 'id');
    }



    public function patient(){
        return $this->hasMany('App\Patient', 'id', 'patientid');
    }

    public function redeem(){
        return $this->hasMany('App\Redeem', 'patientid', 'patientid');
    }   
    
    public function stories(){
        return $this->hasMany('App\Stories', 'patientid', 'patientid');
    }
    

}
