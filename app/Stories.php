<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Stories extends Model
{
    //

    protected $primaryKey = 'storyid';
    
    public function patient(){
        return $this->hasOne('App\Patient', 'patientid', 'patientid');
    }

    public function picture(){
        return $this->hasMany('App\Picture', 'storyid', 'storyid');
    }
    
}
