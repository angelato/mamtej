<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class Donation extends Model
{

	protected $primaryKey = 'donationid';

    public function sponsor(){
    	return $this->hasOne('App\Sponsor','sponsor_serial','sponsor_serial');
    }
    public function patient(){
    	return $this->hasOne('App\Patient','patientid','patientid');
    }

    // public function user(){
    // 	return $this->hasOne('App\User','id','userid');
    // }
}
