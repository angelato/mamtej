<?php

namespace App\Listeners;

use App\Events\Reco;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Http\Controllers\AdminHomeController as admin;
use App\Criteria;
use App\Donation;
use App\Sponsor;
use App\Patient;
use Illuminate\Database\Eloquent\Collection;

class Recommend
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  Reco  $event
     * @return void
     */
    public function handle(Reco $event)
    {
        // $admin = admin::recommending();
            // dd($event->admin);
    //     $carbon = now();
    // $date = $carbon->format('H:i:s');
    // $from = '14:50:00'; $to = '14:52:00';
    if(false){
        dd($event);
        $criteria_p = Criteria::all();

$donation = Donation::select('sponsor_serial')->where('sponsor_serial', '!=', null)->get();
$tohelpxp = Sponsor::where('status', 'd-hx')->whereNotIn('sponsor_serial', $donation)->orderBy('voucherValue', 'desc')->get();
$count_helpxp = $tohelpxp->count();
$sum = $tohelpxp->sum('voucherValue');
//if tanan patient kay nahatagan na
if(Patient::where('flag', '1')->count() == Patient::count()){
    $p_all = Patient::all();
    foreach($p_all as $all){
        $save = Patient::findorfail($all->patientid);
        $save->flag = 0;
        $save->save();
    }
}

//*most sponsor donated
$mostDonated = Sponsor::select('userid')->distinct()->where('status', 'donated')->get();

$most_donated = new Collection();
foreach($mostDonated as $md){
    $spnr = Sponsor::where('userid', $md->userid)->where('status', 'd-hx')->orWhere('status', 'donated')->orWhere('status', 'dd-hx')->count();
    $most['count'] = $spnr;
    $most['userid'] = $md->userid;
    $most_donated->push($most);
}
$sort_most_donated = $most_donated->sortByDesc('count');
//*find patients under this userid's
$patient_withdonation = new Collection();
foreach($sort_most_donated as $most_d){
    $pnts = Patient::where('userid', $most_d['userid'])->where('flag', '0')->get();
    foreach($pnts as $p){
        $p_id = $p->userName->sponsor->count();
        $pa['count'] = $p_id;
        $pa['patientid'] = $p->patientid;
        $pa['patientname'] = $p->patientname;
        $patient_withdonation->push($pa); 
    }
}
$sorted_pwd = $patient_withdonation->sortByDesc('count');
$patients_list = new Collection();
$total_wd = $sorted_pwd->count();
$to_wd = $total_wd;
$total_wd_rank = 1;
foreach($sorted_pwd as $p=>$pwd){
    if($patients_list->where('patientid', $pwd['patientid'])->count() == 0){
        $find = $sorted_pwd->where('count', $pwd['count']);
        foreach($find as $f=>$fi){
            $formula = ($criteria_p[0]->percentage / $to_wd) * $total_wd;
            $wd['rank'] = $total_wd_rank;
            $wd['patientid'] = $fi['patientid'];
            $wd['patientname'] = $fi['patientname'];
            $wd['count'] = $fi['count'];
            $wd['points'] = number_format($formula, 2, '.', ',');
            $patients_list->push($wd);
        }
        $total_wd--; 
        $total_wd_rank++;
    }
}
$patients = $patients_list->take(10);

$sorted_pld = Patient::orderBy('created_at')->where('flag', '0')->get();

$longestDuration_count = new Collection();
$total_pld = $sorted_pld->count();
$to_pld = $total_pld;
$total_pld_rank = 1;
foreach($sorted_pld as $pld){
    if($longestDuration_count->where('patientid', $pld['patientid'])->count() == 0){
    $find = $sorted_pld->where('created_at', $pld['created_at']);
        foreach($find as $fi){
            $formula = ($criteria_p[1]->percentage / $to_pld) * $total_pld;
            $ld['rank'] = $total_pld_rank;
            $ld['created_at'] = $fi['created_at'];
            $ld['patientid'] = $fi['patientid'];
            $ld['patientname'] = $fi['patientname'];
            $ld['points'] = number_format($formula, 2, '.', ',');
            $longestDuration_count->push($ld);
        }
        $total_pld--;
        $total_pld_rank++;
    }
}
$longestDuration = $longestDuration_count->take(10);

$highestNeed = new Collection();
$patient_phn = Patient::where('flag', 0)->get();
foreach($patient_phn as $pnts){
    $pat['patientid'] = $pnts['patientid'];
    $pat['patientname'] = $pnts['patientname'];
    $lacking = $pnts['goal'] - $pnts['TotalRedeem'];
    $pat['lacking'] = $lacking;
    $highestNeed->push($pat);
}
$sorted_phn = $highestNeed->sortByDesc('lacking');
$highest_need_count = new Collection();
$total_phn = $sorted_phn->count();
$to_phn = $total_phn;
$total_phn_rank = 1;
foreach($sorted_phn as $p=>$phn){
    if($highest_need_count->where('patientid', $phn['patientid'])->count() == 0){
        $find = $sorted_phn->where('lacking', $phn['lacking']);
        foreach($find as $fi){
            $formula = ($criteria_p[2]->percentage / $to_phn) * $total_phn;
            $hn['rank'] = $total_phn_rank;
            $hn['patientid'] = $fi['patientid'];
            $hn['patientname'] = $fi['patientname'];
            $hn['points'] = number_format($formula, 2, '.', ',');
            $hn['lacking'] = $fi['lacking'];
            $highest_need_count->push($hn);
        }
        $total_phn--;
        $total_phn_rank++;
    }
}
$highest_need = $highest_need_count->take(10);



$sorted_c = Patient::orderByDesc('condition')->where('flag', '0')->get();
$condition_count = new Collection();
$total_c = $sorted_c->count();
$to_c = $total_c;
$total_c_rank = 1;
foreach($sorted_c as $c){
    if($condition_count->where('condition', $c['condition'])->count() == 0){
        $find = $sorted_c->where('condition', $c['condition']);
        foreach($find as $fi){
            $formula = ($criteria_p[3]->percentage / $to_c) * $total_c;
            $con['rank'] = $total_c_rank;
            $con['patientid'] = $fi['patientid'];
            $con['condition'] = $fi['condition'];
            $con['patientname'] = $fi['patientname'];
            $con['points'] = number_format($formula, 2, '.', ',');
            $condition_count->push($con);
        }
        $total_c--;   
        $total_c_rank++;
    }
}
$condition = $condition_count->take(10);

$all_patients = new Collection();
foreach($longestDuration as $p_longest){
    $all_patients->push($p_longest);
}
foreach($highest_need as $p_highest){
    $all_patients->push($p_highest);
}
foreach($condition as $p_condition){
    $all_patients->push($p_condition);
}
foreach($patients as $patient){
    $all_patients->push($patient);
}

$final_list = new Collection();
foreach($all_patients as $d){
    $final = new Collection();
    if($final_list->where('patientid', $d['patientid'])->count() == 0){
    $total_r = Patient::select('TotalRedeem')->where('patientid', $d['patientid'])->get();
        $find = $all_patients->where('patientid', $d['patientid']);
        foreach($find as $f){
            $final->push($f);
        }
        $f_pnt['patientid'] = $d['patientid'];
        $f_pnt['patientname'] = $d['patientname'];
        $f_pnt['sum'] = $find->sum('points')."%";
        $f_pnt['created_at'] = $final[0]['created_at'];
        $f_pnt['date'] = $final[0]['points'];
        $f_pnt['lacking'] = $final[1]['points'];
        $f_pnt['lacking_num'] = $final[1]['lacking'];
        $f_pnt['condition'] = $final[2]['points'];
        if(isset($final[3])){
            $f_pnt['count'] = $final[3]['points'];
        }
        $f_pnt['flag'] = 0;
        $f_pnt['TotalRedeem'] = $total_r[0]->TotalRedeem;
        $final_list->push($f_pnt);
    }
}

$sorted_all = $final_list->sortByDesc('sum');
$total_rank = 1;
foreach($sorted_all as $s=>$s_all){
    $s_all['rank'] = $total_rank;
    $sorted_all[$s] = $s_all;
    $total_rank++;
}
$finalList = new Collection();
foreach($sorted_all as $all_p){
    $all_p['flag'] = 1;
    $finalList->push($all_p);
}

//RECOMMENDATION
$t = new Collection();
    foreach($tohelpxp as $v){ 
        $cnt = 0;
        foreach($finalList as $p){
            if($p['flag'] == 1){
            $cnt++;
            }
        }
        if($cnt == $finalList->count()){
            foreach($finalList as $pp=>$p){
                $p['flag'] = 0; 
                $finalList[$pp] = $p;
            }
        }
    foreach($finalList as $pi=>$p){
    if($p['lacking_num'] - $v->voucherValue >= 0 && $v->status == "d-hx" && $p['flag'] == 0){
            $p['TotalRedeem'] = $p['TotalRedeem'] + $v->voucherValue;
            $p['lacking_num'] = $p['lacking_num'] - $v->voucherValue;
            $v->status = 'dd-hx';
            $p['flag'] = 1;
            $finalList[$pi] = $p;
        
            $sponsor = Sponsor::findOrFail($v->sponsor_serial);
            $sponsor->status = 'dd-hx';
            $sponsor->save();

            $patient = Patient::findOrFail($p['patientid']);
            $newTotal = $patient->TotalRedeem + $v->voucherValue;
            $patient->TotalRedeem = $newTotal;
            $patient->flag = 1;
            $patient->save();

            $donation = new Donation();
            $donation->patientid = $p['patientid'];
            $donation->sponsor_serial = $v->sponsor_serial;
            $comName = $v->user->fname." ".$v->user->lname;
            $donation->sponsorName = $comName;
            $donation->save();
    }
    }
    }


$all_collection = new Collection();
$all_collection->push($patients);
$all_collection->push($longestDuration);
$all_collection->push($highest_need);
$all_collection->push($condition);
$all_collection->push($finalList);
$all_collection->push($count_helpxp);



return $all_collection;
        }
    }
}
