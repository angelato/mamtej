<?php

namespace App\Console\Commands;
use App\Donation;
use App\Sponsor;
use App\Patient;
use App\User;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Console\Command;

class recommendation extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'recommendation';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'recommends deserving patients';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
      
    }
}
