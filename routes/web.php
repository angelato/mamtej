<?php 

Route::get('/', 'UserController@sharedStories'); //display success stories main page

Route::get('/about', function(){
	return view('about');
});

Route::view('/adminlogin', 'Auth.admin-login'); //admin login
Route::post('/verifyadmin', 'AdminHomeController@adminLogin'); //admin verification

Auth::routes();
Route::get('verifyEmailFirst', 'Auth\RegisterController@verifyEmailFirst')->name('verifyEmailFirst'); //user email
Route::get('verify/{email}/{token}', 'Auth\RegisterController@sendEmailDone')->name('sendEmailDone'); //user email

Route::post('/singlelist', 'PatientController@savePatient'); //save patient story
Route::get('/list/{id}/view','PatientController@patient'); //display patient's posted story
Route::get('/update/{id}/view','PatientController@updatepage'); //display updates of a patient

Route::post('/modifylist', 'PatientController@savemodify'); //save modified story

route::middleware(['auth'])->group(function(){
Route::get('/percentChange/{percent1}/{percent2}/{percent3}/{percent4}/','AdminHomeController@percentChange');
Route::get('/getDen','SponsorController@getDen');
Route::get('/helpVoucher','AdminHomeController@helpVoucher');
Route::get('/recommending','AdminHomeController@recommending');


Route::get('/home', 'PatientController@displayPatient');
Route::get('/patientsdetail', 'PatientController@newPatient');

Route::get('/modify/{patientid}', 'PatientController@newmodify');

Route::get('/mystory', 'StoryController@storyList');
Route::get('/archivestory', 'StoryController@archiveStory');

Route::get('/history', 'UserController@history');
Route::get('/sponsorDonate/{patientid}', 'SponsorController@newSponsor');
Route::get('/donateAny/{sponsorid}','SponsorController@newSponsorAny');
Route::post('/helpxp', 'DonationController@patientDonation');
Route::post('/homepage', 'DonationController@helpxpDonation');
Route::post('/requestsConfirm', 'AdminHomeController@reqConfirm');
Route::get('/confirm/{patientid}', 'RedeemController@redeemConfirmation');
Route::post('/redeem', 'RedeemController@redeem');

Route::get('/total', 'UserController@total');
Route::get('/rcmPatient', 'AdminHomeController@rcmPatient');

Route::get('/buyvoucher/{userid}', 'SponsorController@buyVoucher');
Route::get('/update/{patientid}', 'PatientController@updateStory');
Route::post('/saveUpdate', 'PatientController@saveupdateStory');
//ADMIN Routes
Route::get('/approve', 'AdminHomeController@approveStories');
Route::post('/approved', 'AdminHomeController@approved');
Route::get('/request', 'AdminHomeController@requestRedeem');
Route::post('/requests', 'AdminHomeController@requestApproveRedeem');
Route::get('/releasedvouchers', 'AdminHomeController@releasedVouchers');

Route::post('/released', 'AdminHomeController@released');

Route::get('/displaypatients', 'AdminHomeController@viewPatients');
Route::get('/displaysponsors', 'AdminHomeController@viewSponsors');
Route::get('/delete', 'AdminHomeController@deleteUsers');
Route::get('/sponsoredpatient/{userid}', 'AdminHomeController@sponsorSponsored');
Route::get('/sponsoredhelpxp/{userid}', 'AdminHomeController@sponsoredHelpxp');
Route::get('/displayusers', 'AdminHomeController@viewUsers');
//filter
Route::post('/filter', 'AdminHomeController@filterSponsorDate');
Route::post('/filterpatient', 'AdminHomeController@filterPatientDate');

Route::post('/filtersponsor/{userid}', 'AdminHomeController@filterSponsor');
Route::post('/filtersponsorhelpxp/{userid}', 'AdminHomeController@filterSponsorHelpxp');

Route::get('/ongoing', 'AdminHomeController@filterOngoing'); //ongoing button display
Route::get('/denied', 'AdminHomeController@filterDenied'); //denied button
Route::get('/success', 'AdminHomeController@filterSuccess'); //success button

Route::post('/filterongoing', 'AdminHomeController@filterPatientDateOngoing');
Route::post('/filterdenied', 'AdminHomeController@filterPatientDateDenied');
Route::post('/filtersuccess', 'AdminHomeController@filterPatientDateSuccess');
//filter end
Route::get('/users', 'AdminHomeController@users');//
Route::get('/patients', 'AdminHomeController@patients');
Route::get('/sponsors', 'AdminHomeController@sponsors');
Route::get('/pageview', 'AdminHomeController@pageView');
//Route::get('/redeemhistory/{patientid}', 'RedeemController@redeemHistory');
Route::get('/stories/{userid}', 'AdminHomeController@displayStories');
Route::get('/displayvouchers/{userid}', 'AdminHomeController@displayVouchers');
Route::get('/searchredeem/{request}','AdminHomeController@searchRedeem');//

Route::get('/filterDonations/{from}/{to}', 'UserController@filterDonations');

Route::get('/helpxp/pdf', 'AdminHomeController@pdfvoucher'); //pdf download
Route::get('/pdfvoucher', function(){
	return view('pdfvoucher');
});


//angel
Route::get('/check', 'AdminHomeController@checkPayment');
Route::post('/checked', 'AdminHomeController@checked');
Route::get('/viewvoucher', 'SponsorController@getValue');
Route::post('/search','AdminHomeController@searchPatient');
Route::get('/newStories','AdminHomeController@newStories');
Route::get('/longestStory','AdminHomeController@longestStory');
Route::get('/mostDonated','AdminHomeController@mostDonatedStory');
Route::get('/real', 'NotificationController@realtimedonation');
Route::get('/read','NotificationController@storynotifRead');
Route::get('/markRead','NotificationController@markRead');
Route::get('/storycount','NotificationController@storynotifcount');
Route::get('/storynotifunread', 'NotificationController@storynotifunread');
Route::get('/vouchercount','NotificationController@vouchernotifcount');
Route::get('/voucherunread','NotificationController@vouchernotifunread');
Route::get('/voucherread','NotificationController@voucherRead');
Route::get('/vouchermarkread','NotificationController@vouchermarkRead');
Route::get('/complete','NotificationController@complete');
Route::get('/select','NotificationController@buttonIllness');
Route::get('/adminVoucherCount','NotificationController@adminNotifVoucherCount');
Route::get('/adminStoryCount','NotificationController@adminStoryCount');
Route::get('/redeemCount','NotificationController@redeemNotifcount');
Route::get('/a','NotificationController@datesample');
Route::post('/voucher', 'BillingController@saveBankDepositSlip');

Route::post('/bvoucher', 'SponsorController@saveBankVoucher');
Route::get('/reco', 'RecommendationController@recommend');
Route::get('/criteria', 'CriteriaController@viewCriteria');
Route::post('/criteria', 'CriteriaController@changeCriteria');
// Route::get('/home', 'NotificationController@read');
Route::post('/recoresult', 'RecommendationController@donaterecopatients');
Route::post('/stories', 'PatientController@closestory');//endstory
Route::get('/denomination','NotificationController@helpxpmoney');
Route::post('/donatereco','RecommendationController@donatereco');



//new
Route::get('/try', 'RecommendationController@divide');






Route::get('/donatePatient', 'AdminHomeController@donate');
Route::get('/recommend/{cat}/{ill}', 'UserController@recommend');



Route::get('/setreco', 'AdminHomeController@setreco');

//paypal
Route::get('/buyvoucherpaypal', function(){
	return view('paypal_buyvouchers');
});
Route::get('/paypal', function(){
	return view('paywithpaypal');
});
Route::post('paypal', 'PaypalPaymentController@payWithPaypal');
Route::get('status', 'PaypalPaymentController@getPaymentStatus');
Route::post('/voucherpaypal', 'SponsorController@saveVoucherPaypal');


});

Route::get('invoice', function() {
	return view('invoice');
});


