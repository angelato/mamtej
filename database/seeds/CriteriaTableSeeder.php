<?php

use Illuminate\Database\Seeder;

class CriteriaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $criteria = [
        	[
        		'name' => 'Condition',
                'percentage' => '40'
       		],
            [
                'name' => 'Highest Need',
                'percentage' => '20'
            ],
            [
                'name' => 'Longest Duration',
                'percentage' => '15'
            ],
            [
                'name' => '80% of the goal',
                'percentage' => '15'
            ],
             [
                'name' => 'Most no. of donations',
                'percentage' => '10'
            ]


       	];
       	DB::table('criterias')->insert($criteria);
    }
}
