<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBillingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Billings', function (Blueprint $table) {
             $table->increments('billing_id');
            $table->string('userid');
            $table->string('accountname');
            $table->integer('accountnum');
            $table->integer('amountdeposited');
            $table->string('receipt');
            $table->string('accountHolder');
            $table->string('ifReceived');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Billings');
    }
}
