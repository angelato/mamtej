<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('paypal_balance')->default(0);
            $table->integer('bank_balance')->default(0);
            $table->string('fname');
            $table->string('lname');
            $table->string('username')->unique();
            $table->string('email');
            $table->date('birthdate');
            $table->string('address');
            $table->double('contact');
            $table->string('password');
            $table->string('role')->nullable();
            $table->boolean('status')->default(0);
            $table->string('token', 254)->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
