<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePatientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('patients', function (Blueprint $table) {
            $table->increments('patientid');
            $table->string('userid');
            $table->integer('goal');
            $table->string('filename');
            $table->integer('views')->default(0);
            $table->string('storystatus')->nullable();
            $table->integer('TotalRedeem')->default(0);
            $table->integer('donationPercentage')->default(0);
            $table->integer('sponsorcount')->default(0);
            $table->date('expirydateV')->nullable();
            $table->string('patientname');
            $table->string('illness');
            $table->integer('condition');
            $table->integer('newgoal')->nullable();
            $table->string('redeemed')->nullable();
            $table->string('redeemStatus')->nullable();
            $table->string('action')->nullable();
            $table->integer('flag')->default(0);
            $table->string('denymsg')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('patients');
    }
}
